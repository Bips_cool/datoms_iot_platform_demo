Changes to src/js/index.js
-----------------------------
1. use BrowserRouter instead of HashRouter - Replace 3 occurences of HashRouter by BrowserRouter
2. use actual baseName instead of '' - comment and uncomment the actual line

For App
-----------------------
1. npm run build
2. gulp generate_sri_for_css_and_js_files
3. gulp php_file

For API
------------------------
gulp api_files
