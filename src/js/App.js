import React from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';
import Devices from './components/Devices';
import Dashboard from './components/Dashboard';
import ArchiveReport from './components/CustomReport';
import SMSReport from './components/SMSSummaryReport';
import DeviceDebug from './components/DeviceDebug';
import DeviceConfiguration from './components/DeviceConfiguration';
import DataPushStatus from './components/DataPushStatus';
import OSPCBDebug from './components/OSPCBDebug';
import CPCBDebug from './components/CPCBDebug';
import Error404 from './components/Error404';

export default class App extends React.Component {
	constructor() {
		super();
		this.state = {};

		this.baseName = '';
		if ('##PR_STRING_REPLACE_APP_BASE_PATH##' === 'deploy') {
			this.baseName = '/enterprise/' + (document.getElementById('client_slug') ? document.getElementById('client_slug').value : 'phoenix-robotix') + '/pollution-monitoring/vendor-portal';
		}
	}

	componentDidMount() {
		var tout = null;
		if (!window.showPopup && document.getElementById('popup_alert') && document.getElementById('popup_alert_msg')) {
			window.showPopup = (type, message, timeout) => {
				if (!timeout) {timeout=5000;}
				document.getElementById('popup_alert').className = 'alert alert-'+type+' active';
				document.getElementById('popup_alert_msg').innerHTML = message;
				tout = setTimeout(() => {
					hidePopup();
				}, timeout);
				console.log('Message:',message);
			};
		}
		if (!window.hidePopup && document.getElementById('popup_alert')) {
			window.hidePopup = () => {
				document.getElementById('popup_alert').className = 'alert';
				clearTimeout(tout);
			};
		}
	}

	render() {
		return <Router basename={this.baseName}>
			<Switch>
				<Route exact path='/devices' component={Devices}/>
				<Route exact path='/devices/' component={Devices}/>
				<Route exact path='/' component={Dashboard}/>
				<Route exact path='/reports/archive-report' component={ArchiveReport} />
				<Route exact path='/reports/archive-report/' component={ArchiveReport} />
				<Route exact path='/reports/violation-summary-report' component={SMSReport} />
				<Route exact path='/reports/violation-summary-report/' component={SMSReport} />
				<Route exact path='/devices/:station_id/configuration' component={DeviceConfiguration}/>
				<Route exact path='/devices/:station_id/configuration/' component={DeviceConfiguration}/>
				<Route exact path='/devices/:device_id/debug' component={DeviceDebug}/>
				<Route exact path='/devices/:device_id/debug/' component={DeviceDebug}/>
				<Route exact path='/error-status' component={DataPushStatus} />
				<Route exact path='/error-status/' component={DataPushStatus} />
				<Route exact path='/stations/:station_id/ospcb-debug' component={OSPCBDebug}/>
				<Route exact path='/stations/:station_id/ospcb-debug/' component={OSPCBDebug}/>
				<Route exact path='/stations/:station_id/cpcb-debug' component={CPCBDebug}/>
				<Route exact path='/stations/:station_id/cpcb-debug/' component={CPCBDebug}/>
				{/*<Route exact path='/devices/:device_id/debug/view/:view_mode' component={DeviceDebug}/>
				<Route exact path='/devices/:device_id/debug/view/:view_mode/' component={DeviceDebug}/>*/}
				<Route component={Error404}/>
			</Switch>
		</Router>;
	}
}