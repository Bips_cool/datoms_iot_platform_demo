import React from 'react';
import NavLink from './NavLink';
import ReportsNavMenu from './imports/ReportsNavMenu';
import CustomReportsForm from './imports/CustomReportsForm';
import moment from 'moment-timezone';
import { Scrollbars } from 'react-custom-scrollbars';
import ReactHighcharts from 'react-highcharts';
import HighchartsMore from 'highcharts-more';

export default class CustomReport extends React.Component {
	constructor(props) {
		HighchartsMore(ReactHighcharts.Highcharts);
		super(props);
		/**
		 * state of the component
		 * @type {object}
		 * @property {object} data the report data send form the server
		 * @property {array} selected_parameters all the parameters selected to be shown
		 * @property {array} data_format the modes to be shown graph or grid or both
		 * @property {array} units consist of the units of each parameter being shown
		 * @property {object} all_stations_list all the station of the industry
		 * @property {array} all_parameters list of all the parameters
		 */
		this.state = {
			data: null,
			selected_parameters: null,
			data_format: null,
			selected_stations:[],
			units: null,
			all_stations_list: null,
			all_parameters: null,
			summary_report:null,
			data_loading: false,
			report_steps: [
				{
					selector: '#each_row',
					content: 'Stations',
				},
				{
					selector: '#prameters',
					content:  'Parameters',
				},
				{
					selector: '#time_interval',
					content:  'Time Interval',
				},
				{
					selector: '#aggre_period',
					content:  'Aggregation Period'
				},
				{
					selector: '#data_format',
					content:  'View Data Format',
				},
				{
					selector: '#download_data_format',
					content:  'Download Data Format',
				},
				{
					selector: '#view_button',
					content:  'View Data',
				},
				{
					selector: '#download_button',
					content:  'Download Data',
				}
			],
			isTourOpen: false
		};
		
		/**
		 * @property {array} graph_colors different possiblity colours of graph
		 */
		this.graph_colors = [
			'#43429a',
			'#07adb1',
			'#a44a9c',
			'#f4801f',
			'#c14040',
			'#6fccdd',
			'#61c3ab',
			'#56bc7b',
			'#e2da3e',
			'#41ce00',
			'#aa4728',
			'#b3d23c',
			'#a0632a',
			'#7156a3',
			'#3d577f',
			'#ee3352'
		];

		/**
		 * @property {array} graph_colors different possiblity colours to fill graph
		 */
		this.graph_fill_color = [
			'rgba(67, 66, 154, 0.05)',
			'rgba(7, 173, 177, 0.05)',
			'rgba(164, 74, 156, 0.05)',
			'rgba(244, 128, 31, 0.05)',
			'rgba(193, 64, 64, 0.05)',
			'rgba(111, 204, 221, 0.05)',
			'rgba(97, 195, 171, 0.05)',
			'rgba(86, 188, 123, 0.05)',
			'rgba(226, 218, 62, 0.05)',
			'rgba(65, 206, 0, 0.05)',
			'rgba(170, 71, 40, 0.05)',
			'rgba(179, 210, 60, 0.05)',
			'rgba(160, 99, 42, 0.05)',
			'rgba(113, 86, 163, 0.05)',
			'rgba(61, 87, 127, 0.05)',
			'rgba(238, 51, 82, 0.05)'
		];

		// this.getStationList();
	}

	/**
	 * This function gets the list of all the stations and parameters
	 */
	getStationList(){
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_stations_and_parameters_of_industry_for_reports', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			body: JSON.stringify({
				industry_id: (document.getElementById('client_id')) ? document.getElementById('client_id').value : 1
			})
		}).then(function(Response) {
			// console.log(Response);
			return Response.json();
		}).then(function(json) {
		//	console.log('Stations:', json);
			if (json.status === 'success') {
				that.setState({
					all_stations_list: json.stations,
					all_parameters: json.parameters
				});
				// console.log('Active_Station', that.state.active_station);
				// console.log('Stations_id:', json.stations);
			} else {
				showPopup('danger',json.message);
				// that.setState({loading_data: null});
			}
		});
	}

	/**
	 * Get the archive report data and sets the state
	 * @param {object} report_details 
	 */
	viewIndustryReport(report_details) {
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_archive_report_data_of_industry.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				station_ids: report_details.station_ids,
				parameters: report_details.parameters,
				time_interval: report_details.time_interval,
				data_format: report_details.data_format,
				average_over_time_base: report_details.average_over_time_base
			})
		}).then((response) => {
			return response.json();
		}).then((json) => {
			console.log('Industry Data: ', json);
			if(json.status === 'success') {
				that.setState({
					data: json.station_data,
					selected_parameters: json.parameters,
					selected_stations:report_details.station_ids,
					data_format: report_details.data_format,
					time_interval: report_details.average_over_time_base,
					checked_parameters: report_details.parameters,
					timestamps: json.timestamps,
					summary_report:json.station_summary,
					data_loading: false
				});
			} else {
				showPopup('danger', json.message);
			}
		}).catch((ex) => {
			console.log("Some error in geting report data", ex);
			showPopup('danger', 'Unable to load data!');
		});
	}

	/**
	 * Unsets the previous state and then call the 
	 * @param {object} report_details 
	 */
	renderIndustryReport(report_details){
		let that = this;
		this.setState({
			data: null,
			selected_parameters: null,
			data_format: null,
			time_interval: null,
			checked_parameters: null,
			units: null,
			data_loading: true
		}, () => {
			that.viewIndustryReport(report_details);
		});
	}

	/**
	 * returns the station name of a station according to the station id
	 * @param {string} id 
	 */
	getStationName(id){
		let that = this,station_name='';
		Object.keys(that.state.all_stations_list).map((key) => {
			that.state.all_stations_list[key].map((station) => {
				if(station.id == id){
			//		console.log('station name',station.name);
					station_name=station.name;
				}
			});
		});
		return station_name;
	}

	componentDidMount() {
		document.title = 'Archive Data -  DATOMS® Pollution Monitoring';
		// this.getStationList();
	}
	
	render() {
		ReactHighcharts.Highcharts.setOptions({
			global: {
				useUTC: false
			}
		});

		return <div className="">
			{(() => {
			})()}
			<Scrollbars autoHide>
			<NavLink activeLink={'reports'} />
				<div className="report-container">
					<ReportsNavMenu activeLink={'industry'} />
					<CustomReportsForm {...this.state} getReport={(report_details) => this.renderIndustryReport(report_details)}/>
				</div>
				{(() => {
					if (this.state.data === null) {
						if (this.state.data_loading) {
							return <div className="text-center">
								<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
							</div>;
						}
					} else if (Object.keys(this.state.data).length === 0) {
						return <div className="container text-center">
							<h3 className="text-muted">No records found</h3>
						</div>;
					} else {
						return <div>
							{(()=>{
								if(this.state.data_format && this.state.data_format.length >=1 && false){
									return <div>
										{(()=>{
											let data_summary = Object.keys(this.state.data).map((station_id)=>{
												return <div className="container text-center"> 
													<span className='bold'>{this.state.data[station_id].station_name}</span>
													<div className="table-wrapper top-margin">
														<div className="report-table-container">
															<table className="table table-bordered">
																<thead className="thead">
																	<tr>
																		<th className="text-center"> Parameter </th>
																		<th className="text-center"> Avg </th>
																		<th className="text-center"> Min </th>
																		<th className="text-center"> Max </th>																	
																	</tr>
																</thead>
																<tbody>
																	{(()=>{
																		let table_row = this.state.checked_parameters.map((param)=>{
																			// console.log(param);
																			if(this.state.data[station_id].required_params && this.state.data[station_id].required_params.indexOf(param) !== -1){
																				let count = 0,average = 0,max_value=0,max_value_index = 0, min_value = 99999, min_value_index = 0, param_index = this.state.data[station_id].required_params.indexOf(param);
																				// console.log(param_index,this.state.data[station_id].param_values[param_index]);
																				this.state.data[station_id].param_values.map((param_value,index)=>{
																					console.log(isNaN(param_value[param_index][0]),param_value[param_index][0]);
																					if(param_value[param_index][0] !== null){
																						count++;
																						average += parseFloat(param_value[param_index][1]);
																					}
																					if(param_value[param_index][1] !== null && param_value[param_index][1]){
																						if(min_value > parseFloat(param_value[param_index][1])){
																							min_value = parseFloat(param_value[param_index][1]);
																							min_value_index = index;
																						}
																					}
																					if(param_value[param_index][2] !== null && param_value[param_index][2] != 0){
																						if(max_value < parseFloat(param_value[param_index][2])){
																							max_value = parseFloat(param_value[param_index][2]);
																							max_value_index = index;
																						}
																					}
																				});
																				
																				return <tr>
																				<td><span dangerouslySetInnerHTML={{__html:this.state.selected_parameters[param].name}} /> (<span dangerouslySetInnerHTML={{__html:this.state.selected_parameters[param].unit}} />)</td>
																				<td> {(count > 0)?(average/count).toFixed(2):'NA'} </td>
																				<td> {(min_value === 99999)?'NA':min_value} <br/> 
																					<small>
																						({(min_value === 99999)?'':moment.unix(parseInt(this.state.timestamps[min_value_index][0])).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix(parseInt(this.state.timestamps[min_value_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')})
																					</small> 
																				</td>
																				<td> {(max_value === 0)? 'NA':max_value} <br/> 
																				<small>
																				({ (max_value === 0)?'':moment.unix(parseInt(this.state.timestamps[max_value_index][0])).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix(parseInt(this.state.timestamps[max_value_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')})																				
																				</small> 
																				</td>
																			</tr>;
																			}																		
																		}).filter(Boolean);
																		// console.log(table_row);
																		return table_row; 
																	})()}
																</tbody>
															</table>
														</div>
													</div>
												</div>;												
											});
											return data_summary;
										})()}
									</div>;
								}
								else if(this.state.data_format && this.state.data_format.length >=1 && this.state.summary_report && Object.keys(this.state.summary_report).length > 0){
									return <div>
									{(()=>{
										let data_summary = Object.keys(this.state.summary_report).map((station_id)=>{
											return <div className="container text-center"> 
												<span className='bold'>{this.state.data[station_id].station_name}</span>
												<div className="table-wrapper top-margin">
													<div className="report-table-container">
														<table className="table table-bordered">
															<thead className="thead">
																<tr>
																	<th className="text-center"> Parameter </th>
																	<th className="text-center"> Avg </th>
																	<th className="text-center"> Avg Min </th>
																	<th className="text-center"> Avg Max</th>
																	<th className="text-center"> Min </th>
																	<th className="text-center"> Max </th>																	
																</tr>
															</thead>
															<tbody>
																{(()=>{
																	let table_row = this.state.checked_parameters.map((param)=>{
																		// console.log(param);
																		// if(this.state.data[station_id].required_params && this.state.data[station_id].required_params.indexOf(param) !== -1){
																		// 	let count = 0,average = 0,max_value=0,max_value_index = 0, min_value = 99999, min_value_index = 0, param_index = this.state.data[station_id].required_params.indexOf(param);
																		// 	// console.log(param_index,this.state.data[station_id].param_values[param_index]);
																		// 	this.state.data[station_id].param_values.map((param_value,index)=>{
																		// 		console.log(isNaN(param_value[param_index][0]),param_value[param_index][0]);
																		// 		if(param_value[param_index][0] !== null){
																		// 			count++;
																		// 			average += parseFloat(param_value[param_index][1]);
																		// 		}
																		// 		if(param_value[param_index][1] !== null && param_value[param_index][1]){
																		// 			if(min_value > parseFloat(param_value[param_index][1])){
																		// 				min_value = parseFloat(param_value[param_index][1]);
																		// 				min_value_index = index;
																		// 			}
																		// 		}
																		// 		if(param_value[param_index][2] !== null && param_value[param_index][2] != 0){
																		// 			if(max_value < parseFloat(param_value[param_index][2])){
																		// 				max_value = parseFloat(param_value[param_index][2]);
																		// 				max_value_index = index;
																		// 			}
																		// 		}
																		// 	});
																			
																		// 	return <tr>
																		// 	<td><span dangerouslySetInnerHTML={{__html:this.state.selected_parameters[param].name}} /> (<span dangerouslySetInnerHTML={{__html:this.state.selected_parameters[param].unit}} />)</td>
																		// 	<td> {(count > 0)?(average/count).toFixed(2):'NA'} </td>
																		// 	<td> {(min_value === 99999)?'NA':min_value} <br/> 
																		// 		<small>
																		// 			({(min_value === 99999)?'':moment.unix(parseInt(this.state.timestamps[min_value_index][0])).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix(parseInt(this.state.timestamps[min_value_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')})
																		// 		</small> 
																		// 	</td>
																		// 	<td> {(max_value === 0)? 'NA':max_value} <br/> 
																		// 	<small>
																		// 	({ (max_value === 0)?'':moment.unix(parseInt(this.state.timestamps[max_value_index][0])).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix(parseInt(this.state.timestamps[max_value_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')})																				
																		// 	</small> 
																		// 	</td>
																		// </tr>;
																		// }
																		console.log(param,this.state.summary_report[station_id]);			
																		if(this.state.summary_report[station_id] && this.state.summary_report[station_id][param] && this.state.summary_report[station_id][param] != null && Object.keys(this.state.summary_report[station_id][param]).length > 0){
																			return <tr>
																				<td><span dangerouslySetInnerHTML={{__html:this.state.selected_parameters[param].name}} /> (<span dangerouslySetInnerHTML={{__html:this.state.selected_parameters[param].unit}} />)</td>
																				<td> {(typeof(this.state.summary_report[station_id][param].avg) != 'undefined' && this.state.summary_report[station_id][param].avg != null && this.state.summary_report[station_id][param].avg > 0)?this.state.summary_report[station_id][param].avg.toFixed(2):'NA'} </td>
																				<td> {(typeof(this.state.summary_report[station_id][param].avg_min) != 'undefined' && this.state.summary_report[station_id][param].avg_min != null && this.state.summary_report[station_id][param].avg_min > 0)?this.state.summary_report[station_id][param].avg_min.toFixed(2):'NA'} <br/> 
																				{/*<small>
																					({(typeof(this.state.summary_report[station_id][param].avg_min_index) != 'undefined' && this.state.summary_report[station_id][param].avg_min_index != null && this.state.summary_report[station_id][param].avg_min_index > 0)?moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][0])).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY'):'NA'})
																				</small>*/}
																				{((()=>{
																					if(typeof(this.state.summary_report[station_id][param].avg_min_index) != 'undefined' && this.state.summary_report[station_id][param].avg_min_index != null && this.state.summary_report[station_id][param].avg_min_index >= 0){
																						if ((moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][0])).tz('Asia/Kolkata').format('YYYY')) == (moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][1])).tz('Asia/Kolkata').format('YYYY'))) {
																							
																							// console.log('Entered Year Condition');
																								if ((moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][0])).tz('Asia/Kolkata').format('MMM')) == (moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][1])).tz('Asia/Kolkata').format('MMM'))) {
																									// console.log('Entered Month Condition');
																									if ((moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][0])).tz('Asia/Kolkata').format('DD')) == (moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][1])).tz('Asia/Kolkata').format('DD'))) {
																										return <small> ( {moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][0])).tz('Asia/Kolkata').format('HH:mm')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')} )</small>;
																									} else {
																										return <small> ( {moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][0])).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')} )</small>;
																									}
																								} else {
																									// console.log('Entered Month Condition else');
																									return <small> ( {moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][0])).tz('Asia/Kolkata').format('HH:mm, DD MMM')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')} )</small>;
																								}
																						}else {
																								// console.log('Entered Year Condition else');
																							return <small> ( {moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][0])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_min_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')} ) </small>;
																						}
																					}else{
																						return <small>
																							-
																						</small>;
																					}																					
																				})())}
																				</td>
																				<td> {(typeof(this.state.summary_report[station_id][param].avg_max) != 'undefined' && this.state.summary_report[station_id][param].avg_max != null && this.state.summary_report[station_id][param].avg_max > 0)?this.state.summary_report[station_id][param].avg_max.toFixed(2):'NA'} <br/> 
																					{/*<small>
																						({(typeof(this.state.summary_report[station_id][param].avg_max_index) != 'undefined' && this.state.summary_report[station_id][param].avg_max_index != null && this.state.summary_report[station_id][param].avg_max_index > 0)?moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][0])).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY'):'NA'})
																					</small>*/}
																					{((()=>{
																						if(typeof(this.state.summary_report[station_id][param].avg_max_index) != 'undefined' && this.state.summary_report[station_id][param].avg_max_index != null && this.state.summary_report[station_id][param].avg_max_index >= 0){
																							if((moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][0])).tz('Asia/Kolkata').format('YYYY')) == (moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][1])).tz('Asia/Kolkata').format('YYYY'))) {
																								
																								// console.log('Entered Year Condition');
																								if ((moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][0])).tz('Asia/Kolkata').format('MMM')) == (moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][1])).tz('Asia/Kolkata').format('MMM'))) {
																									// console.log('Entered Month Condition');
																									if ((moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][0])).tz('Asia/Kolkata').format('DD')) == (moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][1])).tz('Asia/Kolkata').format('DD'))) {
																										return <small>( {moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][0])).tz('Asia/Kolkata').format('HH:mm')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')} )</small>;
																									} else {
																										return <small>( {moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][0])).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')} )</small>;
																									}
																								} else {
																									// console.log('Entered Month Condition else');
																									return <small>( {moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][0])).tz('Asia/Kolkata').format('HH:mm, DD MMM')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')} )</small>;
																								}
																							}else {
																									// console.log('Entered Year Condition else');
																								return <small>( {moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][0])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')+' - '+moment.unix(parseInt(this.state.timestamps[this.state.summary_report[station_id][param].avg_max_index][1])).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')} )</small>;
																							}
																						}else{
																							return <small>
																								-
																							</small>;
																						}																					
																					})())} 
																				</td>
																				<td> {(typeof(this.state.summary_report[station_id][param].min) != 'undefined' && this.state.summary_report[station_id][param].min != null && this.state.summary_report[station_id][param].min > 0)?this.state.summary_report[station_id][param].min.toFixed(2):'NA'} <br/> 
																					<small>
																						({(typeof(this.state.summary_report[station_id][param].min_time) != 'undefined' && this.state.summary_report[station_id][param].min_time != null && this.state.summary_report[station_id][param].min_time > 0)?moment.unix(parseInt(this.state.summary_report[station_id][param].min_time)).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY'):'NA'})																																				
																					</small>
													
																				</td>
																				<td> {(typeof(this.state.summary_report[station_id][param].max) != 'undefined' && this.state.summary_report[station_id][param].max != null && this.state.summary_report[station_id][param].max > 0)? this.state.summary_report[station_id][param].max:'NA'} <br/> 
																				<small>
																					({(typeof(this.state.summary_report[station_id][param].max_time) != 'undefined' && this.state.summary_report[station_id][param].max_time != null && this.state.summary_report[station_id][param].max_time > 0)?moment.unix(parseInt(this.state.summary_report[station_id][param].max_time)).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY'):'NA'})																	
																				</small>
																				</td>
																			</tr>;
																		}
																	}).filter(Boolean);
																	console.log(table_row);
																	return table_row; 
																})()}
															</tbody>
														</table>
													</div>
												</div>
											</div>;												
										});
										return data_summary;
									})()}
								</div>;
								}

							})()}
							{/* if graph is selected */}
							{(() => {
								if(this.state.data_format && this.state.data_format.indexOf('graph') != -1){
									return <div>
										{(() => {
											let data_graph = Object.keys(this.state.data).map((station_id) => {
												console.log('station id in render', station_id);
												if(this.state.selected_parameters){
													let station_name = this.state.data[station_id].station_name;
													return this.state.data[station_id].required_params.map((param_id,param_index) => {
														return <div className="container text-center">
															{(() => {
																let series = [],
																	report_config = {},
																	data_list = [],
																	avg_data=[];
																// if(this.state.data[station_id]){
																// 	Object.keys(this.state.data[station_id][]).map((timestamp) => {
																// 		if( Number(this.state.data[station_id][timestamp][index]) != NaN ){
																// 			/* catagories.push(moment.unix(timestamp).tz('Asia/Kolkata').format('HH:mm ,  DD MMM YYYY')); */
																// 			data_list.push([
																// 				parseInt(timestamp * 1000),
																// 				parseFloat(this.state.data[station_id][timestamp][index][1]),
																// 				parseFloat(this.state.data[station_id][timestamp][index][2])
																// 			]);
																// 			avg_data.push([
																// 				parseInt(timestamp * 1000),
																// 				parseFloat(this.state.data[station_id][timestamp][index][0])
																// 			]);
																// 		}
																// 	});
																// }
																if(this.state.timestamps){
																	this.state.timestamps.map((timestamp,time_index) => {
																		if(this.state.data[station_id].param_values[time_index] && this.state.data[station_id].param_values[time_index][param_index]){
																			let data_values = this.state.data[station_id].param_values[time_index][param_index];
																			avg_data.push([
																				parseInt(timestamp[1] * 1000),
																				(data_values[0] != null) ? parseFloat(data_values[0]) : null
																			]);
																			data_list.push([
																				parseInt(timestamp[1] * 1000),
																				(data_values[1] != null) ? parseFloat(data_values[1]) : null,
																				(data_values[2] != null) ? parseFloat(data_values[2]) : null
																			]);
																		}
																	});
																}

																//	console.log('catagories_array',catagories);
																//set up the series
																series = [{
																	color: this.graph_colors[param_index],
																	data: avg_data,
																	marker:{
																		fillColor: this.graph_fill_color[param_index],
																		lineColor: this.graph_colors[param_index],
																		lineWidth: 2,
																		radius: 0
																	},
																	name:'Average',
																	zIndex: 1
																},
																{
																	fillOpacity: 0.3,
																	lineWidth: 0,
																	linkedTo: 'previous',
																	name: 'Min - Max Range',
																	type: 'arearange',
																	color: this.graph_colors[param_index],
																	data: data_list
																}];
																/**
																* This is a object to plot trending graph data.
																* @type {Object}
																* @property {object} chart This sets chart properties.
																* @property {object} title This sets the title for the graph.
																* @property {object} xAxis This defines what to show in xAxis.
																* @property {object} yAxis This defines what to show in yAxis.
																* @property {object} credits This shows/hides highcharts logo.
																* @property {object} legend This hides the printing options from graph.
																* @property {object} tooltip This sets what to show in tooltip.
																* @property {object} plotOptions This customize the graph plot settings/options.
																* @property {Array} series This contains data for ploting graph.
																*/
															
																// console.log('Station_id', this.state.data[station_id]);
																// console.log('Station_parameter', this.state.data[station_id].param_thresholds);
																// console.log('station_thresholds', this.state.data[station_id].param_thresholds[param_index]);
																// console.log('station_danger_limit', this.state.data[station_id].param_thresholds[param_index][1]);
																// console.log('yoooooo', this.state.data[station_id].param_threshold_limits[param_index][1]);
																report_config = {
																	chart: {
																		zoomType:'x',
																		height: (this.state.view_mode === 'desktop') ? (window.innerHeight - 475) : 300,
																		backgroundColor: '#FFF'
																	},
																	// colors: ['#f4801f'],
																	title: {
																		text: this.state.selected_parameters[param_id].name + ' of ' + station_name,
																		style: {
																			fontSize: '16px'
																		}
																	},
																	xAxis: {
																		title:{
																			text:'Time'
																		},
																		crosshair:true,
																		type:'datetime'
																	},
																	yAxis: {
																		title: {
																			text: this.state.selected_parameters[param_id].unit
																		},
																		plotLines: [],
																		floor: 0
																	},
																	credits: {
																		enabled: false
																	},
																	legend: {
																		enabled: false
																	},
																	tooltip: {
																		crosshair:true,
																		shared:true,
																		borderRadius: 1,
																		valueSuffix: this.state.selected_parameters[param_id].unit,
																		valueSuffix: ' ' + this.state.selected_parameters[param_id].unit
																	},
																	series: series
																};

																// {
																// 	color: '#FF0000',
																// 	width: 1,
																// 	zIndex: 1,
																// 	value: (this.state.data[station_id].param_thresholds && this.state.data[station_id].param_thresholds != null && this.state.data[station_id].param_thresholds[param_index].ul_danger && this.state.data[station_id].param_thresholds[param_index].ul_danger != null )? this.state.data[station_id].param_thresholds[param_index].ul_danger : 0
																// }

																if(this.state.data[station_id].param_thresholds && this.state.data[station_id].param_thresholds !=null && this.state.data[station_id].param_thresholds[param_index] && this.state.data[station_id].param_thresholds[param_index] !== null){
																	report_config.yAxis['plotLines'][0] ={
																		color: '#FF0000',
																		width: 1,
																		zIndex: 1,
																		value: (this.state.data[station_id].param_thresholds[param_index].ul_danger && this.state.data[station_id].param_thresholds[param_index].ul_danger != null)? this.state.data[station_id].param_thresholds[param_index].ul_danger : 0
																	};
																	if(this.state.data[station_id].param_thresholds[param_index].ll_danger){
																		report_config.yAxis['plotLines'][1] ={
																			color: '#FF0000',
																			width: 1,
																			zIndex: 1,
																			value:(this.state.data[station_id].param_thresholds[param_index].ll_danger && this.state.data[station_id].param_thresholds[param_index].ll_danger != null)? this.state.data[station_id].param_thresholds[param_index].ll_danger : 0
																		};
																	}
																}

																// console.log(report_config);

																if(avg_data.length <= 0){
																	return <div className='report-graph-container'>
																		<h5> {station_name + ' data for ' + this.state.selected_parameters[param_id].long_name +'('+ this.state.selected_parameters[param_id].unit_noformat +')'} </h5>
																		<br/>
																		<h6 className='text-muted'> No data available within the selected interval </h6>
																		<br/>
																	</div>;
																}else{
																	return <div className='report-graph-container'>
																	<ReactHighcharts ref="archive_report_graph" config={report_config} />
																	</div>;
																}																
															})()}
													</div>;														
													}).filter(Boolean);
												}
											}).filter(Boolean);
											return data_graph;
										})()}
									</div>;
								}
							})()}

							{/* if grid is selected */}
							{(() => {
								if(this.state.data_format && this.state.data_format.indexOf('grid') != -1){
									return <div className="container text-center">
										<div className="table-wrapper">
											<div className="report-table-container">
											<table className="table table-bordered">
												<thead className="thead">
													{(() => {
														return <tr>
															<th className="text-center">Time</th>
															<th className="text-center">Station</th>
															{(() => {
																if (this.state.checked_parameters) {
																	let table_head = this.state.checked_parameters.map((param_id) => {
																		return <th colSpan={3} scope="colgroup" className="text-center" dangerouslySetInnerHTML={{__html:this.state.selected_parameters[param_id].name}}></th>;
																	}).filter(Boolean);
																	return table_head;
																}
															})()}
														</tr>;
													})()}
												</thead>
												<tbody>
													{(() => {
														return <tr>
															<td className="text-center">HH:MM - HH:MM, DD MMM YYYY</td>
															<td>&nbsp;</td>
															{(() => {
																if (this.state.checked_parameters) {
																	let table_row = this.state.checked_parameters.map((param_id) => {
																		return <td colSpan={3} scope="colgroup" className="text-center" > <span dangerouslySetInnerHTML={{__html:this.state.selected_parameters[param_id].unit}} /></td>;
																	}).filter(Boolean);
																	return table_row;
																}
															})()}
														</tr>;
													})()}
													{(() => {
														let col_array = ['Avg','Min','Max'];
														return <tr>
															<td>&nbsp;</td>
															<td>&nbsp;</td>
															{(() => {
																if (this.state.checked_parameters) {
																	let table_row = this.state.checked_parameters.map(() => {
																		return col_array.map((data) => {
																			//console.log('Threshold Name', data);
																			return <td scope="col">{data}</td>;
																		});
																	}).filter(Boolean);
																	return table_row;
																}
															})()}
														</tr>;
													})()}
													{(() => {
														if(this.state.timestamps){
															let old_timestamp = 0, current_time = moment().unix();
															let table_rows = this.state.timestamps.map((timestamp, index) => {
																if (timestamp[1] < current_time) {
																	return Object.keys(this.state.data).map((station_id) => {
																		let index_list = [0,1,2];
																		return <tr>
																			{(()=>{
																				// console.log('datetime_main', timestamp);
																				// console.log('datetime_current', current_time);
																				if(old_timestamp === timestamp){
																					return '';
																				}else{
																					old_timestamp = timestamp;
																					let row_span = this.state.selected_stations.length;
																					// console.log('datetime', timestamp[1]);
																					if ((moment.unix(timestamp[0]).tz('Asia/Kolkata').format('YYYY')) == (moment.unix((parseInt(timestamp[1]))).tz('Asia/Kolkata').format('YYYY'))) {
																						// console.log('Entered Year Condition');
																						if ((moment.unix(timestamp[0]).tz('Asia/Kolkata').format('MMM')) == (moment.unix((parseInt(timestamp[1]))).tz('Asia/Kolkata').format('MMM'))) {
																							// console.log('Entered Month Condition');
																							if ((moment.unix(timestamp[0]).tz('Asia/Kolkata').format('DD')) == (moment.unix((parseInt(timestamp[1]))).tz('Asia/Kolkata').format('DD'))) {
																								return <td rowSpan={row_span}>{moment.unix(timestamp[0]).tz('Asia/Kolkata').format('HH:mm')+' - '+moment.unix((parseInt(timestamp[1]))).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')}</td>;
																							} else {
																								return <td rowSpan={row_span}>{moment.unix(timestamp[0]).tz('Asia/Kolkata').format('HH:mm, DD')+' - '+moment.unix((parseInt(timestamp[1]))).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')}</td>;
																							}
																						} else {
																							// console.log('Entered Month Condition else');
																							return <td rowSpan={row_span}>{moment.unix(timestamp[0]).tz('Asia/Kolkata').format('HH:mm, DD MMM')+' - '+moment.unix((parseInt(timestamp[1]))).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')}</td>;
																						}
																					} else {
																						// console.log('Entered Year Condition else');
																						return <td rowSpan={row_span}>{moment.unix(timestamp[0]).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')+' - '+moment.unix((parseInt(timestamp[1]))).tz('Asia/Kolkata').format('HH:mm, DD MMM YYYY')}</td>;
																					}
																				}
																			})()}
																			<td className="min-width-name">
																				{this.state.data[station_id].station_name}
																			</td>
																			{(() => {
																				if (this.state.checked_parameters) {
																					let table_data = this.state.checked_parameters.map((param, ind) => {
																						// console.log(param, index);
																						return index_list.map((i) => {
																							if(this.state.data[station_id].required_params && this.state.data[station_id].required_params.indexOf(parseInt(param)) !== -1){
																								if(this.state.data[station_id].param_values[index] && this.state.data[station_id].param_values[index][this.state.data[station_id].required_params.indexOf(parseInt(param))][i] !== null){
																									// console.log(param+' '+i+' data '+index,this.state.data[station_id].param_values[index][this.state.data[station_id].required_params.indexOf(parseInt(param))][i]);
																									let param_value = this.state.data[station_id].param_values[index][this.state.data[station_id].required_params.indexOf(parseInt(param))][i];
																									let threshold_value = (this.state.data[station_id].param_thresholds && this.state.data[station_id].param_thresholds != null && this.state.data[station_id].param_thresholds[ind] != null && this.state.data[station_id].param_thresholds[ind].ul_danger && this.state.data[station_id].param_thresholds[ind].ul_danger != null)? this.state.data[station_id].param_thresholds[ind].ul_danger : null;
																									let threshold_value_lower = (this.state.data[station_id].param_thresholds && this.state.data[station_id].param_thresholds != null && this.state.data[station_id].param_thresholds[ind] != null && this.state.data[station_id].param_thresholds[ind].ll_danger && this.state.data[station_id].param_thresholds[ind].ll_danger != null)? this.state.data[station_id].param_thresholds[ind].ll_danger : null
																									// console.log(param_value);
																									if(i == 0){
																										// console.log('Param Value '+ param_value +', Threshold ', this.state.data[station_id].param_thresholds[ind][1]);
	
																										if((threshold_value != null && param_value > threshold_value) || (threshold_value_lower != null && param_value <= threshold_value_lower)){
																											// console.log(threshold_value);
																											return <td className="min-width-value fontcolor warning-cell">{param_value}</td>;
																										}
																									}
																									return <td className="min-width-value">{param_value}</td>;
																								}else{
																									return <td>{'NA'}</td>;
																								}
																							}else{
																								return <td>{'-'}</td>;	
																							}
																						}).filter(Boolean);
																					}).filter(Boolean);
																					return table_data;
																				}
																			})()}
																		</tr>;							
																	}).filter(Boolean);
																}
															}).filter(Boolean);
															return table_rows;
														}
													})()}
												</tbody>
											</table>
											</div>
										</div>							
									</div>;
								}
							})()}
						</div>;
					}
				})()}
			</Scrollbars>
		</div>;
	}
}

CustomReport.contextTypes = {
	router: React.PropTypes.object
};