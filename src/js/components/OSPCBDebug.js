import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import { Scrollbars } from 'react-custom-scrollbars';
import CommonDebugForm from './imports/CommonDebugForm';

/** 
 * SPCBDebug class
 */

export default class SPCBDebug extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			station_id: props.match.params.station_id
		};
	}

	componentDidMount() {
		document.title = 'OSPCB Debug - Datoms IoT Platform';

		this.get_spcb_debug_log();
	}

	checkTime() {
		if (parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')) < parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))) {
			return true;
		} else {
			this.setState({status_msg: 'Invalid date duration!'});
			return false;
		}
	}

	export_error_data(data_string) {
		window.open('##PR_STRING_REPLACE_API_BASE_PATH##download_spcb_error_data?d=' + JSON.stringify(data_string), '_self');
	}

	get_spcb_debug_log() {
		if(this.checkTime()) {
			var that = this;
			that.setState({
				data_loading: true,
				status_msg: null
			});
			let time_interval = [(parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))),(parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')))];
			if ((time_interval[1] - time_interval[0]) <= 86400) {
				fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_spcb_debug_log', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					credentials: 'include',
					body: JSON.stringify({
						station_id: that.props.match.params.station_id,
						time_interval: [time_interval[0], time_interval[1]]
					})
				}).then((response) => {
					return response.json();
				}).then((json) => {
					console.log('All Error Data', json);
					if (json.status === 'success') {
						that.setState({
							spcb_debug_data: json.spcb_debug_logs,
							error_data_count: json.error_count_percentage,
							data_loading: false
						});
					} else {
						that.setState({
							status_msg: json.message,
							data_loading: false
						});
					}
				}).catch((ex) => {
					console.log('parsing failed', ex);
					that.setState({
						status_msg: 'Unable to load data!',
						data_loading: false,
					});
				});
			} else {
				// showPopup('danger', 'Please select time duration for maximum 24 hours!');
				that.setState({
					status_msg: 'Please select time duration for maximum 24 hours!',
					data_loading: false
				});
			}
		}
	}

	render() {
		return(
			<div id="spcb-debug">
				<div className="spcb-debug-container">
					<Scrollbars autoHide>	
						<NavLink activeLink={'spcb'} />
						<CommonDebugForm {...this.state} get_error_log={() => this.get_spcb_debug_log()} export_error_data={(data_string) => this.export_error_data(data_string)} />
						{(() => {
							if (this.state.status_msg && this.state.status_msg !== null ) {
								return <div className="no-data-text">
									<center className="page-not-found">
										<h1>{this.state.status_msg}</h1>
									</center>
								</div>;
							} else if (this.state.data_loading) {
								return <div className="loading">
									<svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
								</div>;
							} else if (!this.state.data_loading && this.state.spcb_debug_data && Object.keys(this.state.spcb_debug_data).length){
								return <div className="container height-main-container">
									<div className="col-sm-4 full-height">
										<div className="panel panel-default height-panel">
											<div className="panel-heading table-header text-center">
												<span className="debug-table-header">Detected Errors</span>
											</div>
											<div className="panel-body fixed-height">
												<Scrollbars autoHide>
													<table className="table">
														<thead>
															<tr>
																<th>Name</th>
																<th>Count</th>
																<th>Percentage</th>
															</tr>
														</thead>
														<tbody>
															{(() => {
																let error_count =  Object.keys(this.state.error_data_count).map((data) => {
																	return <tr>
																		<td>{data}</td>
																		<td>{this.state.error_data_count[data].count}</td>
																		<td>{this.state.error_data_count[data].percent}</td>
																	</tr>;
																}).filter(Boolean);
																return error_count;
															})()}
														</tbody>
													</table>
												</Scrollbars>
											</div>
										</div>
									</div>
									<div className="col-sm-8 full-height">
										<div className="panel panel-default height-panel">
											<div className="panel-heading table-header text-center">
												<span className="debug-table-header">Error Log</span>
											</div>
											<div className="panel-body fixed-height">
												<Scrollbars autoHide>
													<table className="table">
														<thead>
															<tr>
																<th>Date Time</th>
																<th>Message</th>
															</tr>
														</thead>
														<tbody>
															{(() => {
																let debug_row = this.state.spcb_debug_data.map((debug, ind) => {
																	return <tr>
																		<td>{moment.unix(debug.timestamp).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss')}</td>
																		<td>{debug.message}</td>
																	</tr>;
																}).filter(Boolean);
																return debug_row;
															})()}
														</tbody>
													</table>
												</Scrollbars>
											</div>
										</div>
									</div>
								</div>;
							}else{
								return <div className="no-data-text">
									<center className="page-not-found">
										<h1>No Errors Found</h1>
									</center>
								</div>;
							}
						})()}
					</Scrollbars>
				</div>
			</div>
		);
	}
}

SPCBDebug.contextTypes = {
	router: React.PropTypes.object
};
