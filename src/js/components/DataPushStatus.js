import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import { Scrollbars } from 'react-custom-scrollbars';

/** 
 * DataPushStatus class
 */
export default class DataPushStatus extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentDidMount() {
		document.title = 'Data Push Status - Datoms IoT Platform';
		
		this.get_spcb_cpcb_status_of_devices();
		this.interval = setInterval(()=>{
			this.get_spcb_cpcb_status_of_devices();
		},60000);
	}

	get_spcb_cpcb_status_of_devices() {
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_spcb_cpcb_status_of_devices', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			body: JSON.stringify({})
		}).then((response) => {
			return response.json();
		}).then((json) => {
			console.log('All Error Data', json);
			if (json.status === 'success') {
				this.setState({
					cpcb_error_data: json.cpcb_error_data,
					spcb_error_data: json.spcb_error_data,
					cpcb_success_data: json.cpcb_success_data,
					spcb_success_data: json.spcb_success_data,
					data_loading: false
				});
			} else {
				this.setState({
					status_msg: json.message,
					data_loading: false
				});
			}
		}).catch((ex) => {
			console.log('parsing failed', ex);
			this.setState({
				status_msg: 'Unable to load data!',
				data_loading: false,
			});
		});
	}

	componentWillUnmount() {
		if (this.interval) {
			clearInterval(this.interval);
		}
	}

	render() {
		let client_slug = document.getElementById('client_slug') ? document.getElementById('client_slug').value : 'phoenix-robotix',
			base_path = ('##PR_STRING_REPLACE_BASE_PATH##' == 1) ? 'https://datoms.phoenixrobotix.com/enterprise/'+ client_slug +'/pollution-monitoring/vendor-portal' : '##PR_STRING_REPLACE_BASE_PATH##';

		return <div id="data-push-errors">
			<div className="data-push-errors-container">
				<Scrollbars autoHide>	
					<NavLink activeLink={'status'} />
					{(() => {
						if ((this.state.spcb_error_data && Object.keys(this.state.spcb_error_data).length) || (this.state.cpcb_error_data && Object.keys(this.state.cpcb_error_data).length)) {
							return <div className="container height-main-container">
								<div className="col-sm-6 full-height">
									<div className="panel panel-default half-height-panel">
										<div className="panel-heading table-header text-center">
											<span className="debug-table-header">SPCB Detected Errors</span>
										</div>
										{(() => {
											if (!this.state.data_loading && this.state.spcb_error_data && Object.keys(this.state.spcb_error_data).length) {
												return <div className="panel-body fixed-height">
													<Scrollbars autoHide>
														{(() => {
															let spcb_error_data = Object.keys(this.state.spcb_error_data).map((spcb) => {
																return <div>
																	<div className="list-label">{spcb}</div>
																	{(() => {
																		let spcb_data = this.state.spcb_error_data[spcb].map((org, id) => {
																			return <div className="flex">
																				<a className={'list-item icon-navlist-circle ' + org.spcb_status} target="_blank" href={base_path + '/stations/'+ org.loc_id +'/ospcb-debug'}>{org.loc_name}</a>
																				<small className="last-error">{'Last Error at ' + moment.unix(org.data_failure_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss')}</small>
																			</div>;
																		});
																		return spcb_data;
																	})()}
																	
																</div>;
															});
															return spcb_error_data;
														})()}
													</Scrollbars>
												</div>;
											} else if (this.state.spcb_error_data && this.state.spcb_error_data.length == 0) {
												return <div className="no-data-text">
													<center className="page-not-found">
														<h1>No Errors Found</h1>
													</center>
												</div>;
											} else {
												return <div className="loading">
													<svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
												</div>;
											} 
										})()}
									</div>
									<div className="panel panel-default half-height-panel">
										<div className="panel-heading table-header text-center">
											<span className="debug-table-header">SPCB Detected Success</span>
										</div>
										{(() => {
											if (!this.state.data_loading && this.state.spcb_success_data && Object.keys(this.state.spcb_success_data).length) {
												return <div className="panel-body fixed-height">
													<Scrollbars autoHide>
														{(() => {
															let spcb_success_data = Object.keys(this.state.spcb_success_data).map((spcb) => {
																return <div>
																	<div className="list-label">{spcb}</div>
																	{(() => {
																		let spcb_data = this.state.spcb_success_data[spcb].map((org, id) => {
																			return <div className="flex">
																				<a className={'list-item icon-navlist-circle ' + org.spcb_status} target="_blank" href={base_path + '/stations/'+ org.loc_id +'/ospcb-debug'}>{org.loc_name}</a>
																				<small className="last-error">{'Last Success at ' + moment.unix(org.data_failure_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss')}</small>
																			</div>;
																		});
																		return spcb_data;
																	})()}
																	
																</div>;
															});
															return spcb_success_data;
														})()}
													</Scrollbars>
												</div>;
											} else if (this.state.spcb_success_data && this.state.spcb_success_data.length == 0) {
												return <div className="no-data-text">
													<center className="page-not-found">
														<h1>No Success Found</h1>
													</center>
												</div>;
											} else {
												return <div className="loading">
													<svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
												</div>;
											} 
										})()}
									</div>
								</div>
								<div className="col-sm-6 full-height">
									<div className="panel panel-default half-height-panel">
										<div className="panel-heading table-header text-center">
											<span className="debug-table-header">CPCB Detected Errors</span>
										</div>
										{(() => {
											if (!this.state.data_loading && this.state.cpcb_error_data && Object.keys(this.state.cpcb_error_data).length) {
												return <div className="panel-body fixed-height">
													<Scrollbars autoHide>
														{(() => {
															let cpcb_error_data = Object.keys(this.state.cpcb_error_data).map((cpcb) => {
																return <div>
																	<div className="list-label">{cpcb}</div>
																	{(() => {
																		let cpcb_data = this.state.cpcb_error_data[cpcb].map((org, id) => {
																			return <div className="flex">
																				<a className={'list-item icon-navlist-circle ' + org.cpcb_status} target="_blank" href={base_path + '/stations/'+ org.loc_id +'/cpcb-debug'}>{org.loc_name}</a>
																				<small className="last-error">{'Last Error at ' + moment.unix(org.data_failure_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss')}</small>
																			</div>;
																		});
																		return cpcb_data;
																	})()}
																</div>;
															});
															return cpcb_error_data;
														})()}
													</Scrollbars>
												</div>;
											} else if (this.state.cpcb_error_data && this.state.cpcb_error_data.length == 0) {
												return  <div className="no-data-text">
													<center className="page-not-found">
														<h1>No Errors Found</h1>
													</center>
												</div>;
											} else {
												return <div className="loading">
													<svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
												</div>;
											} 
										})()}
									</div>
									<div className="panel panel-default half-height-panel">
										<div className="panel-heading table-header text-center">
											<span className="debug-table-header">CPCB Detected Success</span>
										</div>
										{(() => {
											if (!this.state.data_loading && this.state.cpcb_success_data && Object.keys(this.state.cpcb_success_data).length) {
												return <div className="panel-body fixed-height">
													<Scrollbars autoHide>
														{(() => {
															let cpcb_success_data = Object.keys(this.state.cpcb_success_data).map((cpcb) => {
																return <div>
																	<div className="list-label">{cpcb}</div>
																	{(() => {
																		let cpcb_data = this.state.cpcb_success_data[cpcb].map((org, id) => {
																			return <div className="flex">
																				<a className={'list-item icon-navlist-circle ' + org.cpcb_status} target="_blank" href={base_path + '/stations/'+ org.loc_id +'/cpcb-debug'}>{org.loc_name}</a>
																				<small className="last-error">{'Last Success at ' + moment.unix(org.data_failure_time).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss')}</small>
																			</div>;
																		});
																		return cpcb_data;
																	})()}
																</div>;
															});
															return cpcb_success_data;
														})()}
													</Scrollbars>
												</div>;
											} else if (this.state.cpcb_success_data && this.state.cpcb_success_data.length == 0) {
												return <div className="no-data-text">
													<center className="page-not-found">
														<h1>No Errors Found</h1>
													</center>
												</div>;
											} else {
												return <div className="loading">
													<svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
												</div>;
											} 
										})()}
									</div>
								</div>
							</div>;
						} else if (this.state.status_msg && this.state.status_msg !== null ) {
							return <div className="no-data-text">
								<center className="page-not-found">
									<h1>{this.state.status_msg}</h1>
								</center>
							</div>;
						} else if ((this.state.spcb_error_data && !Object.keys(this.state.spcb_error_data).length) || (this.state.cpcb_error_data && !Object.keys(this.state.cpcb_error_data).length)) {
							return <div className="no-data-text">
								<center className="page-not-found">
									<h1>Everything Running Fine</h1>
								</center>
							</div>;
						} else {
							return <div className="loading">
								<svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
							</div>;
						}
					})()}
				</Scrollbars>
			</div>
		</div>;
	}
}

DataPushStatus.contextTypes = {
	router: React.PropTypes.object
};