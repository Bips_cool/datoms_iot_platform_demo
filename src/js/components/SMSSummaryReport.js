import React from 'react';
import NavLink from './NavLink';
import ReportsNavMenu from './imports/ReportsNavMenu';
import SMSReportsForm from './imports/SMSReportsForm';
import { Scrollbars } from 'react-custom-scrollbars';
import moment from 'moment-timezone';
import _ from 'lodash';
import ReactHighcharts from 'react-highcharts';

export default class SMSSummaryReport extends React.Component {
	constructor(props) {
		super(props);
		this.state={
			selected_stations: [],
			raw: false,
			avg: false,
			raw_report:null,
			all_stations_list: null,
			all_parameters: null,
			// checked_parameters: null,
			data: null,
			data_loading: null,
			average_over_time: null,
			all_industries: null,
			sms_report_steps: [
				{
					selector: '#each_row',
					content: 'Stations',
				},
				{
					selector: '#prameters',
					content:  'Parameters',
				},
				{
					selector: '#time_interval',
					content:  'Time Interval',
				},
				{
					selector: '#data_format',
					content:  'View Data Format',
				},
				{
					selector: '#report_type',
					content:  'Report type',
				},
				{
					selector: '#view_button',
					content:  'View Data',
				},
				{
					selector: '#download_button',
					content:  'Download Data',
				}
			],
			isTourOpen: false
		};

		// this.getStationList();
	}

	getStationList(){
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_all_stations_and_params_for_archive_report.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			body: JSON.stringify({
				industry_id: (document.getElementById('client_id')) ? document.getElementById('client_id').value : 310
			})
		}).then((response) => {
			return response.json();
		}).then((json) => {
			console.log('Stations:', json);
			if (json.status === 'success') {
				that.sanitizeIndustrySelection(json.all_industries,json.all_parameters);
			} else {
				showPopup('danger', json.message);
				// that.setState({loading_data: null});
			}
		}).catch((ex) => {
			console.log('parsing failed', ex);
			showPopup('danger', 'Unable to load data!');
		});
	}

	sanitizeIndustrySelection(all_industries,all_parameters){
		let state_list = [],industry_type=[],industry_list={};
		Object.keys(all_industries).map((key)=>{
			if(industry_type.indexOf(all_industries[key].category) === -1 && all_industries[key].category !== undefined && all_industries[key].category !== null && all_industries[key].category !== ''){
				industry_type.push(all_industries[key].category);
			}
			if(state_list.indexOf(all_industries[key].state) === -1 && all_industries[key].state !== undefined && all_industries[key].state !== null && all_industries[key].state !== ''){
				state_list.push(all_industries[key].state);
			}
			industry_list[all_industries[key].id] = all_industries[key];
		});

		// console.log('state list',state_list);
		// console.log('industry type',industry_type);
		// console.log('industry list',industry_list);

		let state_list_object={},industry_type_object={},select_industry_list={};

		state_list.map((state)=>{
			state_list_object[state] = state;
		});

		industry_type.map((type)=>{
			industry_type_object[type] = type;
		});

		Object.keys(industry_list).map((id)=>{
			select_industry_list[id] =industry_list[id].name;
		});

		console.log('select_industry_list',select_industry_list);
		console.log('industry_list',industry_list);

		this.setState({
			industry_list:industry_list,
			state_list:state_list_object,
			industry_type:industry_type_object,
			all_parameters:all_parameters,
		});

	}

	setIndustryId(value){
		let selected_value=value,that=this;
		

		let all_station_list = null,selected_industry=(selected_value != null)?this.state.industry_list[selected_value]:null;

		if(selected_industry != null){
			all_station_list = {};
			all_station_list.aaqms = selected_industry.stations[1];
			all_station_list.cems = selected_industry.stations[2];
			all_station_list.eqms = selected_industry.stations[3];			
		}

		console.log('selected indutry',selected_industry);		
		console.log('selected value',selected_value);
		console.log('all_station_list m',all_station_list);

		this.setState({
			all_stations_list:all_station_list
		});
	}


	getSMSReport(form_details){
		console.log('form details',form_details);
		this.setIndustryId(form_details.industry_id);
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_violation_summary_report_data_of_industry.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				industry_id: form_details.industry_id,
				station_ids: form_details.station_ids,
				parameters: form_details.parameters,
				time_interval: form_details.time_interval,
				data_type: form_details.data_type,
				average_over_time_base: form_details.average_over_time_base
			})
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log('Get data:', json);
			if(json.status === 'success') {
				//Do stuffs here
				delete json.status;
				that.setState({
					data_loading: false,
					data: json,
					average_over_time: form_details.average_over_time_base,
					selected_stations: form_details.station_ids,
					parameters: form_details.parameters,
					raw: false,
					avg: false
				}, ()=>{
					if(form_details.data_type === 'raw'){
						that.setState({
							raw:true
						});
					}else if(form_details.data_type === 'avg'){
						that.setState({
							avg:true
						});
					}

					that.reconfigureStationData();

					// console.log('Get data with type:', that.state.data);
				});
			} else {
				//Show error message
			}
		});
	}

	reconfigureStationData(){
		let grid_data = this.state.data.grid,station_data={};
		console.log('reconfiguring data ',grid_data);
		if(grid_data && grid_data.length > 0){
			grid_data.map((data)=>{
				if(!station_data[data.station]){
					station_data[data.station] = [];
				}
				station_data[data.station].push(data);
			});
		}
		console.log('reconfigured data ',station_data);

		this.setState({
			raw_report:station_data
		});
	}

	renderSMSReport(form_details){
		let that = this;
		this.setState({
			data_loading:true,
			data:{},
			raw: false,
			avg:false
		}, ()=>{
			that.getSMSReport(form_details);
		});
	}

	getStationName(id){
		let that = this,station_name='';
		Object.keys(that.state.all_stations_list).map((key)=>{
			that.state.all_stations_list[key].map((station)=>{
				if(station.id == id){
					console.log('station name',station.name);
					station_name=station.name;
				}
			});
		});
		return station_name;
	}

	componentDidMount() {
		document.title = 'Violation Summary Report -  DATOMS® Pollution Monitoring';
		this.getStationList();
	}

	render() {
		return <div>
			<Scrollbars autoHide>		
			<NavLink activeLink={'reports'} />			
				<div className="report-container">
					<ReportsNavMenu activeLink={'sms'} />
					<SMSReportsForm {...this.state} ref={(child) => {this._child = child;}} getReport={(form_details)=>{this.renderSMSReport(form_details);}} />
				</div>

				{(()=>{
					if(this.state.data_loading){
						return <div  className="text-center">
							<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
						</div>;
					}
					else{
						return <div>
							{/* raw data */}
							{(()=>{
								if(this.state.raw && this.state.data.grid){
									return <div>
										{/* summary report */}

										{(()=>{
											return <div className="container text-center">
												<div className="table-wrapper">
													<div className="report-table-container">
														<table className="table table-bordered">
															{/* table header */}
															<thead>
																{(()=>{
																	return <tr>
																		<th> Station </th>
																		<th colSpan={2} scope="colgroup" >Total</th>
																		{(()=>{
																			let table_head = this.state.parameters.map((param_id)=>{
																				return <th colSpan={2} scope="colgroup"><span dangerouslySetInnerHTML={{__html:this.state.all_parameters[param_id].prm_name}} /></th>;
																			});
																			return table_head;
																		})()}
																	</tr>;
																})()}
																{/* table body 1st row */}
																{(()=>{
																	let pre = true;
																	return <tr>
																	<th></th>
																	<th scope="col"> Warning </th>
																	<th scope="col"> Danger </th>
																	{(()=>{
																		let table_data = this.state.parameters.map((param_id)=>{
																			if(pre){
																				pre = false;
																				return <th scope="col"> Warning </th>;				
																			}else{
																				pre = true;
																				return <th scope="col"> Danger </th>;	
																			}
																		}).filter(Boolean);
																		return table_data;
																	})()}
																	{(()=>{
																		let table_data = this.state.parameters.map((param_id)=>{
																			if(pre){
																				pre = false;
																				return <th scope="col"> Warning </th>;		
																			}else{
																				pre = true;
																				return <th scope="col"> Danger </th>;						
																			}
																		}).filter(Boolean);
																		return table_data;
																	})()}
																	</tr>;
																})()}
															</thead>
															<tbody>
																{(()=>{
																	return Object.keys(this.state.data.graph).map((station_id)=>{
																		return <tr>
																			{(()=>{
																				return Object.keys(this.state.all_stations_list).map((key)=>{
																					return this.state.all_stations_list[key].map((station)=>{
																						// console.log(station_id,station);
																						if(station.id == station_id){
																							return <td>{station.name}</td>;
																						}
																					}).filter(Boolean);
																				}).filter(Boolean);						
																			})()}
																			{(()=>{
																				let data_set = [];
																				let	param_index = 2;
																				let total_warning=0;
																				let total_danger=0;
			
																				Object.keys(this.state.parameters).map((param_id)=>{
																					data_set[param_index++] = this.state.data.graph[station_id][this.state.parameters[param_id]][0];
																					data_set[param_index++] = this.state.data.graph[station_id][this.state.parameters[param_id]][1];
																					total_warning+=this.state.data.graph[station_id][this.state.parameters[param_id]][0];
																					total_danger+=this.state.data.graph[station_id][this.state.parameters[param_id]][1];
																				});
																				data_set[0]=total_warning;
																				data_set[1]=total_danger;
			
																				return data_set.map((data)=>{
																					return <td>{data}</td>;
																				});
																			})()}
																		</tr>;
																	});
																})()}
															</tbody>
														</table>
													</div>
												</div>
											</div>;											
										})()}

										{/* grid of all station */}
										{(()=>{
											if(this.state.raw_report && Object.keys(this.state.raw_report).length > 0){
												return Object.keys(this.state.raw_report).map((station_name)=>{
													return <div className="container text-center">
													<div className="table-wrapper">
														<div className="table-header">{station_name}</div>
														<div className="report-table-container">
															<table className="table table-bordered">
																<thead>
																	<tr>
																		<th>Time</th>
																		<th>Station Name</th>
																		<th>Parameter Name</th>
																		<th>Type</th>
																		<th>Parameter Value</th>
																		<th>Threshold Value</th>
																	</tr>
																</thead>
																<tbody>
																	{(()=>{
																		let sorted_grid  = _.sortBy(this.state.raw_report[station_name], 'timestamp');
																		let grid_row = sorted_grid.map((data)=>{
																			return <tr>
																				<td>{moment.unix(data.timestamp).tz('Asia/Kolkata').format('HH:mm -  DD MMM YYYY')}</td>
																				<td>{data.station}</td>
																				<td><span dangerouslySetInnerHTML={{__html:data.param}} /></td>
																				<td>{data.type}</td>
																				<td>{data.value} <span dangerouslySetInnerHTML={{__html:data.unit}} /></td>
																				{(()=>{
																					if(data.threshold){
																						return <td>
																						{data.threshold} <span dangerouslySetInnerHTML={{__html:data.unit}} />
																						</td>;
																					}else{
																						return <td>
																							-
																						</td>;
																					}
																				})()}						
																			</tr>;
																		}).filter(Boolean);
																		return grid_row;
																	})()}
																</tbody>
															</table>
														</div>
													</div>
												</div>;
												});
											}else{
												return <div>
												<div className="text-muted margin-no-data"> No Records Found </div>
												</div>;
											}										
										})()}
									</div>;
								}
							})()}
						</div>;
					}
				})()}
			</Scrollbars>
		</div>;
	}
}

SMSSummaryReport.contextTypes = {
	router: React.PropTypes.object
};