import React from 'react';
import NavLink from './NavLink';
import io from 'socket.io-client';
import moment from 'moment-timezone';
import { Link } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';
import _ from 'lodash';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Devices extends React.Component {
	constructor(props) {
		super();
		this.state = {
			devices: null,
			industries: null,
			applications: null,
			device_filter: '',
			open_filter: false,
			open_station_configure: false,
			modal_sync_view: false,
			loading_modal: 0,
			vendor_id: (document.getElementById('vendor_id')) ? document.getElementById('vendor_id').value : 1,
			health_status: {
				0: 'OK',
				1: 'Any',
				2: 'Power Failure',
				3: 'Code Restart',
				4: 'Modem Restart',
				5: 'Other Error',
				6: 'Battery Voltage Down',
				7: 'Network Signal Low',
				8: 'Debug Data Not Sent'
			},
			device_id: null,
			device_name: null,
			industry_id: null
		};
	}

	getStationDetails(device_details) {
		this.setState({
			open_station_configure: true,
			device_id: device_details.id,
			device_name: device_details.name,
			industry_id: device_details.industry_id,
			loading_modal: 1
		});
		console.log('Modal Open');
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_stations_for_device', {
			credentials: 'include',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				vendor_id: (document.getElementById('vendor_id')) ? document.getElementById('vendor_id').value : 1,
				device_id: device_details.id
			})
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log('stations under Device', json);
			if (json.status === 'success') {
				that.setState({
					assigned_stations: json.assigned_stations,
					unassigned_stations: json.unassigned_stations,
					loading_modal: 0
				});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	saveStationsUnderDevice() {
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'save_stations_under_device', {
			credentials: 'include',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				vendor_id: that.state.vendor_id,
				device_id: that.state.device_id,
				stations_under_device: Object.keys(that.state.assigned_stations)
			})
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			// console.log('Stations_list:', json);
			if (json.status === 'success') {
				showPopup('success', 'Successfully Updated');
				that.closeModal();
			} else {
				showPopup('danger', json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	closeModal() {
		this.setState({
			open_station_configure: false,
			device_id: null,
			device_name: null,
			industry_id: null
		});
	}

	deviceSyncingStatus() {
		this.setState({modal_sync_view: false});
		if (
			_.find(this.state.devices, { 'id': this.state.sync_device_id, 'sync_status': 'in_sync'}) || _.find(this.state.device_details, { 'id': this.state.sync_device_id, 'sync_status': 'not_in_sync'})
			) {
			this.socket.emit('sync_settings_to_device', JSON.stringify({
				device_id: this.state.sync_device_id
			}));
			this.setState({sync_index: null});
		} else {
			this.socket.emit('stop_syncing_settings_to_device', JSON.stringify({
				device_id: this.state.sync_device_id
			}));
			this.setState({sync_index: null});
		}
	}

	changeStationUnderDeviceStatus(station_id, key) {
		let unassigned_stations = this.state.unassigned_stations;
		let assigned_stations = this.state.assigned_stations;
		if (key == 'assigned_stations') {
			unassigned_stations[station_id] = this.state.assigned_stations[station_id];
			delete assigned_stations[station_id];
			this.setState({
				unassigned_stations: unassigned_stations
			});
			console.log('unassigned_stations', unassigned_stations);
		} else {
			assigned_stations[station_id] = this.state.unassigned_stations[station_id];
			delete unassigned_stations[station_id];
			this.setState({
				assigned_stations: assigned_stations
			});
			console.log('assigned_stations', assigned_stations);
		}
	}

	getApplications() {
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_industry_and_application_names_of_vendor.php', {
			credentials: 'include',
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				vendor_id: that.state.vendor_id,
			})
		}).then(function(response) {
			return response.json();
		}).then(function(json) {
			console.log('Customers:', json);
			if (json.status === 'success') {
				that.setState({
					industries: json.industries,
					applications: json.applications
				});
			} else {
				showPopup('danger',json.message);
			}
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger','Unable to load data!');
		});
	}

	changeDeviceStatus(id,data_sending_status) {
		if (this.state.devices) {
			let devices_updated_status = this.state.devices;
			devices_updated_status.map((value,key) => {
				if (value && id == value.id) {
					devices_updated_status[key]['data_sending_status'] = data_sending_status;
				}
			});
			this.setState({devices: devices_updated_status});
		}
		this.socket.emit('change_device_data_sending_status', 
			JSON.stringify({
				device_id: id,
				data_sending_status: data_sending_status
			})
		);
	}

	componentDidMount() {
		console.log('Component Mounted Successfully');
		document.title = 'Devices - Datoms IoT Platform';
		
		this.socket = io('##PR_STRING_REPLACE_SOCKET_BASE_PATH##');
		this.socket.on('connect', () => {
			this.socket.emit('connect_dashboard_to_socket', JSON.stringify({
				vendor_id: this.state.vendor_id
			}));
			console.log('connected to socket');
		});

		this.socket.on('dashboard_successfully_connected', () => {
			console.log('dashboard_successfully_connected');
			this.getApplications();
			this.socket.emit('get_list_of_devices', JSON.stringify({
				vendor_id: this.state.vendor_id
			}));
		});

		this.socket.on('update_list_of_devices', (payload) => {
			let list_of_devices = JSON.parse(payload);
			console.log('update_list_of_devices', list_of_devices);
			this.setState({ devices: list_of_devices.devices });
		});

		this.socket.on('update_location_details_in_dashboard', (payload) => {
			let data_status = JSON.parse(payload);
			if (this.state.devices) {
				let status_change = this.state.devices;
				status_change.map((stat,index) => {
					if (stat && data_status.device_id == stat.id) {
						Object.keys(data_status).map((key) => {
							if (key != 'id') {
								status_change[index][key] = data_status[key];
							}
						});
					}
				});
				this.setState({devices: status_change});
			}
			console.log('Dashboard successfully connected to socket');
			console.log('payload', data_status);
		});

		this.socket.on('show_error', (payload) => {
			let error_msg = JSON.parse(payload);
			console.log('show_error', error_msg);
			showPopup('error', error_msg.message);
		});
	}

	componentWillUnmount() {
		this.socket.close();
	}

	componentDidUpdate() {
		ReactTooltip.rebuild();
	}

	render() {
		return <div className="">
		<Scrollbars autoHide>
			<NavLink activeLink={'devices'} />
				{(() => {
					if (this.state.devices && this.state.devices.length === 0) {
						return <div className="no-data-text">No Devices to show</div>;
					} else if (this.state.devices && this.state.industries && this.state.applications) {
						let dev_length = this.state.devices.map((device, index) => {
							if ((this.state.device_filter == '') || ((this.state.industries[device.industry_id] + ' ' + this.state.applications[device.application_id] + ' ' + device.name + ' ' + device.type + ' ' + device.qr_code + ' ' + device.firmware_version).toLowerCase().search(this.state.device_filter.toLowerCase()) >= 0)) {
								return device.id;
							}
						}).filter(Boolean).length;

						return <div className="device-list-container">
							<div className="industry-details-list">
								<div className="industry-details-container">
									<div className="no-of-device">
										<h4 className="title-text">
											Number of Devices: {dev_length}
										</h4>
									</div>
									<input className="form-control search-box-devices-list" placeholder="Search Device" onChange={(e) => this.setState({device_filter: e.target.value})}/>
									{/*<span className="svg-filter" onClick={(e) => this.setState({open_filter: true})} data-tip={'Click to filter'}>
										<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill='#666' d="M10 18h4v-2h-4v2zM3 6v2h18V6H3zm3 7h12v-2H6v2z"/></svg>
									</span>*/}
									{/*(() => {
										if (this.state.open_filter) {
											return <div className="form-horizontal bg-box background-box" ref="filterOptionsPanel">
												<button type="button" className="close close-icon" onClick={(e) => this.setState({open_filter: false})}>&times;</button>
												<fieldset>
													<div className="form-group">
														<label className="col-lg-3 control-label label-font">City:</label>
														<div className="col-lg-5">
															<select className="form-control select-style" >
																<option value={0}>Any</option>
																	<option value={1}>device 1</option>
																	<option value={1}>device 2</option>
																	<option value={1}>device 3</option>
															</select>
														</div>
													</div>
													<div className="form-group">
														<label className="col-lg-3 control-label label-font">Connection Status:</label>
														<div className="col-lg-5">
															<select className="form-control select-style">
																<option value={0}>Any</option>
																<option value={'online'}>Online</option>
																<option value={'offline'}>Offline</option>
															</select>
														</div>
													</div>
													<div className="form-group">
														<label className="col-lg-3 control-label label-font">Sync Status:</label>
														<div className="col-lg-5">
															<select className="form-control select-style" >
																<option value={0}>Any</option>
																<option value={'in_sync'}>In sync</option>
																<option value={'syncing'}>Syncing</option>
																<option value={'waiting_for_device'}>Waiting for device</option>
															</select>
														</div>
													</div>
													<div className="form-group">
														<label className="col-lg-3 control-label label-font">Health Status:</label>
														<div className="col-lg-5">
															<select className="form-control select-style" >
																<option value={1}>Heath ok</option>
																<option value={1}>Heath not-ok</option>
																<option value={1}>Heath good</option>
															</select>
														</div>
													</div>
													<div className="form-group">
														<label className="col-lg-3 control-label label-font">Data Sending:</label>
														<div className="col-lg-5">
															<select className="form-control select-style" >
																<option value={0}>Any</option>
																<option value={'on'}>On</option>
																<option value={'off'}>Off</option>
															</select>
														</div>
													</div>
													<div className="form-group btn-top-margin">
														<div className="col-lg-3 btn-group">
															<button className="btn btn-default btn-margin border-radious">Reset</button>
															<button className="btn btn-primary border-radious">Apply</button>
														</div>
													</div>
												</fieldset>
											</div>;
										}
									})()*/}
								</div>
								<div className="panel panel-default">
									<table className="table details-list">
										<thead>
											<tr>
												<th>Sl.</th>
												<th>QR Code</th>
												<th>Industry Name</th>
												<th>Device Name</th>
												<th>Product</th>
												<th>Application</th>
												<th>Firmware</th>
												<th>Active</th>
												<th>Sync</th>
												<th>Health</th>
												<th>Configure</th>
												<th>Data Status</th>
											</tr>
										</thead>
										<tbody>
											{(() => {
												return this.state.devices.map((device, index) => {
													let industry_name = this.state.industries[device.industry_id],
														application = this.state.applications[device.application_id],
														search_string = industry_name + ' ' + application + ' ' + device.name + ' ' + device.type + ' ' + device.qr_code + ' ' + device.firmware_version,
														last_online_timestamp = (device.last_online_timestamp ? moment.unix(device.last_online_timestamp).tz('Asia/Kolkata').format('DD MMM YYYY - hh:mm:ss') : 'Never'),
														last_data_receive_timestamp = (device.last_data_receive_timestamp ? moment.unix(device.last_data_receive_timestamp).tz('Asia/Kolkata').format('DD MMM YYYY - hh:mm:ss') : 'Never');
													if ((this.state.device_filter == '') || (search_string.toLowerCase().search(this.state.device_filter.toLowerCase()) >= 0)) {
														return <tr key={index}>
															<td>{(index + 1)}</td>
															<td>{device.qr_code}</td>
															<td className="hellip">{industry_name}</td>
															<td className="hellip">{device.name}</td>
															<td>{device.type}</td>
															<td className="hellip">{application}</td>
															<td className="text-center">{device.firmware_version}</td>
															<td>
																<div className={'center device-status' + ((device.status == "data_off") ? ' data-off' : ((device.status == "online") ? ' active' : ''))} data-tip={(device.status == "data_off") ? 'Last data received at ' + last_data_receive_timestamp : ((device.status == "online") ? 'Online' : ((last_online_timestamp == 'Never') ? 'Device never connected' : 'Last active at ' + last_online_timestamp))}>
																	<div className="circle center" />
																	{(device.status == "online") ? 'Online' : ((device.status == "data_off") ? last_data_receive_timestamp : last_online_timestamp)}
																</div>
															</td>
															<td className="text-center syncing-status" onClick={(e) => this.setState({modal_sync_view: true, sync_device_id: device.id})}>
																{(() => {
																	switch (device.sync_status) {
																		case 'in_sync':
																			return <div className="sync-status" data-tip="In Sync">
																				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="green" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31-132.6 78-7.8-3.8-16.4-5.7-25.5-5.7-32.8 0-59.8 24.4-64 56C47 152.3 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="green" d="M374.7 144.5L353.2 123 231 245.2l-38.5-38.5-21 21 50.6 51 .8-.2 9.2 9.3"/></svg>
																			</div>;
																			break;
																		case 'not_in_sync':
																			return <div className="sync-status" data-tip="Device not in sync">
																				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="red" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="red" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm83.6 52.2h21v-21h-21zM363.8 124H301v62.7l23-23c11.4 10.4 18.7 26 18.7 43.8 0 27.2-17.8 50-41.8 59.5v22c35.2-9.4 62.3-41.8 62.3-80.4 0-23-9.4-44-25-58.5zM259 218h21v-62.7h-21z"/></svg>
																			</div>;
																			break;
																		case 'syncing':
																			return <div className="sync-status" data-tip="Syncing...">
																				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="#FF830D" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="#FF830D" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm188-83.5h-62.7v62.7l23-23c11.5 10.4 18.8 26 18.8 43.8 0 27.2-17.8 50-41.8 59.5v22c35.5-9.4 62.6-41.8 62.6-80.4 0-23-9.4-44-25-58.5z"/></svg>
																			</div>;
																			break;
																		case 'waiting_for_device':
																			return <div className="sync-status" data-tip="Waiting for device to connect...">
																				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 542 343" width="20px" height="20px"><path fill="none" stroke="#800080" strokeWidth="30" d="M444.4 328c45.6 0 82.6-37 82.6-82.6 0-39.2-27.3-72-64-80.5-.7-83-68-150-151.2-150-57 0-106.8 31.3-132.6 78-7.8-3.5-16.4-5.4-25.5-5.4-32.8 0-59.8 24.4-64 56C47 152.4 15 190 15 235c0 51.4 41.6 93 93 93h336.4z"/><path fill="#800080" d="M175.5 207.5c0 23 9.4 44 25 58.5l-25 25h62.7v-62.6l-23 23c-11.5-10.5-18.8-26-18.8-44 0-27 17.8-50 41.8-59.4v-20.8c-35.5 9.3-62.7 41.7-62.7 80.3zm188-83.5h-62.7v62.7l23-23c11.5 10.4 18.8 26 18.8 43.8 0 27.2-17.8 50-41.8 59.5v22c35.5-9.4 62.6-41.8 62.6-80.4 0-23-9.4-44-25-58.5z"/></svg>
																			</div>;
																			break;
																	}
																})()}
															</td>
															{(() => {
																if (device.health_status == 0) {
																	return <td className="text-center" data-tip={this.state.health_status[device.health_status] + ' - View Debug Log'}>
																		<Link to={'/devices/' + device.id + '/debug/view/table'} className="device-error-log">
																			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448.8 448.8" width="16"><path fill="#18BC9C" d="M142.8 324l-107-107.2L0 252.4l142.8 143 306-306-35.7-36"/></svg>
																		</Link>
																	</td>;
																} else {
																	return <td className="text-center" data-tip={this.state.health_status[device.health_status] + ' - View Debug Log'}>
																		<Link to={'/devices/' + device.id + '/debug/'} className={'device-error-log debug-' + device.health_status}>
																			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 286.1 286.1" width="16"><path fill="#F25A59" d="M143 0C64 0 0 64 0 143s64 143 143 143 143-64 143-143S222 0 143 0zm0 259.2c-64.2 0-116.2-52-116.2-116.2S78.8 26.8 143 26.8s116.2 52 116.2 116.2-52 116.2-116.2 116.2zm0-196.5c-10.2 0-18 5.4-18 14V156c0 8.6 7.8 14 18 14 10 0 18-5.7 18-14V76.6c0-8.4-8-14-18-14zm0 125c-9.8 0-17.8 8-17.8 18 0 9.7 8 17.7 17.8 17.7s18-8 18-17.8c0-10-8-18-18-18z"/></svg>
																		</Link>
																	</td>;
																}
															})()}
															<td>
																<div className="configure-device" data-tip={'Configure Device'} onClick={(e) => this.getStationDetails(device)}>
																	<svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 932.179 932.179">
																		<path d="M61.2 341.538c4.9 16.8 11.7 33 20.3 48.2l-24.5 30.9c-8 10.1-7.1 24.5 1.9 33.6l42.2 42.2c9.1 9.1 23.5 9.9 33.6 1.9l30.7-24.3c15.8 9.1 32.6 16.2 50.1 21.2l4.6 39.5c1.5 12.8 12.3 22.4 25.1 22.4h59.7c12.8 0 23.6-9.602 25.1-22.4l4.4-38.1c18.8-4.9 36.8-12.2 53.7-21.7l29.7 23.5c10.1 8 24.5 7.1 33.6-1.9l42.2-42.2c9.1-9.1 9.9-23.5 1.9-33.6l-23.1-29.3c9.6-16.602 17.1-34.3 22.1-52.8l35.6-4.1c12.8-1.5 22.4-12.3 22.4-25.1v-59.7c0-12.8-9.6-23.6-22.4-25.1l-35.1-4.1c-4.8-18.3-12-35.8-21.2-52.2l21.6-27.3c8-10.1 7.1-24.5-1.9-33.6l-42.1-42.1c-9.1-9.1-23.5-9.9-33.6-1.9l-26.5 21c-17.2-10.1-35.6-17.8-54.9-23l-4-34.3c-1.5-12.8-12.3-22.4-25.1-22.4h-59.7c-12.8 0-23.6 9.6-25.1 22.4l-4 34.3c-19.8 5.3-38.7 13.3-56.3 23.8l-27.5-21.8c-10.1-8-24.5-7.1-33.6 1.9l-42.2 42.2c-9.1 9.1-9.9 23.5-1.9 33.6l23 29.1c-9.2 16.6-16.2 34.3-20.8 52.7l-36.8 4.2c-12.8 1.5-22.4 12.3-22.4 25.1v59.7c0 12.8 9.6 23.6 22.4 25.1l38.8 4.5zm216.3-161.5c54.4 0 98.7 44.3 98.7 98.7s-44.3 98.7-98.7 98.7c-54.4 0-98.7-44.3-98.7-98.7s44.3-98.7 98.7-98.7z"/>
																		<path d="M867.7 356.238l-31.5-26.6c-9.7-8.2-24-7.8-33.2.9l-17.4 16.3c-14.7-7.1-30.3-12.1-46.4-15l-4.898-24c-2.5-12.4-14-21-26.602-20l-41.1 3.5c-12.6 1.1-22.5 11.4-22.9 24.1l-.8 24.4c-15.8 5.7-30.7 13.5-44.3 23.3l-20.8-13.8c-10.6-7-24.7-5-32.9 4.7l-26.6 31.7c-8.2 9.7-7.8 24 .9 33.2l18.2 19.4c-6.3 14.2-10.8 29.1-13.4 44.4l-26 5.3c-12.4 2.5-21 14-20 26.6l3.5 41.1c1.1 12.6 11.4 22.5 24.1 22.9l28.1.9c5.102 13.4 11.8 26.1 19.9 38l-15.7 23.7c-7 10.6-5 24.7 4.7 32.9l31.5 26.6c9.7 8.2 24 7.8 33.2-.9l20.6-19.3c13.5 6.3 27.7 11 42.3 13.8l5.7 28.2c2.5 12.4 14 21 26.6 20l41.1-3.5c12.6-1.1 22.5-11.4 22.9-24.1l.9-27.602c15-5.3 29.2-12.5 42.3-21.4l22.7 15c10.6 7 24.7 5 32.9-4.7l26.6-31.5c8.2-9.7 7.8-24-.9-33.2l-18.3-19.398c6.7-14.2 11.602-29.2 14.4-44.6l25-5.1c12.4-2.5 21-14 20-26.602l-3.5-41.1c-1.1-12.6-11.4-22.5-24.1-22.9l-25.1-.8c-5.2-14.6-12.2-28.4-20.9-41.2l13.7-20.6c7.2-10.598 5.2-24.798-4.5-32.998zm-154.9 237.6c-44.4 3.8-83.6-29.3-87.3-73.7-3.8-44.4 29.3-83.6 73.7-87.3 44.4-3.8 83.6 29.3 87.3 73.7 3.8 44.4-29.3 83.6-73.7 87.3zM205 704.438c-12.6 1.3-22.3 11.9-22.4 24.6l-.3 25.3c-.2 12.7 9.2 23.5 21.8 25.1l18.6 2.4c3.1 11.3 7.5 22.1 13.2 32.3l-12 14.8c-8 9.9-7.4 24.1 1.5 33.2l17.7 18.1c8.9 9.1 23.1 10.1 33.2 2.3l14.9-11.5c10.5 6.2 21.6 11.102 33.2 14.5l2 19.2c1.3 12.6 11.9 22.3 24.6 22.4l25.3.3c12.7.2 23.5-9.2 25.1-21.8l2.3-18.2c12.6-3.1 24.6-7.8 36-14l14 11.3c9.9 8 24.1 7.4 33.2-1.5l18.1-17.7c9.1-8.898 10.1-23.1 2.302-33.2l-10.702-13.9c6.6-11 11.7-22.7 15.2-35l16.6-1.7c12.6-1.3 22.3-11.9 22.4-24.6l.3-25.3c.2-12.7-9.2-23.5-21.8-25.1l-16.2-2.1c-3.1-12.2-7.7-24-13.7-35l10.1-12.4c8-9.9 7.4-24.1-1.5-33.2l-17.698-18.1c-8.9-9.102-23.102-10.102-33.2-2.3l-12.102 9.3c-11.4-6.9-23.6-12.2-36.4-15.8l-1.6-15.7c-1.3-12.602-11.9-22.3-24.6-22.4l-25.3-.3c-12.7-.2-23.5 9.2-25.1 21.8l-2 15.6c-13.2 3.4-25.9 8.6-37.7 15.4l-12.5-10.2c-9.9-8-24.1-7.4-33.2 1.5l-18.2 17.8c-9.1 8.9-10.1 23.1-2.3 33.2l10.7 13.8c-6.2 11-11.1 22.7-14.3 35l-17.5 1.8zm163.3-28.6c36.3.4 65.4 30.3 65 66.6-.4 36.3-30.3 65.4-66.6 65-36.3-.4-65.4-30.3-65-66.6.4-36.3 30.3-65.4 66.6-65z"/>
																	</svg>
																</div>
															</td>
															<td>
																{(() => {
																	if (device.data_sending_status == 'wait') {
																		return <div className="onoffswitch">
																			<div className="inline-loading center" data-tip={'Waiting for Response from Device'}>
																				<svg className="loading-spinner" width="20px" height="20px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
																			</div>
																		</div>;
																	} else {
																		return <div className="onoffswitch" data-tip={'Toggle device data sending'}>
																			<input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id={'myonoffswitch ' + (device.id)} defaultChecked={(device.data_sending_status == 'on') ? true : false} onChange={() => this.changeDeviceStatus(device.id,(device.data_sending_status == 'on') ? 'off' : 'on')}/>
																			<label className="onoffswitch-label" htmlFor={'myonoffswitch ' + (device.id)}>
																				<span className="onoffswitch-inner"></span>
																				<span className="onoffswitch-switch"></span>
																			</label>
																		</div>;
																	}
																})()}
															</td>
														</tr>;
													}
												}).filter(Boolean);
											})()}
										</tbody>
									</table>
								</div>
								{(() => {
									if (this.state.open_station_configure) {
										return <div className="modal">
											<Scrollbars autoHide>
												<div className="modal-dialog">
													<div className="modal-content">
														<div className="modal-header">
															<button type="button" className="close" onClick={() => this.closeModal()}>
																<svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 21.9 21.9">
																		<path fill="black" d="M14.1 11.3c-.2-.2-.2-.5 0-.7l7.5-7.5c.2-.2.3-.5.3-.7s-.1-.5-.3-.7L20.2.3c-.2-.2-.5-.3-.7-.3-.3 0-.5.1-.7.3l-7.5 7.5c-.2.2-.5.2-.7 0L3.1.3C2.9.1 2.6 0 2.4 0s-.5.1-.7.3L.3 1.7c-.2.2-.3.5-.3.7s.1.5.3.7l7.5 7.5c.2.2.2.5 0 .7L.3 18.8c-.2.2-.3.5-.3.7s.1.5.3.7l1.4 1.4c.2.2.5.3.7.3s.5-.1.7-.3l7.5-7.5c.2-.2.5-.2.7 0l7.5 7.5c.2.2.5.3.7.3s.5-.1.7-.3l1.4-1.4c.2-.2.3-.5.3-.7s-.1-.5-.3-.7l-7.5-7.5z"/>
																	</svg>
															</button>
															<h4 className="modal-title">{'Stations under ' + this.state.device_name}</h4>
														</div>
														<div className="modal-body" id="modal_content">
														{(() => {
															if (this.state.loading_modal) {
																return <div className="modal-body">
																	<div className="loading"><svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>
																	</div>;
															} else {
																return <div className="col-lg-12 col-md-12 col-sm-12">
																	{(() => {
																		if (Object.keys(this.state.assigned_stations).length || Object.keys(this.state.unassigned_stations).length ) {
																			return <div>
																				<div className="row station-header">Assigned_stations Stations</div>
																				{(() => {
																					if (Object.keys(this.state.assigned_stations).length ) {
																						let assigned_stations = Object.keys(this.state.assigned_stations).map((key) => {
																							return <div className="row">
																								<div className="flex">
																								{(() => {
																									if (Object.keys(this.state.assigned_stations).includes(key)) {
																										return <input type="checkbox" id={'device_' + key} className="all-check" checked={true} onChange={() => this.changeStationUnderDeviceStatus(key, 'assigned_stations')}/>;
																									} else {
																										return <input type="checkbox" id={'device_' + key} className="all-check" checked={false} onChange={() => this.changeStationUnderDeviceStatus(key, 'assigned_stations')}/>;
																									}
																								})()}		
																									<label className="station-label" htmlFor={'device_' + key}>{this.state.assigned_stations[key]}</label>
																								</div>
																							</div>;
																						});
																						return assigned_stations;
																					} else {
																						return <div className="no-stations">No assagined stations found under this device.</div>
																					}
																				})()}
																				<div className="row station-header">Unassaigned Stations</div>
																				{(() => {
																					if (Object.keys(this.state.unassigned_stations).length ) {
																						let unassigned_stations = Object.keys(this.state.unassigned_stations).map((key) => {
																							return <div className="row">
																								<div className="flex">
																									<input type="checkbox" id={'device_' + key} className="all-check" checked={Object.keys(this.state.assigned_stations).includes(parseInt(key))} onChange={() => this.changeStationUnderDeviceStatus(key, 'unassigned_stations')} />
																									<label className="station-label" htmlFor={'device_' + key}>{this.state.unassigned_stations[key]}</label>
																								</div>
																							</div>;
																						});
																						return unassigned_stations;
																					} else {
																						return <div className="no-stations">No unassagined stations found under this device.</div>
																					}
																				})()}
																			</div>;
																		} else {
																			return <div className="no-stations">No stations found under this device.</div>
																		}
																	})()}
																</div>;
															}
														})()}
														</div>
														<div className="modal-footer">
															<button type="button" className="btn btn-cancel" onClick={() => this.closeModal()}>Cancel</button>
															<button type="button" className="btn btn-primary btn-save" disabled={this.state.submitting_formdata ? true : false} onClick={() => this.saveStationsUnderDevice()}>Save</button>
														</div>
													</div>
												</div>
											</Scrollbars>
										</div>;
									}
								})()}
								{(() => {
									if (this.state.modal_sync_view) {
										return <div className="modal">
											<div className="modal-dialog">
												<div className="modal-content">
													<div className="modal-header modal-header-add">
														<button type="button" className="close" onClick={() => this.setState({modal_sync_view: false})}>
															<svg xmlns="http://www.w3.org/2000/svg" width="13" height="13" viewBox="0 0 21.9 21.9">
																<path fill="black" d="M14.1 11.3c-.2-.2-.2-.5 0-.7l7.5-7.5c.2-.2.3-.5.3-.7s-.1-.5-.3-.7L20.2.3c-.2-.2-.5-.3-.7-.3-.3 0-.5.1-.7.3l-7.5 7.5c-.2.2-.5.2-.7 0L3.1.3C2.9.1 2.6 0 2.4 0s-.5.1-.7.3L.3 1.7c-.2.2-.3.5-.3.7s.1.5.3.7l7.5 7.5c.2.2.2.5 0 .7L.3 18.8c-.2.2-.3.5-.3.7s.1.5.3.7l1.4 1.4c.2.2.5.3.7.3s.5-.1.7-.3l7.5-7.5c.2-.2.5-.2.7 0l7.5 7.5c.2.2.5.3.7.3s.5-.1.7-.3l1.4-1.4c.2-.2.3-.5.3-.7s-.1-.5-.3-.7l-7.5-7.5z"/>
															</svg>
														</button>
														<h4 className="modal-title-add">Sync Settings</h4>
													</div>
													<div className="modal-body add-modal-body">
														{(() => {
															if (
																_.find(this.state.devices, { 'id': this.state.sync_device_id, 'sync_status': 'in_sync'}) || _.find(this.state.device_details, { 'id': this.state.sync_device_id, 'sync_status': 'not_in_sync'})
																) {
																return <label className="col-lg-12 sync-body">Sync settings to device ?</label>;
															} else {
																return <label className="col-lg-12 sync-body">Stop sync settings to device ?</label>;
															}
														})()}
													</div>
													<div className="modal-footer border-zero">
														<button type="button" className="btn btn-default" onClick={(e) => this.setState({modal_sync_view: false})} >Cancel</button>
														{(() => {
															if (
																_.find(this.state.devices, { 'id': this.state.sync_device_id, 'sync_status': 'in_sync'}) || _.find(this.state.device_details, { 'id': this.state.sync_device_id, 'sync_status': 'not_in_sync'})
																) {
																return <button type="button" className="btn btn-primary" onClick={(e) => this.deviceSyncingStatus()}>Sync</button>;
															} else {
																return <button type="button" className="btn btn-primary" onClick={(e) => this.deviceSyncingStatus()}>Stop Syncing</button>;
															}
														})()}
													</div>
												</div>
											</div>
										</div>;
									}
								})()}
							</div>
							<ReactTooltip effect="solid" />
						</div>;
					} else {
						return <div className="loading"><svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg></div>;
					}
				})()}
			</Scrollbars>
		</div>;
	}
}

Devices.contextTypes = {
	router: React.PropTypes.object
};
