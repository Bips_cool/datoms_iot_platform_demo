import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import { Scrollbars } from 'react-custom-scrollbars';

/** 
 * DMDDebug class
 */

export default class DMDDebug extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	componentDidMount() {
		document.title = 'DMD Debug - Datoms IoT Platform';
	}

	render() {
		return(
			<div id="dmd-debug">
				<div className="dmd-debug-container">
					<Scrollbars autoHide>	
						<NavLink activeLink={'dmd'} />
					</Scrollbars>
				</div>
			</div>
		);
	}
}

DMDDebug.contextTypes = {
	router: React.PropTypes.object
};
