import React from 'react';
import NavLink from './NavLink';
import moment from 'moment-timezone';
import { Scrollbars } from 'react-custom-scrollbars';
import { Link } from 'react-router-dom';
import DebugForm from './imports/DebugForm';
import ReactHighcharts from 'react-highcharts';
import Circle from './imports/Circle';
import DebugErrorsTable from './imports/DebugErrorsTable';
import _ from 'lodash';

/** 
 * DeviceDebug class
 */

export default class DeviceDebug extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			debug_data: null,
			time_interval: null,
			status_msg: null,
			active_view: props.match.params.view_mode ? props.match.params.view_mode : 'table',
			device_id: props.match.params.device_id,
			device_online_status: null,
			data_loading: true,
			status_list:[],
			error_count_percent:null,
			detected_errors:[],
			error_log:[],
			sorted_detected_log:null,
			sorted_error_log:null,
			error_data:null,
			device_online_graph:null,
			device_online_percentage:null
		};

		this.color_list = {
			success:{
				fill_color:'#70f174',
				border_color:'#378633'
			},
			warning:{
				fill_color:'#f5f525',
				border_color:'#908506'
			},
			danger:{
				fill_color:'#f76055',
				border_color:'#7d0d05'
			},
			none:{
				border_color:'#ddd'
			},
		};
	}

	checkTime() {
		if (parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')) < parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))) {
			return true;
		} else {
			this.setState({status_msg: 'Invalid date duration!'});
			return false;
		}
	}

	initialize_logs(){
		let error_log = this.state.error_data,
			detected_log = this.state.error_count_percent,
			serialize_error_log=[],
			serialize_detected_log=[];

		Object.keys(error_log).map((timestamp)=>{
			serialize_error_log.push({
				timestamp:timestamp,
				error:error_log[timestamp].error,
				actions:error_log[timestamp].actions
			});
		});

		// console.log('log ->>',detected_log);

		Object.keys(detected_log).map((name)=>{
			serialize_detected_log.push({
				name:name,
				count:detected_log[name].count,
				percent:detected_log[name].percent
			});
			// console.log('the detected error',serialize_detected_log);
		});

		// console.log('the detected error',serialize_detected_log);

		this.setState({
			detected_errors:serialize_detected_log,
			error_log:serialize_error_log,
			sorted_detected_log:null,
			sorted_error_log:null
		});
	}

	get_device_debug_data(id) {
		if(this.checkTime()) {
			var that = this;
			that.setState({data_loading: true});
			let time_interval = [(parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))),(parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')))];

			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_device_debug_data', {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				credentials: 'include',
				body: JSON.stringify({
					device_id: that.props.match.params.device_id,
					// device_id:1,
					// time_interval: [1510374600,1510381800]
					time_interval: [time_interval[0], time_interval[1]]
				})
			}).then((response) => {
				return response.json();
			}).then((json) => {
				console.log('All Error Data', json);
				if (json.status === 'success') {
					that.setState({
						error_count_percent:json.error_count_percentage,
						error_data:json.error_data,
						device_online_graph:json.device_online_graph_data,
						device_online_percentage:json.device_online_percent,
						data_loading: false
					},()=>{
						that.initialize_logs();
					});
				} else {
					that.setState({
						status_msg: json.message,
						data_loading: false
					});
				}
			}).catch((ex) => {
				console.log('parsing failed', ex);
				that.setState({
					status_msg: 'Unable to load data!',
					data_loading: false,
				});
			});
		}
	}

	setActiveView(){
		this.setState({
			active_view: this.props.match.params.view_mode ? this.props.match.params.view_mode : 'table'
		});
	}

	componentDidMount() {
		document.title = 'Debug - Datoms IoT Platform';

		this.get_device_debug_data();
	}

	demoStatus(){
		let that = this,status_list = this.state.status_list;
		
		for(let i=0;i<100;i++){
			if(i%3 === 0)
				status_list.push([moment().format('x'),0]);
			else
				status_list.push([moment().format('x'),1]);
		}

		that.setState({
			status_list:status_list
		});
	}

	render() {
		ReactHighcharts.Highcharts.setOptions({
			global: {
				useUTC: false
			}
		});

		return(
			<div id="device-debug">
				<div className="debug-container">
					<Scrollbars autoHide>	
						<NavLink activeLink={'devices'} />
						<DebugForm {...this.state} get_device_error_log={(id) => this.get_device_debug_data(id)} get_device_online_status={() => this.get_device_online_status()} />
					
						{(() => {
							if(this.state.status_msg && this.state.status_msg !== null ) {
								return <div className="no-data-text">
									<center className="page-not-found">
										<h1>{this.state.status_msg}</h1>
									</center>
								</div>;
							} else if(this.state.data_loading) {
								return <div className="loading">
									<svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
								</div>;
							}else if(!this.state.data_loading && this.state.device_online_percentage !== null && this.state.device_online_graph !== null && Object.keys(this.state.device_online_graph).length > 0){
								return <div className="container height-main-container">
									<div className="row">
										<div className="col-sm-3">
											<div className="panel panel-default">
												<div className="panel-body">
													{(()=>{
														if(this.state.device_online_percentage !== null){
															let condition = '', percent_value=this.state.device_online_percentage;
															if (parseInt(percent_value) == 0) {
																condition = 'none';
															} else {
																if(parseInt(percent_value) >= 60 && parseInt(percent_value) < 90 ){
																	condition = 'warning';
																}else if(parseInt(percent_value) >= 90){
																	condition = 'success';
																}else{
																	condition = 'danger';
																}
															}
															return <div>
																<Circle parcent={percent_value} size='100' lineWidth='10' data_set='cirle1' fill_color={this.color_list[condition].fill_color} border_color={this.color_list[condition].border_color}  />
																<div className="violation-title"> Online Time </div>
															</div>;
														}else{
															return <div className="violation-title"> No Data Available </div>
														}
													})()}
												</div>
											</div>
										</div>
										<div className="col-sm-9">
											<div className="panel panel-default">
												<div className="panel-body default-height">
													{(()=>{
														if(this.state.device_online_graph !== null && this.state.device_online_graph.length > 0){
															let report_config = {
																chart: {
																	zoomType: 'x',
																	height: 135,
																	type: 'area',
																	backgroundColor: '#FFF'
																},
																title: {
																	text: '',
																	style: {
																		fontSize: '14px'
																	}
																},
																xAxis: {
																	type: 'datetime',
																	text: 'Date'
																},
																yAxis: {
																	visible:false,
																	max: 1
																},
																credits: {
																	enabled: false
																},
																legend: {
																	enabled: false
																},
																tooltip: {
																	formatter: function() {
																		return '<b>' + moment((this.point.x / 1000),'X').tz('Asia/Kolkata').format('DD-MM-YYYY HH:mm:ss') + '</b><br/>' + (this.point.y ? (this.point.error ? 'Error: ' + this.point.error.error + (this.point.error.actions.length ? '<br/>Actions: ' + this.point.error.actions.join(', ') : '') : 'Device is Online') : 'Device is Offline');
																	}
																},
																plotOptions: {
																	series:{
																		linecap: 'square',
																		turboThreshold: 0,
																		lineWidth: 0
																	}
																},
																series: [
																	{
																		data: this.state.device_online_graph,
																		states: {
																			hover: {
																				enabled: false
																			}
																		}
																	}
																]
															};
															return <ReactHighcharts ref="sms_report_graph" config={report_config} />;
														}else{
															return <div>
																<h4 className="text-muted"> No Data Available</h4>
															</div>;
														}
													})()}
												</div>
											</div>
										</div>
									</div>
									{(() => {
										if (this.state.error_data && Object.keys(this.state.error_data).length && this.state.error_count_percent && Object.keys(this.state.error_count_percent).length) {
											console.log('DebugErrorsTable');
											return <DebugErrorsTable error_data={this.state.error_data} error_count_percent={this.state.error_count_percent} />;
										} else {
											console.log('DebugErrorsTable', this.state);
										}
									})()}
								</div>;
							}else{
								return <div className="no-data-text">No Data to Show</div>;
							}
						})()}
					</Scrollbars>
				</div>
			</div>
		);
	}
}

DeviceDebug.contextTypes = {
	router: React.PropTypes.object
};