import React from 'react';
import ReactDOM from 'react-dom';
import ReactTooltip from 'react-tooltip';
import { Router } from 'react-router';
import DateTime from 'react-datetime';
import { Scrollbars } from 'react-custom-scrollbars';
import moment from 'moment';
import NavLink from './NavLink';


var FromDateTime = React.createClass({
	displayName: 'FromDateTime',
	getInitialState: function() {
		return {fromDate: moment().subtract(1, 'day')};
	},
	handleChange: function(date) {
		this.setState({fromDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="from_date_time" dateFormat="DD-MM-YYYY" timeFormat="HH:mm" defaultValue={that.state.fromDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

var UptoDateTime = React.createClass({
	displayName: 'UptoDateTime',
	getInitialState: function() {
		return {uptoDate: moment()};
	},
	handleChange: function(date) {
		this.setState({uptoDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="upto_date_time" dateFormat="DD-MM-YYYY" timeFormat="HH:mm" defaultValue={that.state.uptoDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});



/**
 * This is the main class of Dashboard page.
 */
export default class Dashboard extends React.Component {
	constructor() {
		super();

		this.state = {
			user_filter_value: '',
			open_filter: false,
			filter_state: false,
			pin_it: false,
			panel_open: true,
			have_firmware_version: 0
		};
		this.all_stations = [
			{
				"id": "1",
				"name": "DATOMS Device 1",
				"lat": "22.393259",
				"long": "83.850961",
				"connection_status": "offline"
			},
			{
				"id": "2",
				"name": "DATOMS Device 2",
				"lat": "22.595638",
				"long": "83.526049",
				"connection_status": "offline"
			},
			{
				"id": "5",
				"name": "DATOMS Device 3",
				"lat": "22.233826",
				"long": "82.559810",
				"connection_status": "online"
			},
			{
				"id": "74",
				"name": "DATOMS Device 4",
				"lat": "22.100910",
				"long": "84.704590",
				"connection_status": "shutdown"
			},
			{
				"id": "77",
				"name": "DATOMS Device 5",
				"lat": "21.915676",
				"long": "81.189823",
				"connection_status": "offline"
			},
			{
				"id": "78",
				"name": "DATOMS Device 6",
				"lat": "22.083120",
				"long": "80.448932",
				"connection_status": "offline"
			},
			{
				"id": "79",
				"name": "DATOMS Device 7",
				"lat": "22.403441",
				"long": "73.509692",
				"connection_status": "offline"
			},
			{
				"id": "80",
				"name": "DATOMS Device 8",
				"lat": "22.512592",
				"long": "70.343441",
				"connection_status": "offline"
			},
			{
				"id": "81",
				"name": "DATOMS Device 9",
				"lat": "20.099826",
				"long": "84.494518",
				"connection_status": "offline"
			},
			{
				"id": "82",
				"name": "DATOMS Device 10",
				"lat": "20.207700",
				"long": "83.970796",
				"connection_status": "shutdown"
			},
			{
				"id": "162",
				"name": "DATOMS Device 11",
				"lat": "22.231535",
				"long": "85.031535",
				"connection_status": "online"
			},
			{
				"id": "163",
				"name": "DATOMS Device 12",
				"lat": "21.118085",
				"long": "85.301387",
				"connection_status": "shutdown"
			},
			{
				"id": "164",
				"name": "DATOMS Device 13",
				"lat": "18.932551",
				"long": "83.451564",
				"connection_status": "shutdown"
			},
			{
				"id": "165",
				"name": "DATOMS Device 14",
				"lat": "19.259581",
				"long": "78.692295",
				"connection_status": "online"
			}
		];
	}

	fetchDeviceData(device_id) {
		if (device_id) {
			console.log('Station Id:',device_id);
			var that = this;
			that.setState({
				panel_open: true,
				device_data: null
			});
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##' + 'get_data_of_device.php', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				credentials: 'include',
				body: JSON.stringify({
					device_id: device_id
				})
			}).then(function(Response) {
				return Response.json();
			}).then(function(json) {
				console.log('Station Data:',json);
				if (json.status === 'success') {
					that.setState({
						station_id: station_id,
						station_data: json,
						station_type: json.loc_type,
						calling: true
					});
				} else {
					showPopup('danger',json.message);
				}
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				showPopup('danger','Unable to load data!');
			});
		}
	}

	componentDidMount() {
		window.react_container.addEventListener('click', (event) => this.closeFilterOptions(event));

		document.title = 'Dashboard - Datoms IoT Platform';
		console.log('all_stations', this.all_stations);

		this.map_view = true;
		var mapProp= {
			center:new google.maps.LatLng(23.678543, 78.234321),
			zoom:5,
		};
		/**
		 * This is used for generating google map.
		 */
		this.map=new google.maps.Map(document.getElementById('mapView'),mapProp);

		this.all_stations.map((point) => {
			var marker_lat_long = new google.maps.LatLng(point.lat, point.long);
			console.log(point.lat, point.long);
			var marker_icon = '';

			if (point.connection_status == 'online') {
				marker_icon = 'https://dev.datoms.io/binay/location-icon/location-status-1.png';
			} else if (point.connection_status == 'offline') {
				marker_icon = 'https://dev.datoms.io/binay/location-icon/location-status-6.png';
			} else {
				marker_icon = 'https://dev.datoms.io/binay/location-icon/location-status-0.png';
			}

			var marker = new google.maps.Marker({
				position: marker_lat_long,
				map: this.map,
				title: point.name,
				icon: marker_icon
			});
			// console.log('marker_icon', marker);

			var that = this;
			google.maps.event.addListener(marker,'click',function() {
				// that.fetchDeviceData(point.id);
				// that.context.transitionTo('/'+point.id);
				console.log(point.id);
			});
		});
	}
	 
	/**
	 * This Perform any necessary cleanup in this method, such as invalidating timers, canceling network requests, or cleaning up any DOM elements that were created in componentDidMount.
	 */
	componentWillUnmount() {
		window.react_container.removeEventListener('click', (event) => this.closeFilterOptions(event));
	}

	resetFilter() {
		this.setState({
			city_id: '0',
			connection: '0',
			sync: '0',
			health: '0',
			data_sending: '0',
			unconfigured: '0',
			no_data_received: '0',
			publicly_accessible: '0'
		});
	}

	closeFilterOptions(event) {
		const filterOptionsPanel = ReactDOM.findDOMNode(this.refs.filterOptionsPanel);
		if (filterOptionsPanel && !filterOptionsPanel.contains(event.target)) {
			this.setState({ open_filter: false });
		}
	}

	render() {
		return(
			<div className="full-page-container">
				<Scrollbars autoHide>
					<NavLink activeLink={''} />
					<div id="main_container">
						<div className="search-bar-filter">
							<div className="search-bar">
								<span className="search-svg">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill="#666" d="M15.5 14h-.8l-.3-.3c1-1 1.6-2.6 1.6-4.2C16 6 13 3 9.5 3S3 6 3 9.5 6 16 9.5 16c1.6 0 3-.6 4.2-1.6l.3.3v.8l5 5 1.5-1.5-5-5zm-6 0C7 14 5 12 5 9.5S7 5 9.5 5 14 7 14 9.5 12 14 9.5 14z" />
									</svg>
								</span>
								<input type="text" className="form-control search-box" placeholder="Search by User name, email, mobile no. or reports to" onChange={(e) => this.setState({user_filter_value: e.target.value})}/>
								{(() => {
									if (this.state.open_filter) {
										return <div className="form-horizontal bg-box background-box" ref="filterOptionsPanel">
											<button type="button" className="close close-icon" onClick={(e) => this.setState({open_filter: false})}>&times;</button>
											<fieldset>
												<div className="form-group">
													<label className="control-label label-font">Firmware Version:</label>
													<div className="select-form firmware-select">
														<select className="form-control select-style" onChange={(e) => this.setState({have_firmware_version: e.target.value})}>
															<option value={0} selected disabled>Select</option>
															<option value={1}>Having</option>
															<option value={2}>Not Having</option>
														</select>
													</div>
													{(() => {
														if (this.state.have_firmware_version == 1) {
															return <div className="select-form firmware-version">
																<select className="form-control select-style" onChange={(e) => this.setState({firmware_version: e.target.value})}>
																	<option value={0} selected disabled>Select Version</option>
																	<option value={'Latest'}>Latest</option>
																	<option value={'1.0.0'}>1.0.0</option>
																	<option value={'1.1.0'}>1.1.0</option>
																	<option value={'1.2.0'}>1.2.0</option>
																	<option value={'1.3.0'}>1.3.0</option>
																</select>
															</div>;
														}
													})()}
												</div>
												<div className="form-group">
													<label className="control-label label-font">Status:</label>
													<div className="select-form">
														<select className="form-control select-style" onChange={(e) => this.setState({status: e.target.value})}>
															<option value={0} selected disabled>Select</option>
															<option value={'online'}>Online</option>
															<option value={'offline'}>Offline</option>
															<option value={'shutdown'}>Shutdown</option>
															<option value={'any'}>Any</option>
														</select>
													</div>
												</div>
												<div className="form-group">
													<label className="control-label label-font">Manufactured:</label>
													<div className="select-form manufacture-select">
														<select className="form-control select-style" onChange={(e) => this.setState({manufactured: e.target.value})}>
															<option value={0} selected disabled>Select</option>
															<option value={'before'}>Before</option>
															<option value={'afetr'}>After</option>
														</select>
													</div>
													{/*(() => {
														if (this.state.manufactured == 'before') {
															return <div className="manufacture-date-time">
																<UptoDateTime />
															</div>;
														} else {
															return <div className="manufacture-date-time">
																<FromDateTime />
															</div>;
														}
													})()*/}
												</div>
												<div className="form-group">
													<label className="control-label label-font">Assembled:</label>
													<div className="select-form assemble-select">
														<select className="form-control select-style" onChange={(e) => this.setState({assembled: e.target.value})}>
															<option value={0} selected disabled>Select</option>
															<option value={'before'}>Before</option>
															<option value={'afetr'}>After</option>
														</select>
													</div>
												</div>
												<div className="form-group">
													<label className="control-label label-font">Deployed:</label>
													<div className="select-form deploy-select">
														<select className="form-control select-style" onChange={(e) => this.setState({deployed: e.target.value})}>
															<option value={0} selected disabled>Select</option>
															<option value={'before'}>Before</option>
															<option value={'afetr'}>After</option>
														</select>
													</div>
												</div>
												<div className="form-group">
													<label className="control-label label-font">Health status:</label>
													<div className="select-form health-status-select">
														<select className="form-control select-style" onChange={(e) => this.setState({health_status: e.target.value})}>
															<option value={0} selected disabled>Select</option>
															<option value={1}>any</option>
															<option value={2}>any2</option>
															<option value={3}>any3</option>
														</select>
													</div>
												</div>
												<div className="form-group">
													<label className="control-label label-font">Configurations changed:</label>
													<div className="select-form configurations-changed-select">
														<select className="form-control select-style" onChange={(e) => this.setState({configurations_changed: e.target.value})}>
															<option value={0} selected disabled>Select</option>
															<option value={'before'}>Before</option>
															<option value={'afetr'}>After</option>
														</select>
													</div>
												</div>
												{/*<div className="form-group">
													<label className="control-label label-font">Error:</label>
													<div className="select-form error-select">
														<select className="form-control select-style" onChange={(e) => this.setState({error: e.target.value})}>
															<option value={0} selected disabled>Select</option>
															<option value={'reported'}>Reported</option>
															<option value={'not-reported'}>Not Reported</option>
														</select>
													</div>
												</div>*/}
												<div className="btn-group submit-reset">
													<button className="btn btn-margin border-radius" onClick={() => this.resetFilter()}>Reset</button>
													<button className="btn btn-primary border-radius" onClick={() => this.applyFilter()}>Apply</button>
												</div>
											</fieldset>
										</div>;
									}
								})()}
							</div>
							<div className="app-filter">
								<select className="form-control app-select">
									<option value={0} disabled selected>Select Application</option>
									<option value={1}>Aurassure</option>
									<option value={2}>Pollution Monitoring</option>
									<option value={3}>Energy Monitoring</option>
								</select>
							</div>
							<div className="area-filter">
								<select className="form-control area-select">
									<option value={0} disabled selected>Select Area</option>
									<option value={1}>Odisha</option>
									<option value={2}>Raipur</option>
									<option value={3}>Siliguri</option>
								</select>
							</div>
							<div className="more-filter">
								<div className="form-control more-input" onClick={(e) => this.setState({open_filter: true})} data-tip={'Click to filter'}>More</div>
								<span className="filter-svg" onClick={(e) => this.setState({open_filter: true})}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24"><path fill={(this.state.open_filter || this.state.filter_state ? '#2673B5' : '#666')} d="M10 18h4v-2h-4v2zM3 6v2h18V6H3zm3 7h12v-2H6v2z"/></svg>
								</span>
							</div>
							<div className="pin-it">
								<div className="btn btn-block pin-btn">Pin-it</div>
								<span className="pin-icon">
								{(() => {
									if (this.state.pin_it) {
										return <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.011 53.011">
											<path d="M52.963 21.297a1 1 0 0 0-.609-.727 23.316 23.316 0 0 0-8.673-1.653c-4.681 0-8.293 1.338-9.688 1.942L19.114 8.2C19.634 3.632 17.17.508 17.06.372A1.006 1.006 0 0 0 16.335 0a1.036 1.036 0 0 0-.761.292L.32 15.546a1 1 0 0 0 .085 1.491c2.181 1.73 4.843 2.094 6.691 2.094.412 0 .764-.019 1.033-.04l12.722 14.954c-.868 2.23-3.52 10.27-.307 18.337a.997.997 0 0 0 .929.63 1 1 0 0 0 .707-.293l14.57-14.57 13.57 13.57a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414l-13.57-13.57 14.527-14.528c.238-.238.34-.58.272-.91z"/>
										</svg>;
									} else {
										return <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 53.011 53.011">
											<path d="M52.963 21.297a1 1 0 0 0-.609-.727c-8.574-3.416-16.173-.665-18.361.288L19.114 8.2C19.634 3.632 17.17.508 17.06.372A1.006 1.006 0 0 0 16.335 0a1.036 1.036 0 0 0-.761.292L.32 15.546a1 1 0 0 0 .085 1.491c2.775 2.202 6.349 2.167 7.726 2.055l12.721 14.953c-.868 2.23-3.52 10.27-.307 18.337a.997.997 0 0 0 .929.63 1 1 0 0 0 .707-.293l14.57-14.57 13.57 13.57a.997.997 0 0 0 1.414 0 .999.999 0 0 0 0-1.414l-13.57-13.57 14.527-14.528c.237-.238.339-.58.271-.91zM35.314 36.755L21.89 50.18c-2.437-8.005.993-15.827 1.03-15.909.158-.353.1-.765-.15-1.059L9.31 17.39a1.004 1.004 0 0 0-.918-.339c-.036.006-3.173.472-5.794-.955l13.5-13.5c.604 1.156 1.391 3.26.964 5.848a.997.997 0 0 0 .338.924l15.785 13.43a1.01 1.01 0 0 0 1.105.128c.077-.04 7.378-3.695 15.869-1.017L35.314 36.755z"/>
										</svg>;
									}
								})()}
								</span>
							</div>
						</div>
						<div className={'map-container' + (this.state.panel_open ? ' map-width' : '')}>
							<div className="map-height" id="mapView"></div>
						</div>
						{(() => {
							if (this.state.panel_open) {
								return <div className="right-panel">
									<Scrollbars autoHide>
										<div className="panel-view">
											<div className="panel-header">Device Details</div>
											<div className="panel-body">
												<div className="device-detail">QR Code: </div>
												<div className="device-detail">Application: </div>
												<div className="device-detail">Client: </div>
												<div className="device-detail">Locations/stations: </div>
												<div className="device-detail">Firmware Version: </div>
												<div className="device-detail">Last changed time: </div>
												<div className="device-detail">Health status: </div>
												<div className="device-detail">Device deploy time: </div>
												<div className="device-detail">Last configure time: </div>
												<div className="device-detail">Last configure by : </div>
												<div className="device-detail">Life Cycle: </div>
											</div>
										</div>
									</Scrollbars>
								</div>;
							}
						})()}
					</div>
				</Scrollbars>
			</div>
		);
	}
}

Dashboard.contextTypes = {
	router: React.PropTypes.object
};