import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import NavLink from './NavLink';



/**
 * This is the main class of Dashboard page.
 */
export default class Debug extends React.Component {
	/**
	 * This is the Constructor for Dashboard Page use to set the default task while page load.
	 * @param  {Object} props This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);
		
		this.state = {
			
		};

	}
	
	componentDidMount() {
		document.title = 'Debug -  DATOMS® Pollution Monitoring';

		window.addEventListener('resize', () => {
			this.setState({ view_mode: (window.innerWidth >= 1024) ? 'desktop' : 'mobile' });
		});
	}

	/**
     * This Renders Entire dashboard with navigation bar.
     * @return {object}   Returns value in Object format.
     */
	render() {
		return <div className="full-page-container">
			<NavLink activeLink={'debug'} />
			<h1 className="text-center"> Debug Page </h1>
		</div>;
	}
}

Debug.contextTypes = {
	router: React.PropTypes.object
};