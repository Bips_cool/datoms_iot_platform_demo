import React from 'react';
import ReactDOM from 'react-dom';
import { Link } from 'react-router-dom';
import { Scrollbars } from 'react-custom-scrollbars';
import moment from 'moment-timezone';
/** 
 * NavLink class
 */
export default class NavLink extends React.Component {
	constructor() {
		super();
		this.state = {
			menu_opened: false,
			view_mobile_nav: false,
			notification_popup: false,
			modules_popup_opened: false,
			notifications: null,
			notifications_count: null,
			data_loading: 1,
			view_mode: (window.innerWidth >= 1024) ? 'desktop' : 'mobile'
		};
		// this.getNotification();
		// this.getNotificationCount();
		// this.count_notification_interval = null;
		// this.getReadNotification();
	}

	toggleMenu() {
		this.setState({
			menu_opened: !this.state.menu_opened
		});
		// console.log('toggling user menu');
	}

	toggleNotificationMenu() {
		this.setState({
			notification_popup: !this.state.notification_popup
		}, () => this.getNotification());
		// console.log('toggling user menu');
	}

	toggleModule() {
		this.setState({
			modules_popup_opened: !this.state.modules_popup_opened
		});
		// console.log('toggling modules menu');
	}

	closeDropdowns(event) {
		const modulesDropdownArea = ReactDOM.findDOMNode(this.refs.modulesDropdownArea);
		if (modulesDropdownArea && !modulesDropdownArea.contains(event.target)) {
			// console.log('closing modules menu');
			this.setState({
				modules_popup_opened: false
			});
		}
		const userDropdownArea = ReactDOM.findDOMNode(this.refs.userDropdownArea);
		if (userDropdownArea && !userDropdownArea.contains(event.target)) {
			// console.log('closing user menu');
			this.setState({
				menu_opened: false
			});
		}

		/*const notificationdropdownarea = ReactDOM.findDOMNode(this.refs.notificationdropdownarea);
		if (notificationdropdownarea && !notificationdropdownarea.contains(event.target)) {
			// console.log('closing user menu');
			this.setState({
				notification_popup: false
			});
		}*/
	}

	componentDidMount() {
		window.react_container.addEventListener('click', (event) => this.closeDropdowns(event));

		window.addEventListener('resize', () => {
			this.setState({view_mode: (window.innerWidth >= 1024) ? 'desktop' : 'mobile'});
		});

		// this.count_notification_interval = setInterval(() => this.getNotificationCount(), 60000);
	}

	componentWillUnmount() {
		window.react_container.removeEventListener('click', (event) => this.closeDropdowns(event));

		/*if(this.count_notification_interval) {
			clearInterval(this.count_notification_interval);
			this.count_notification_interval = null;
		}*/
	}

	render() {
		let user_name = document.getElementById('user_name') ? document.getElementById('user_name').value : 'Username';
		let client_name = document.getElementById('client_name') ? document.getElementById('client_name').value : 'Phoenix Robotix Pvt. Ltd.';
		let client_slug = document.getElementById('client_slug') ? document.getElementById('client_slug').value : 'phoenix-robotix';
		let navlink_base_path = 'https://datoms.phoenixrobotix.com/enterprise/'+ client_slug +'/pollution-monitoring/vendor-portal';
		// let client_id = document.getElementById('client_id') ? document.getElementById('client_id').value : '310';
		// let user_is_admin = (document.getElementById('user_is_admin') && document.getElementById('user_is_admin').value == 1) ? true : false;
		let current_module = 'Pollution Monitoring';

		return <div>
			<nav className="navbar navbar-inverse">
				<div className="container-fluid">
					<div className="navbar-header">
						<Link to={'/'}>
							<div className="modules-dropdown" id="module_navigation">
								<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 107 18.2" height="30"><path fill="#ff8500" d="M15.3 14.2C15 13.1 14.5 12.2 13.8 11.4 13.1 10.6 12.2 10 11.1 9.6 10.1 9.2 8.8 9 7.2 9 6.9 9 6.5 9 6.1 9 5.6 9 5.2 9 4.7 9 4.2 9 3.8 9 3.3 9.1 2.9 9.1 2.5 9.2 2.1 9.2v4.6H5.5V12.1C5.7 12.1 6 12 6.3 12c.4 0 .7 0 1.1 0 .9 0 1.7.2 2.3.5.6.3 1.1.8 1.4 1.3.4.5.6 1.2.7 1.8.1.7.2 1.4.2 2 0 .8-.1 1.5-.2 2.3-.1.7-.4 1.4-.8 1.9-.4.6-.9 1-1.5 1.4-.6.3-1.4.5-2.4.5-.1 0-.2 0-.4 0-.2 0-.3 0-.5 0-.2 0-.4 0-.5 0-.2 0-.3 0-.3 0V22H2.2v4.6c.2 0 .5.1 1 .1.4 0 .9 0 1.4.1.5 0 .9 0 1.4.1.4 0 .8 0 1.1 0 1.7 0 3.1-.3 4.2-.8 1.1-.5 2-1.2 2.7-2 .7-.8 1.2-1.8 1.5-2.9.3-1.1.4-2.2.4-3.4-.1-1.3-.3-2.5-.6-3.6zM26.6 9.1h-2.5l-6.2 17.5h3.4l1.3-3.8h5.3l1.4 3.8h3.6zm-3.1 10.9l1.3-3.8.5-2.8h.1l.5 2.7 1.3 3.9zM33.1 9.2v3h5.1v14.3h3.4V12.3h5.1v-3H33.1zM63 14.2C62.7 13.1 62.2 12.1 61.6 11.4 61 10.6 60.2 10 59.3 9.6 58.4 9.2 57.3 9 56.1 9c-2.4 0-4.2.8-5.5 2.3-1.3 1.6-1.9 3.8-1.9 6.7 0 1.4.1 2.6.4 3.7.3 1.1.7 2.1 1.4 2.8.6.8 1.4 1.4 2.3 1.8.9.4 2 .6 3.3.6 2.3 0 4.2-.8 5.4-2.3 1.3-1.6 1.9-3.8 1.9-6.7 0-1.3-.1-2.6-.4-3.7zm-4.1 8.3c-.7.9-1.6 1.4-2.8 1.4-.7 0-1.2-.1-1.7-.4-.5-.3-.9-.7-1.2-1.2-.3-.5-.5-1.2-.7-1.9-.1-.7-.2-1.5-.2-2.4 0-2.1.3-3.6 1-4.5.7-1 1.6-1.4 2.8-1.4.7 0 1.3.2 1.8.5.5.3.9.7 1.2 1.3.3.5.5 1.2.7 1.9.1.7.2 1.5.2 2.4-.1 1.8-.5 3.3-1.1 4.3zM81.5 9.2l-4.4 7.9-.7 2h-.1l-.8-2-4.6-7.9H67.8V26.5H71v-9l-.5-3.4h.1l1.1 2.8 3.9 6.5h1.1l3.7-6.5 1.1-2.8h.1l-.4 3.4v9h3.4V9.2zM99.7 19.2C99.3 18.6 98.8 18.1 98.2 17.6 97.6 17.2 97 16.8 96.3 16.5 95.6 16.2 95 15.9 94.4 15.6 93.8 15.3 93.3 15 92.9 14.7 92.5 14.4 92.3 14 92.3 13.5c0-.5.2-.9.6-1.3.4-.3 1-.5 1.8-.5.8 0 1.6.1 2.4.3.7.2 1.3.4 1.6.6L99.8 9.8C99.2 9.5 98.5 9.2 97.6 9 96.7 8.8 95.8 8.7 94.8 8.7c-.9 0-1.7.1-2.4.3-.7.2-1.3.5-1.8.9-.5.4-.9.9-1.2 1.5-.3.9-.4 1.6-.4 2.4 0 1 .2 1.8.6 2.4.4.6.9 1.2 1.5 1.6.6.4 1.2.8 1.9 1.1.7.3 1.3.6 1.9.8.6.3 1.1.6 1.5.9.4.3.6.8.6 1.3 0 .7-.3 1.2-.8 1.5-.5.3-1.3.5-2.3.5-.4 0-.8 0-1.3-.1-.4-.1-.8-.1-1.2-.2-.4-.1-.7-.2-1-.3-.3-.1-.5-.2-.7-.3l-1.1 2.9c.4.2 1.1.5 2.1.7.9.2 2 .3 3.3.3.9 0 1.8-.1 2.6-.3.8-.2 1.5-.5 2-1 .6-.4 1-1 1.3-1.7.3-.7.5-1.5.5-2.4-.1-.9-.3-1.6-.7-2.3z" transform="translate(-2.1 -8.7)"/><path fill="#ff8500" d="M2.2 17.4H5.6V18.4H2.2zM2.2 19.7H5.6V20.7H2.2zM2.2 15.2H5.6V16.2H2.2z" transform="translate(-2.1 -8.7)"/><g transform="translate(-2.1 -8.7)"><circle cx="105.3" cy="12.8" r="3.3" fill="none" stroke="#ff8500"/><path fill="none" d="M103.3 11.6H108.2V14.2H103.3z"/><path fill="#ff8500" stroke="#ff8500" strokeWidth=".3" d="m 106.74381,14.486346 h -0.55736 l -1.08001,-1.283874 h -0.60507 v 1.283874 h -0.42941 v -3.229205 h 0.90435 q 0.29278,0 0.48796,0.03904 0.19519,0.03687 0.35133,0.13446 0.17567,0.110604 0.27326,0.279763 0.0998,0.166991 0.0998,0.425067 0,0.349162 -0.17566,0.585551 -0.17567,0.23422 -0.48363,0.353499 z M 105.7397,12.16583 q 0,-0.138798 -0.0499,-0.245064 -0.0477,-0.108436 -0.16049,-0.182172 -0.0932,-0.06289 -0.2212,-0.08675 -0.12796,-0.02602 -0.30145,-0.02602 h -0.50531 v 1.218813 h 0.43374 q 0.20386,0 0.35567,-0.0347 0.15181,-0.03687 0.25807,-0.13446 0.0976,-0.09109 0.14314,-0.208196 0.0477,-0.119279 0.0477,-0.30145 z" transform="scale(1.00177 .99823)"/></g></svg>
							</div>
						</Link>
						{(() => {
							if (this.state.view_mode === 'desktop') {
								return <div className="client-module">
									<div className="client-name">{client_name}</div>
									<small>{current_module}</small>
								</div>;
							}
						})()}
					</div>
					{(() => {
						if (this.state.view_mode === 'desktop') {
							let link_key = 1,
								navigation_links = [];
							navigation_links.push(
								<Link to={'/'} className={'link dashboard' + ((this.props.activeLink === '') ? ' active' : '')} key={link_key++}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 329 329" height="20" width="20">
										<path fill="#fff" d="M329 240V21.5H0V240h139.3v31.2h-50L75 307.4h179l-14.5-36.2h-50V240zM29.5 52.3h269.7V209H29.6V52.4z"/>
										<rect className="fill" fill="none" width="249.5" height="135.2" x="40.4" y="62.7"/>
									</svg>
									<label>Dashboard</label>
								</Link>
							);
							navigation_links.push(
								<Link to={'/reports/archive-report'} className={'link reports' + ((this.props.activeLink === 'reports') ? ' active' : '')} key={link_key++}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" height="20" width="20">
										<g stroke="#fff" strokeLinejoin="round">
											<path className="fill" fill="none" strokeWidth="2.9" d="M46.4 32.8l.3-18.5L34.2 2.6 3 2.4v54h29.7V32.7z" strokeLinecap="round" />
											<path className="unfill" fill="none" strokeWidth="3.5" d="M11 35.6h15zm0 11.6h15zM11 24h29zm0-11.5h20.7z" />
											<rect fill="none" width="20.6" height="20.6" x="38.1" y="38.1" strokeWidth="1.2" rx=".6" ry=".6" strokeLinecap="round" />
											<path fill="#fff" strokeWidth="1.2" d="M38 38v20.7h20.7V38zm19 19H39.8V39.8h7.7v10.8L44 47l-1.4 1 5.7 5.7L54 48l-1-1-3.7 3.6V39.8H57z" strokeLinecap="round" />
										</g>
									</svg>				
									<label>Reports</label>
								</Link>
							);
							navigation_links.push(
								<a href={'/industries'} target="_blank" className={'link dashboard'} key={link_key++}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" height="20" width="20">
										<circle cx="9" cy="9" r="7.9" className="fill" fill="none" stroke="#FFF" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round"/>
										<g className="unfill" fill="#FFF">
											<rect width="2" height="6" x="8" y="8"/>
											<rect width="2" height="2" x="8" y="4"/>
										</g>
									</svg>
									<label>Industries</label>
								</a>
							);
							navigation_links.push(
								<a href={'/device-list'} target="_blank" className={'link dashboard'} key={link_key++}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="20" width="20">
										<path fill="#fff" d="M3 2H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h2c.6 0 1-.4 1-1V3c0-.6-.4-1-1-1zm0 8H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h2c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1zm0 8H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h2c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1zM23 2H9c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h14c.6 0 1-.4 1-1V3c0-.6-.4-1-1-1zm0 8H9c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h14c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1zm0 8H9c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h14c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1z"/>
									</svg>
									<label>Devices</label>
								</a>
							);
							navigation_links.push(
								<a href={'/dmd'} target="_blank" className={'link'} key={link_key++}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 329 329" height="20" width="20">
										<path fill="#fff" d="M329 240V21.5H0V240h139.3v31.2h-50L75 307.4h179l-14.5-36.2h-50V240zM29.5 52.3h269.7V209H29.6V52.4z"/>
										<rect className="fill" fill="none" width="249.5" height="135.2" x="40.4" y="62.7"/>
									</svg>
									<label>DMD</label>
								</a>
							);
							navigation_links.push(
								<Link to={'/error-status'} className={'link status' + ((this.props.activeLink === 'status') ? ' active' : '')} key={link_key++}>
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 329 329" height="20" width="20">
										<path fill="#fff" d="M329 240V21.5H0V240h139.3v31.2h-50L75 307.4h179l-14.5-36.2h-50V240zM29.5 52.3h269.7V209H29.6V52.4z"/>
										<path className="fill" strokeWidth="38.506" fill="none" d="M41.125 63.907l.545 133.767h246.205V63.91zM102.24 182.63H55.604v-25.425h46.634zm0-39.48H55.604v-26.126h46.634zm84.61 39.48h-70.13v-25.425h70.13v25.425zm0-39.48h-70.13v-26.126h70.13v26.127zm0-39.084h-70.13v-26.02h70.13v26.02zm85.826 78.564h-71.342v-25.425h71.342zm0-39.48h-71.342v-26.126h71.342zm0-39.084h-71.342v-26.02h71.342zm-6.034 33.018h-59.496v-13.17h59.494v13.17z"/>
									</svg>
									<label>Status</label>
								</Link>
							);
									
							return <div className="navigation-links">
								{navigation_links}
							</div>;
						} else {
							return <div className={'menu-hamburger' + (this.state.view_mobile_nav ? ' active' : '')} onClick={() => this.setState({view_mobile_nav: !this.state.view_mobile_nav})}>
								<div />
								<div />
								<div />
							</div>;
						}
					})()}
					<div ref="userDropdownArea" className={'navbar-menu' + ((this.state.menu_opened) ? ' active' : '')} onClick={() => this.toggleMenu()} >
						<span className="dropdown-toggle">{user_name.charAt(0).toUpperCase()}</span>
						<div className="clearfix"/>
						<div className="dropdown-label profile-label">Me▾</div>
						<div className="dropdown-menu" onClick={(e) => {e.stopPropagation();}}>
							<div className="username">
								<div>{user_name}</div>
							</div>
							{/* <a className="account-links" href={'##PR_STRING_REPLACE_ACCOUNT_BASE_PATH##/enterprise/' + client_slug + '/users'} target="_blank">Manage Users</a> */}
							<a className="account-links" href="##PR_STRING_REPLACE_ACCOUNT_BASE_PATH##/change-password" target="_blank">Change Password</a>
							<a className="account-links" href="##PR_STRING_REPLACE_ACCOUNT_BASE_PATH##/logout">Logout</a>
						</div>
					</div>
				</div>
			</nav>
			<div className={'mobile-navigation-container' + (this.state.view_mobile_nav ? ' active' : '')}>
				<div className="mobile-navigation">
					<div className="navigation-header">
						<div className="client-name hellip">{client_name}</div>
						<div className="client-module hellip">{current_module}</div>
					</div>
					<div className="navigation-menu">
						<Scrollbars autoHide>
							<a href={navlink_base_path + '/dashboard'} target="_blank" className={'menu-item'}>
								<div className="menu-icon">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 329 329" height="22" width="22">
										<path fill="#666" d="M329 240V21.5H0V240h139.3v31.2h-50L75 307.4h179l-14.5-36.2h-50V240zM29.5 52.3h269.7V209H29.6V52.4z"/>
										<rect className="fill" fill="none" width="249.5" height="135.2" x="40.4" y="62.7"/>
									</svg>
								</div>
								<div className="menu-label hellip">Dashboard</div>
							</a>
							<Link to={'/reports/archive-report'} className={'menu-item' + ((this.props.activeLink === 'reports') ? ' active' : '')}>
								<div className="menu-icon">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" height="22" width="22">
										<g stroke="#666" strokeLinejoin="round">
											<path className="fill" fill="none" strokeWidth="2.9" d="M46.4 32.8l.3-18.5L34.2 2.6 3 2.4v54h29.7V32.7z" strokeLinecap="round" />
											<path className="unfill" fill="none" strokeWidth="3.5" d="M11 35.6h15zm0 11.6h15zM11 24h29zm0-11.5h20.7z" />
											<rect fill="none" width="20.6" height="20.6" x="38.1" y="38.1" strokeWidth="1.2" rx=".6" ry=".6" strokeLinecap="round" />
											<path fill="#666" strokeWidth="1.2" d="M38 38v20.7h20.7V38zm19 19H39.8V39.8h7.7v10.8L44 47l-1.4 1 5.7 5.7L54 48l-1-1-3.7 3.6V39.8H57z" strokeLinecap="round" />
										</g>
									</svg>
								</div>
								<div className="menu-label hellip">Reports</div>
							</Link>
							<a href={navlink_base_path + '/industries'} target="_blank" className={'menu-item'}>
								<div className="menu-icon">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 18 18" height="22" width="22">
										<circle cx="9" cy="9" r="7.9" className="fill" fill="none" stroke="#666" strokeWidth="2.2" strokeLinecap="round" strokeLinejoin="round"/>
										<g className="unfill" fill="#666">
											<rect width="2" height="6" x="8" y="8"/>
											<rect width="2" height="2" x="8" y="4"/>
										</g>
									</svg>
								</div>
								<div className="menu-label hellip">Industries</div>
							</a>
							<a href={navlink_base_path + '/device-list'} target="_blank" className={'menu-item'}>
								<div className="menu-icon">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" height="22" width="22">
										<path fill="#666" d="M3 2H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h2c.6 0 1-.4 1-1V3c0-.6-.4-1-1-1zm0 8H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h2c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1zm0 8H1c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h2c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1zM23 2H9c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h14c.6 0 1-.4 1-1V3c0-.6-.4-1-1-1zm0 8H9c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h14c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1zm0 8H9c-.6 0-1 .4-1 1v2c0 .6.4 1 1 1h14c.6 0 1-.4 1-1v-2c0-.6-.4-1-1-1z"/>
									</svg>
								</div>
								<div className="menu-label hellip">Devices</div>
							</a>
							<a href={navlink_base_path + '/dmd'} target="_blank" className={'menu-item'}>
								<div className="menu-icon">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 329 329" height="22" width="22">
										<path fill="#666" d="M329 240V21.5H0V240h139.3v31.2h-50L75 307.4h179l-14.5-36.2h-50V240zM29.5 52.3h269.7V209H29.6V52.4z"/>
										<rect className="fill" fill="none" width="249.5" height="135.2" x="40.4" y="62.7"/>
									</svg>
								</div>
								<div className="menu-label hellip">DMD</div>
							</a>
							<Link to={'/error-status'} className={'menu-item' + ((this.props.activeLink === 'status') ? ' active' : '')}>
								<div className="menu-icon">
									<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 329 329" height="22" width="22">
										<path fill="#666" d="M329 240V21.5H0V240h139.3v31.2h-50L75 307.4h179l-14.5-36.2h-50V240zM29.5 52.3h269.7V209H29.6V52.4z"/>
										<path className="fill" strokeWidth="38.506" fill="none" d="M41.125 63.907l.545 133.767h246.205V63.91zM102.24 182.63H55.604v-25.425h46.634zm0-39.48H55.604v-26.126h46.634zm84.61 39.48h-70.13v-25.425h70.13v25.425zm0-39.48h-70.13v-26.126h70.13v26.127zm0-39.084h-70.13v-26.02h70.13v26.02zm85.826 78.564h-71.342v-25.425h71.342zm0-39.48h-71.342v-26.126h71.342zm0-39.084h-71.342v-26.02h71.342zm-6.034 33.018h-59.496v-13.17h59.494v13.17z"/>
									</svg>
								</div>
								<div className="menu-label hellip">Status</div>
							</Link>
						</Scrollbars>
					</div>
				</div>
			</div>
			<div className="clearfix"></div>
			<div className="alert alert-success" id="popup_alert">
				<span className="close" onClick={() => hidePopup()}>&times;</span>
				<div id="popup_alert_msg"></div>
			</div>
		</div>;
	}
}