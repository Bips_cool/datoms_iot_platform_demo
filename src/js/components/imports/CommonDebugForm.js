import React from 'react';
import moment from 'moment-timezone';
import DateTime from 'react-datetime';

var FromDateTime = React.createClass({
	displayName: 'FromDateTime',
	getInitialState: function() {
		return {fromDate: moment().subtract(1, 'd').add(1, 'm')};
	},
	handleChange: function(date) {
		this.setState({fromDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="from_date_time" dateFormat="DD-MM-YYYY" id="fromTime" timeFormat="HH:mm" defaultValue={that.state.fromDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

var UptoDateTime = React.createClass({
	displayName: 'UptoDateTime',
	getInitialState: function() {
		return {uptoDate: moment()};
	},
	handleChange: function(date) {
		this.setState({uptoDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="upto_date_time" dateFormat="DD-MM-YYYY" id="upToTime" timeFormat="HH:mm" defaultValue={that.state.uptoDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

export default class CommonDebugForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			station_id: props.station_id
		};
		console.log('Station ID:',this.state.station_id);
	}

	shouldComponentUpdate(nextProps) {
		console.log(nextProps, this.props);
		if ((nextProps.station_id != this.props.station_id) || (this.props.time_interval && ((nextProps.time_interval[0] != this.props.time_interval[0]) || (nextProps.time_interval[1] != this.props.time_interval[1])))) {
			this.setState({
				station_id: nextProps.station_id
			});
		}
		return true;
	}

	setTimeInterval(index, time) {
		let time_interval = this.state.time_interval;
		time_interval[index] = time;
		this.setState({time_interval: time_interval});
		console.log('time', this.state.time_interval);
	}

	validateData() {
		if ((parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')) < parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))) && this.state.station_id) {
			console.log('Station ID', this.state.station_id);
			console.log('Time Interval', [parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')), parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))]);
			let from_date_time = parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')),
				upto_date_time = parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'));
			if ((upto_date_time - from_date_time) <= 86400) {
				return {
					station_id: this.state.station_id,
					time_interval: [from_date_time, upto_date_time]
				};
			} else {
				that.setState({
					status_msg: 'Please select time duration for maximum 24 hours!',
					data_loading: false
				});
			}
		} else {
			let err_msg = 'Invalid Input!';
			if (!parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))) {
				err_msg = 'From Date should not be empty!';
			} else if (!parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))) {
				err_msg = 'Upto Date should not be empty!';
			} else if (parseInt(moment(document.querySelector('.from_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X')) >= parseInt(moment(document.querySelector('.upto_date_time .form-control').value,'DD-MM-YYYY HH:mm').tz('Asia/Kolkata').format('X'))) {
				err_msg = 'From Date should be less than Upto Date!';
			} else if (!this.state.stations_id) {
				err_msg = 'Please select a Station from list!';
			}
			showPopup('danger', err_msg);
			return false;
		}
	}

	exportDebugData() {
		let data_to_be_posted = this.validateData();
		console.log('data_to_be_posted', data_to_be_posted);
		if (data_to_be_posted) {
			data_to_be_posted['org_name'] = (document.getElementById('client_name')) ? document.getElementById('client_name').value : 'Phoenix Robotix Pvt. Ltd.';
			data_to_be_posted['from_time'] = data_to_be_posted['time_interval'][0];
			data_to_be_posted['upto_time'] = data_to_be_posted['time_interval'][1];
			data_to_be_posted['data_file_format'] = 1;
			delete(data_to_be_posted['time_interval']);
			// console.log('final_data_to_be_posted', data_to_be_posted);
			this.props.export_error_data(data_to_be_posted);
		}
	}

	viewDebugData() {
		this.props.get_error_log();
	}

	render() {
		return <div className="form-container">
			<div className="row debug-form">
				<div className="col-sm-12">
					<div className="col-sm-3 col-sm-offset-2 margin-top-sm">
						<FromDateTime />				
					</div>
					<div className="col-sm-3 margin-top-sm form-padding">
						<UptoDateTime />				
					</div>
					<div className="col-sm-2 margin-top-sm form-padding">
						<button type="button" className="btn btn-primary btn-crisp btn-responsive btn-margin" onClick={() => this.viewDebugData()}>View</button>
						<button type="button" className="btn btn-crisp btn-responsive btn-margin btn-success" onClick={() => this.exportDebugData()}>Export</button>								
					</div>					
				</div>
			</div>
		</div>;
	}
}

CommonDebugForm.contextTypes = {
	router: React.PropTypes.object
};