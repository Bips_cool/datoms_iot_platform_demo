import React from 'react';
import moment from 'moment-timezone';
import { Scrollbars } from 'react-custom-scrollbars';
import _ from 'lodash';

export default class ErrorLog extends React.Component {

	constructor(props){
		super(props);
		this.state = {
			error_log:[],
			sorted_error_log:null		
		};
	}

	componentDidMount() {
		this.initialize_logs();
	}


	initialize_logs(){
		let error_log = this.props.error_data,serialize_error_log=[];

		Object.keys(error_log).map((timestamp)=>{
			serialize_error_log.push({
				timestamp:timestamp,
				error:error_log[timestamp].error,
				actions:error_log[timestamp].actions
			});
		});

		// console.log('the detected error',serialize_detected_log);

		this.setState({
			error_log:serialize_error_log,
			sorted_error_log:null
		});
	}

	sort_error_log(key){

		let error_log_array= this.state.error_log ,sorted_error_array,type=1;

		if(this.state.sorted_error_log !== null && this.state.sorted_error_log[key] !== undefined){
			if(this.state.sorted_error_log[key] == 1){
				type = 0;
			}
		}

		sorted_error_array = _.sortBy(error_log_array,key);

		if(type == 0){
			sorted_error_array = sorted_error_array.reverse();
		}

		console.log('sorted array is -> ', error_log_array);

		let sorted_detected_log_log={};
		sorted_detected_log_log[key] = type;

		console.log('sorted detec ->',sorted_detected_log_log);

		this.setState({
			error_log: sorted_error_array,
			sorted_error_log: sorted_detected_log_log
		});

	}

	render(){
		return <table className="table table-bordered">
		<thead>
			<tr>
				<th className={(this.state.sorted_error_log !== null && this.state.sorted_error_log.timestamp !== undefined)?'col-sm-3 min-width-md sort-btn sort-active':'col-sm-3 min-width-md sort-btn'} onClick={()=>{this.sort_error_log('timestamp');}}>
				{/*<span className="pull-right">
					<button disabled={(this.state.sorted_error_log !== null && this.state.sorted_error_log.timestamp && this.state.sorted_error_log.timestamp == 1)? true:false} onClick={()=>{this.sort_error_log('timestamp',1)}}  className="btn-icon">
						<svg enableBackground="new 0 0 32 32" height="15px" id="Layer_1" version="1.1" viewBox="0 0 32 32" width="15px" xmlSpace="preserve" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"><path d="M18.221,7.206l9.585,9.585c0.879,0.879,0.879,2.317,0,3.195l-0.8,0.801c-0.877,0.878-2.316,0.878-3.194,0  l-7.315-7.315l-7.315,7.315c-0.878,0.878-2.317,0.878-3.194,0l-0.8-0.801c-0.879-0.878-0.879-2.316,0-3.195l9.587-9.585  c0.471-0.472,1.103-0.682,1.723-0.647C17.115,6.524,17.748,6.734,18.221,7.206z" fill="#515151" /></svg>
					</button>
					<button disabled={(this.state.sorted_error_log !== null && this.state.sorted_error_log.timestamp !== undefined && this.state.sorted_error_log.timestamp == 0)? true:false} onClick={()=>{this.sort_error_log('timestamp',0)}} className="btn-icon">
						<svg enableBackground="new 0 0 32 32" height="15px" id="Layer_1" version="1.1" viewBox="0 0 32 32" width="15px" xmlSpace="preserve" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink"><path d="M14.77,23.795L5.185,14.21c-0.879-0.879-0.879-2.317,0-3.195l0.8-0.801c0.877-0.878,2.316-0.878,3.194,0  l7.315,7.315l7.316-7.315c0.878-0.878,2.317-0.878,3.194,0l0.8,0.801c0.879,0.878,0.879,2.316,0,3.195l-9.587,9.585  c-0.471,0.472-1.104,0.682-1.723,0.647C15.875,24.477,15.243,24.267,14.77,23.795z" fill="#515151" /></svg>
					</button>																			
				</span>*/}
				Time
				{(()=>{
					if(this.state.sorted_error_log !== null && this.state.sorted_error_log.timestamp && this.state.sorted_error_log.timestamp == 1){
						return<span>
							<svg className="sort-btn-aesc" fill="#333" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
								<path d="M8 5v14l11-7z"/>
								<path d="M0 0h24v24H0z" fill="none"/>
							</svg>
						</span>;
					}else if((this.state.sorted_error_log !== null && this.state.sorted_error_log.timestamp !== undefined && this.state.sorted_error_log.timestamp == 0)){
						return <span>
							<svg className="sort-btn-desc" fill="#333" height="15" viewBox="-2 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
								<path d="M8 5v14l11-7z"/>
								<path d="M0 0h24v24H0z" fill="none"/>
							</svg>
						</span>;
					}else{
						return <span>
							<svg className="sort-btn-aesc" fill="#ffffff" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
								<path d="M8 5v14l11-7z"/>
								<path d="M0 0h24v24H0z" fill="none"/>
							</svg>
						</span>;
					}
				})()}
				</th>
				<th className={(this.state.sorted_error_log !== null && this.state.sorted_error_log.error !== undefined)?'col-sm-8 sort-btn sort-active':'col-sm-8 sort-btn'} onClick={()=>{this.sort_error_log('error');}}>
					Log
					{(()=>{
						if(this.state.sorted_error_log !== null && this.state.sorted_error_log.error && this.state.sorted_error_log.error == 1){
							return<span>
								<svg className="sort-btn-aesc" fill="#333" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						}else if((this.state.sorted_error_log !== null && this.state.sorted_error_log.error !== undefined && this.state.sorted_error_log.error == 0)){
							return <span>
								<svg className="sort-btn-desc" fill="#333" height="15" viewBox="-2 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						}else{
							return <span>
								<svg className="sort-btn-aesc" fill="#ffffff" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						}
					})()}
				</th>
			</tr>
		</thead>
		<tbody>
			{(()=>{
				let row = this.state.error_log.map((error_log)=>{
					if(this.props.error_filter.indexOf(error_log.error) === -1){
						return <tr>
							<td>{moment.unix(error_log.timestamp).tz('Asia/Kolkata').format('DD MMM YYYY - HH:mm:ss')}</td>
							<td>
								{error_log.error}
								<br/>
								{(error_log.actions && error_log.actions.length > 0)? 'Actions: '+ error_log.actions.join(', ') : ''}
							</td>																
						</tr>;
					}
				}).filter(Boolean);
				return row;
			})()}
		</tbody>
		
	</table>;
	}

}
