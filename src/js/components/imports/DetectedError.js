import React from 'react';
import moment from 'moment-timezone';
import { Scrollbars } from 'react-custom-scrollbars';
import _ from 'lodash';

export default class DetectedError extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			detected_errors:[],
			error_log:[],
			sorted_detected_log:null,
			sorted_error_log:null,
			error_data:null			
		};
	}

	componentDidMount() {
		this.initialize_logs();
	}

	initialize_logs(){
		let detected_log = this.props.error_count_percent,serialize_detected_log=[];

		console.log('log ->>',detected_log);

		Object.keys(detected_log).map((name)=>{
			serialize_detected_log.push({
				name:name,
				count:detected_log[name].count,
				percent:detected_log[name].percent
			});
			// console.log('the detected error',serialize_detected_log);
		});

		console.log('the detected error',serialize_detected_log);
		this.setState({
			detected_errors:serialize_detected_log,
			sorted_detected_log:null
		});
	}

	sort_detected_log(key){
		let detected_log_array= this.state.detected_errors ,sorted_detected_array=[],type = 1;
		
		if(this.state.sorted_detected_log !== null && this.state.sorted_detected_log[key] !== undefined){
			if(this.state.sorted_detected_log[key] == 1){
				type = 0;
			}
		}

		sorted_detected_array = _.sortBy(detected_log_array,key);

		if(type == 0){
			sorted_detected_array = sorted_detected_array.reverse();
		}

		console.log('sorted array is -> ', sorted_detected_array);

		let sorted_detected_log_log={};
		sorted_detected_log_log[key] = type;

		console.log('sorted detec ->',sorted_detected_log_log);

		this.setState({
			detected_errors: sorted_detected_array,
			sorted_detected_log: sorted_detected_log_log
		});		
	}

	render(){
		return <table className="table table-bordered">
		<thead>
			<tr>
				<th className={'col-sm-1 sort-btn'}>
				<input type="checkbox" onClick={()=>{this.props.setErrorFilter('all');}} checked={(this.props.error_filter.length === 0)? true:false}/>
				</th>
				<th className={(this.state.sorted_detected_log !== null && this.state.sorted_detected_log.name !== undefined)?'col-sm-6 sort-btn sort-active':'col-sm-6 sort-btn'} onClick={()=>{this.sort_detected_log('name');}}>
				Name
					{(()=>{
						if (this.state.sorted_detected_log !== null && this.state.sorted_detected_log.name && this.state.sorted_detected_log.name == 1){
							return<span>
								<svg className="sort-btn-aesc" fill="#333" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
							
						} else if ((this.state.sorted_detected_log !== null && this.state.sorted_detected_log.name !== undefined && this.state.sorted_detected_log.name == 0)){
							return <span>
								<svg className="sort-btn-desc" fill="#333" height="15" viewBox="-2 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						} else {
							return <span>
								<svg className="sort-btn-aesc" fill="#ffffff" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						}
					})()}
				<span>
				</span>
				</th>
				<th className={(this.state.sorted_detected_log !== null && this.state.sorted_detected_log.count !== undefined)?'col-sm-2 sort-btn sort-active':'col-sm-2 sort-btn'} onClick={()=>{this.sort_detected_log('count');}}>
					Count
					{(()=>{
						if (this.state.sorted_detected_log !== null && this.state.sorted_detected_log.count && this.state.sorted_detected_log.count == 1){
							return<span>
								<svg className="sort-btn-aesc" fill="#333" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
							
						} else if ((this.state.sorted_detected_log !== null && this.state.sorted_detected_log.count !== undefined && this.state.sorted_detected_log.count == 0)){
							return <span>
								<svg className="sort-btn-desc" fill="#333" height="15" viewBox="-2 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						} else {
							return <span>
								<svg className="sort-btn-aesc" fill="#ffffff" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						}
					})()}
				</th>
				<th className={(this.state.sorted_detected_log !== null && this.state.sorted_detected_log.percent !== undefined)?'col-sm-2 sort-btn sort-active':'col-sm-2 sort-btn'} onClick={()=>{this.sort_detected_log('percent');}}>
					Percent
					{(()=>{
						if (this.state.sorted_detected_log !== null && this.state.sorted_detected_log.percent && this.state.sorted_detected_log.percent == 1){
							return<span>
								<svg className="sort-btn-aesc" fill="#333" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						} else if ((this.state.sorted_detected_log !== null && this.state.sorted_detected_log.percent !== undefined && this.state.sorted_detected_log.percent == 0)){
							return <span>
								<svg className="sort-btn-desc" fill="#333" height="15" viewBox="-2 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						} else {
							return <span>
								<svg className="sort-btn-aesc" fill="#ffffff" height="15" viewBox="5 0 24 24" width="15" xmlns="http://www.w3.org/2000/svg">
									<path d="M8 5v14l11-7z"/>
									<path d="M0 0h24v24H0z" fill="none"/>
								</svg>
							</span>;
						}
					})()}
				</th>
			</tr>
		</thead>
		<tbody>
			{(()=>{
				let rows = this.state.detected_errors.map((log)=>{
					return <tr>
						<td><input type="checkbox" onClick={()=>{this.props.setErrorFilter(log.name)}} checked={(this.props.error_filter.indexOf(log.name) === -1)? true:false}/></td>
						<td>{log.name}</td>
						<td>{log.count}</td>
						<td>{log.percent}</td>
					</tr>;
				});
				return rows;
			})()}
		</tbody>												
	</table>;
		
	}
}

    