import React from 'react';
import Select from './Select';
import MultiSelect from './MultiSelect';
import moment from 'moment-timezone';
import DateTime from 'react-datetime';

var FromDateTime = React.createClass({
	displayName: 'FromDateTime',
	getInitialState: function() {
		return {fromDate: moment({hour: 0}).subtract(1, 'd')};
	},
	componentDidMount: function() {
		console.log(this.state.fromDate.unix());
		this.props.callback(moment(this.state.fromDate).unix());		
	},
	handleChange: function(date) {
		this.setState({fromDate: date});
		// console.log(moment(date).unix().tz('Asia/Kolkata'));
		this.props.callback(moment(date).unix());
	},
	render: function() {
		var that = this,
			valid = function(current) {
				return current.isBefore(moment());
			};
		return <DateTime className="from_date_time" dateFormat="DD MMM YYYY" id="fromTime" timeFormat={false} defaultValue={that.state.fromDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

var UptoDateTime = React.createClass({
	displayName: 'UptoDateTime',
	getInitialState: function() {
		return {uptoDate: moment({hour: 0}).subtract(1, 's')};
	},
	componentDidMount: function() {
		// console.log(this.state.fromDate.unix());
		this.props.callback(moment(this.state.uptoDate).unix());		
	},
	handleChange: function(date) {
		this.setState({uptoDate: date});
		this.props.callback(moment(date).unix());		
	},
	render: function() {
		var that = this,
			valid = function(current) {
				return current.isBefore(moment());
			};
		return <DateTime className="upto_date_time" dateFormat="DD MMM YYYY" id="upToTime" timeFormat={false} defaultValue={that.state.uptoDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

export default class SMSReportsForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			station_type_placeholder: 'Station Type',
			stations_placeholder: 'Stations',
			data_type: 'avg',
			selected_parameters: null,
			checked_parameters: [],
			station_types: {},
			time_interval: [],
			avg_time_interval: 86400,
			station_type_selected: [],
			stations_list: {},
			download_format_selected: 'csv',
			states_selected: [],
			industry_type_selected: [],
			selected_industry: null,
			industry_list: null,
			state_list: null,
			industry_type: null,
			select_industry_list: null
		};
	}

	componentDidMount(){
		// alert('mounted');
		this.getStationList();		
	}

	getStationType(value) {
		// console.log('values types', value);
		let that = this;
		that.setState({
			stations_list: null,
			selected_parameters:null,
			station_type_selected: value
		}, () => {
			that.setStationList();
			// that.setParamsIds();
		});
	}

	/**
	 * This function gets an array of station id those are selected from the multi select
	 * @param {Array} values 
	 */
	getStationIds(values) {
		let that = this;
		console.log('values stations', values);
		// console.log('thi is ',that);
		that.setState({
			stations_selected: values
		}, () => {
			that.setParamsIds();
		});
	}

	/**
	 * It also set all the list of stations under the selected station types
	 */
	setStationType() {
		// console.log(this.state.stations_list);
		let that = this,
			station_types = {},
			default_selected = [];
		if (that.state.all_stations_list) {
			// default_selected = Object.keys(that.state.all_stations_list)[0];
			default_selected = Object.keys(that.state.all_stations_list).find((key)=>{
				if(that.state.all_stations_list[key].length > 0)
					return key;
			});
			Object.keys(that.state.all_stations_list).map((key, index) => {
				station_types[key] = key.toUpperCase();
			});
		}
		// console.log(station_types);
		that.setState({
			station_types: station_types,
			station_type_selected: default_selected
		}, () => {
			that.setStationList();
			// that.setParamsIds();			
		});
	}

	/**
	 * This function sets the liste of station being registered under the currently selected station types
	 */
	setStationList() {
		let that = this,
			stations_list = {};
		if (that.state.all_stations_list && Object.keys(that.state.all_stations_list).length) {
			Object.keys(that.state.all_stations_list).map((key) => {
				// console.log(key);
				if (that.state.station_types && Object.keys(that.state.station_types).length && that.state.station_type_selected && that.state.station_type_selected.length && that.state.station_type_selected && that.state.station_type_selected.includes(key)) {
					that.state.all_stations_list[key].map((station) => {
						stations_list[station.id] = station.name;
					});
				}
			});
		}
		console.log('stations_list', stations_list);
		that.setState({
			stations_list: stations_list
		}, () => {
			that.resetSelectedStations();
		});
	}

	/**
	 * This function resets the selected stations and checked parameters when ever a station type is unselected
	 * It removes the selected station and checked parametrs of the previously selected selected stations
	 */
	resetSelectedStations(){
		let that = this,
			selected = [],
			checked_parameters = [];
		if (that.state.stations_selected) {
			that.state.stations_selected.map((id) => {
				Object.keys(that.state.stations_list).map((key) => {
					if(key == id && selected.indexOf(id) === -1) {
						selected.push(id);
					}
				});
			});
		}

		if (that.state.selected_parameters) {
			that.state.selected_parameters.map((id) => {
				that.state.checked_parameters.map((check_id) => {
					if(check_id == id && checked_parameters.indexOf(check_id) === -1)
						checked_parameters.push(check_id);
				});
			});
		}

		that.setState({
			stations_selected: selected,
			checked_parameters: checked_parameters
		});
	}

	/**
	 * This function sets all the parameters in the exists in the currently selected stations 
	 */
	setParamsIds() {
		let that = this,
			new_parameters = [];
		// console.log(station_ids);
		Object.keys(this.state.all_stations_list).map((key) => {
			if(that.state.station_type_selected === key){
				that.state.all_stations_list[that.state.station_type_selected].map((station)=>{
					that.state.stations_selected.map((id)=>{
						console.log(id, station);
						if(station.id == id){
							console.log('looking here' + station.name);
							station.params.map((params)=>{
								// console.log(params);
								if(new_parameters.indexOf(params) === -1){
									// console.log(params);
									new_parameters.push(params);
								}
							});
						}
					});
				});
			}
		});

		if(this.state.stations_selected.length === 0){
			new_parameters = null;
		}

		// console.log('selected new params',new_parameters);

		this.setState({
			selected_parameters: new_parameters,
			checked_parameters: new_parameters
		},() => this.resetSelectedStations());
	}

	/**
	 * This one sets and unsets the selected and unselected parameters respectively
	 * @param {Boolean} status 
	 * @param {String} id 
	 */
	addSelectedParameters(status, id) {
		let selected_parameters = this.state.checked_parameters;
		console.log(status);
		if(status && selected_parameters.indexOf(id) === -1) {
			selected_parameters.push(id);
		}else if(selected_parameters.indexOf(id) !== -1) {
			selected_parameters.splice(selected_parameters.indexOf(id),1);
		}

		this.setState({
			checked_parameters: selected_parameters
		}, () => {
			console.log('checked parameters', this.state.checked_parameters);
		});
	}

	/**
	 * It takes the time stamp of the date that user selected and seits in the thime interval
	 * @param {Number} from_time 
	 */
	setFromTime(from_time) {
		console.log('from the time', from_time);
		// console.log('From Date', document.getElementById('fromTime'));
		let time_interval = this.state.time_interval;
		time_interval[0] = from_time;
		this.setState({
			time_interval: time_interval
		});

	}

	/**
	 * It takes the time stamp of the date that user selected and sets in the thime interval
	 * @param {Number} upto_time 
	 */
	setUptoTime(upto_time) {
		let time_interval = this.state.time_interval;
		time_interval[1] = upto_time;
		this.setState({
			time_interval: time_interval
		});
	}

	/**
	 * This gets the station list and the parameters list form the server and sets them respectively
	 */
	getStationList() {
		// alert('called');
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_all_stations_and_params_for_archive_report.php', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			credentials: 'include',
			body: JSON.stringify({
				industry_id: (document.getElementById('client_id')) ? document.getElementById('client_id').value : 310
			})
		}).then((response) => {
			return response.json();
		}).then((json) => {
			console.log('Stations:', json);
			if (json.status === 'success') {
				that.sanitizeIndustrySelection(json.all_industries,json.all_parameters);				
			} else {
				showPopup('danger', json.message);
				// that.setState({loading_data: null});
			}
		}).catch((ex) => {
			console.log('parsing failed', ex);
			showPopup('danger', 'Unable to load data!');
		});
	}

	validateData() {
		if ((parseInt(this.state.time_interval[0]) < parseInt(this.state.time_interval[1])) &&
			(this.state.stations_selected && this.state.stations_selected.length > 0 && this.state.checked_parameters.length) &&
			(this.state.avg_time_interval == null || parseInt(this.state.avg_time_interval))
		) {
			console.log('Station List', this.state.stations_selected);
			console.log('Checked Parametes', this.state.checked_parameters);
			console.log('Data Type', this.state.data_type);
			console.log('Time Interval', this.state.time_interval);
			console.log('avg_time_interval', this.state.avg_time_interval);
			return {
				industry_id: this.state.selected_industry,
				station_ids: this.state.stations_selected,
				parameters: this.state.checked_parameters,
				data_type: 'raw',
				time_interval: this.state.time_interval,
				average_over_time_base: this.state.avg_time_interval
			};
		} else {
			let err_msg = 'Invalid Input!';
			if (!this.state.time_interval[0]) {
				err_msg = 'From Date should not be empty!';
			} else if (!this.state.time_interval[1]) {
				err_msg = 'Upto Date should not be empty!';
			} else if (parseInt(this.state.time_interval[0]) >= parseInt(this.state.time_interval[1])) {
				err_msg = 'From Date should be less than Upto Date!';
			} else if (!this.state.stations_selected || this.state.stations_selected.length <= 0) {
				err_msg = 'Please select a Station from list!';
			} else if (this.state.checked_parameters.length == 0) {
				err_msg = 'Please select atleast 1 Parameter!';
			} 
			// else if (
			// 	(this.state.data_type != 'raw') && 
			// 	(this.state.data_type != 'avg')
			// ) {
			// 	err_msg = 'Please select a valid Data Type!';
			// } 
			else if (this.state.data_type == 'avg' && !parseInt(this.state.avg_time_interval)) {
				err_msg = 'Please select average over time based duration!';
			}
			showPopup('danger', err_msg);
			return false;
		}
	}

	sanitizeIndustrySelection(all_industries,all_parameters){
		let state_list = [],industry_type=[],industry_list={};
		Object.keys(all_industries).map((key)=>{
			if(industry_type.indexOf(all_industries[key].category) === -1 && all_industries[key].category !== undefined && all_industries[key].category !== null && all_industries[key].category !== ''){
				industry_type.push(all_industries[key].category);
			}
			if(state_list.indexOf(all_industries[key].state) === -1 && all_industries[key].state !== undefined && all_industries[key].state !== null && all_industries[key].state !== ''){
				state_list.push(all_industries[key].state);
			}
			industry_list[all_industries[key].id] = all_industries[key];
		});

		// console.log('state list',state_list);
		// console.log('industry type',industry_type);
		// console.log('industry list',industry_list);

		let state_list_object={},industry_type_object={},select_industry_list={};

		state_list.map((state)=>{
			state_list_object[state] = state;
		});

		industry_type.map((type)=>{
			industry_type_object[type] = type;
		});

		Object.keys(industry_list).map((id)=>{
			select_industry_list[id] =industry_list[id].name;
		});

		console.log('select_industry_list',select_industry_list);

		this.setState({
			industry_list: industry_list,
			state_list: state_list_object,
			industry_type: industry_type_object,
			all_parameters: all_parameters,
			select_industry_list: select_industry_list
		});

	}

	getIndustryId(){

		let select_industry_list = JSON.parse(JSON.stringify(this.state.industry_list));
		if(this.state.states_selected !== null && this.state.states_selected.length > 0){
			Object.keys(select_industry_list).map((id)=>{
				if(select_industry_list[id] && this.state.states_selected.indexOf(select_industry_list[id].state) == -1){
					delete select_industry_list[id];
				}
			});
		}

		if(this.state.industry_type_selected !== null && this.state.industry_type_selected.length > 0){
			Object.keys(select_industry_list).map((id)=>{
				if(select_industry_list[id] && this.state.industry_type_selected.indexOf(select_industry_list[id].category) == -1){
					delete select_industry_list[id];
				}
			});
		}

		// if(Object.keys(select_industry_list).indexOf(this.state.selected_industry[0]) === -1){
		// 	select_industry_list[this.state.selected_industry[0]] = this.state.industry_list[this.state.selected_industry[0]];
		// }
		// console.log('select_industry_list',select_industry_list);
		
		let filtered_list={},that = this;
		Object.keys(select_industry_list).map((id)=>{
			if(select_industry_list[id] && select_industry_list[id].name){
				filtered_list[id]=select_industry_list[id].name;			
			}
		});

		// console.log('filtered_list',filtered_list);

		// console.log('father list',this.state.industry_list);

		that.setState({
			select_industry_list: null,
			selected_industry: null
		},()=>{
			that.resetStationList();
			that.setState({
				select_industry_list:filtered_list				
			});
		});
	}

	setIndustryType(selected){
		console.log('type',selected);
		let that = this;
		this.setState({
			industry_type_selected:selected
		},()=>{
			that.getIndustryId();
		});
	}

	setIndustryState(selected){
		console.log('states',selected);
		let that = this;
		this.setState({
			states_selected:selected
		},()=>{
			that.getIndustryId();
		});
	}

	resetStationList(){
		this.setState({
			all_stations_list: null,
			selected_parameters: null,
			checked_parameters: [],
			station_type: null,
			stations_list: null,
			stations_selected: null
		});
	}

	setIndustryId(value){
		if(this.state.all_stations_list != null){
			this.resetStationList();
		}
		let that = this,
			selected_value = value,
			all_station_list = null,
			selected_industry = (selected_value != null) ? this.state.industry_list[selected_value] : null;

		if (selected_industry != null) {
			all_station_list = {};
			all_station_list.cems = selected_industry.stations[1];
			all_station_list.aaqms = selected_industry.stations[2];
			all_station_list.eqms = selected_industry.stations[3];		
		}

		console.log('selected indutry', selected_industry);		
		console.log('selected value', selected_value);
		console.log('all_station_list m', all_station_list);

		if(all_station_list !== null){
			this.setState({
				selected_industry: selected_value,
				all_stations_list: all_station_list
			},()=>{
				that.setStationType();
			});
		}else{
			this.setState({
				selected_industry: selected_value
			});
			this.resetStationList();
		}
	}

	/**
	 * This function calls the the parent funtion once the form is submitted
	 */
	getSMSReport() {
		let data_to_be_posted = this.validateData();
		console.log('data_to_be_posted', data_to_be_posted);
		if(data_to_be_posted) {
			this.props.getReport(data_to_be_posted);
		}
	}

	/**
	 * This function downloads the report after verifying the required data.
	 */
	downloadReport() {
		let that = this,
			data_to_be_posted = that.validateData();
		console.log('data_to_be_posted', data_to_be_posted);
		if (data_to_be_posted) {
			data_to_be_posted['org_name'] = that.state.selected_industry ? that.state.industry_list[that.state.selected_industry].name : 'Phoenix Robotix Pvt. Ltd.';
			data_to_be_posted['from_time'] = data_to_be_posted['time_interval'][0];
			data_to_be_posted['upto_time'] = data_to_be_posted['time_interval'][1];
			delete(data_to_be_posted['time_interval']);
			switch(that.state.download_format_selected) {
				case 'csv':
					data_to_be_posted['data_file_format'] = 1;
					break;
				case 'html':
					data_to_be_posted['data_file_format'] = 2;
					break;
				case 'pdf':
					data_to_be_posted['data_file_format'] = 3;
					break;
				case 'xlsx':
					data_to_be_posted['data_file_format'] = 4;
					break;
				case 'docx':
					data_to_be_posted['data_file_format'] = 5;
					break;
			}
			
			window.open('##PR_STRING_REPLACE_API_BASE_PATH##download_violation_summary_report.php?d=' + JSON.stringify(data_to_be_posted), '_self');
		}
	}

	render() {
		return <div className="report-form">
			<h3 className="report-title">Select your requirements</h3>
			{(() => {
				if(this.state.station_types != null) {
					return <div>
						<div className="report-options">
							<div className="option-row" id="each_row">
								<div className="option-label">Industries </div>
								<div className="option-content">
									{(() => {
										if (this.state.state_list && this.state.state_list !== null && Object.keys(this.state.state_list).length) {
											// let options = {'1':'state 1','2':'state 2','3':'state 3','4':'state 4'};
											return <div className="industry-area">
												<MultiSelect
													placeholder={'State'}
													options={this.state.state_list}
													selected={this.state.states_selected}
													callback={(selected) => this.setIndustryState(selected)}
													item_type={'Stations'} />
											</div>;
										}
									})()}
									{(() => {
										if (this.state.industry_type && this.state.industry_type !== null && Object.keys(this.state.industry_type).length) {
											// let options = {'1':'type 1','2':'type 2','3':'type 3','4':'type 4'};
											
											return <div className="industry-area">
												<MultiSelect
													placeholder={'Industry Type'}
													options={this.state.industry_type}
													selected={this.state.industry_type_selected}
													callback={(selected) => this.setIndustryType(selected)}
													item_type={'Stations'} />
											</div>;
										}
									})()}
									{(() => {
										if(true) {
											return <div className="industry-area">
												{(() => {
													if (this.state.select_industry_list && this.state.select_industry_list != null && Object.keys(this.state.select_industry_list).length) {
														// let options = ['','Industry 1','Industry 2'/,'Industry 3'];
														// Object.keys(this.state.station_types).map((type) => {
														// 	if (Object.keys(this.state.all_stations_list[type]).length) {
														// 		options[type] = this.state.station_types[type];
														// 	}
														// });
														// console.log('all_stations_list', this.state.all_stations_list);
														// return <Select
														// 	options={options}
														// 	selected={this.state.selected_industry}
														// 	callback={(value)=>this.setIndustryId(value)}
														// 	placeholder={'Select Station Type'} />;
														// return <select className="industry-select" value={this.state.selected_industry} onChange={(e)=>{this.setIndustryId(e.target.value);}}>
														// 	<option value=''>Select Industry </option>
														// 	{(()=>{
														// 		return Object.keys(this.state.select_industry_list).map((id)=>{
														// 			return <option value={id} >{this.state.select_industry_list[id]}</option>
														// 		});
														// 	})()}
														// </select>;
														return <Select
														placeholder={'Industry Name'}
														options={this.state.select_industry_list}
														selected={this.state.selected_industry}
														callback={(selected) => this.setIndustryId(selected)}
														item_type={'Industry'} />;
													}
												})()}
											</div>; 
										}else{
											return <div className="text-muted">Select a indutries first</div>;
										}
									})()}
								</div>
							</div>
							<div className="option-row" id="each_row">
								<div className="option-label">Station</div>
								<div className="option-content">
									{(() => {
										if(this.state.station_types != null) {
											return <div className="station-area">
												{(() => {
													if (this.state.station_types && Object.keys(this.state.station_types).length &&  this.state.all_stations_list != null) {
														let options = [];
														Object.keys(this.state.station_types).map((type) => {
															if (Object.keys(this.state.all_stations_list[type]).length) {
																options[type] = this.state.station_types[type];
															}
														});
														return <Select
															options={options}
															selected={this.state.station_type_selected}
															callback={(selected) => this.getStationType(selected)}
															placeholder={'Select Station Type'} />;
													}
													else{
														return <div className="text-muted">Select an industry first</div>;														
													}
												})()}
											</div>;
										}
									})()}
									{(() => {
										if(this.state.stations_list && Object.keys(this.state.stations_list).length) {
											return <div className="station-area">
												<MultiSelect
													placeholder={this.state.stations_placeholder}
													options={this.state.stations_list}
													selected={this.state.stations_selected}
													callback={(selected) => this.getStationIds(selected)}
													item_type={'Stations'} />
											</div>;
										}
									})()}
								</div>
							</div>
							<div className="option-row" id="prameters">
								<div className="option-label">Parameters</div>
								<div className="option-content">
									{(() => {
										if(this.state.selected_parameters && this.state.selected_parameters != null && this.state.selected_parameters.length > 0) {
											let params_list = '';
											params_list = this.state.selected_parameters.map((id) => {
												return <div className="option">
													<input type="checkbox" id={'param_' + id} checked={this.state.checked_parameters.indexOf(id) >= 0} onChange={(e) => {this.addSelectedParameters(e.target.checked,id);}} />
													<label htmlFor={'param_' + id} dangerouslySetInnerHTML={{__html:this.state.all_parameters[id].prm_name}} />
												</div>;
											});
											return params_list;
										}else{
											return <div className="text-muted">Select a station first</div>;
										}
									})()}
								</div>
							</div>
							<div className="option-row" id="time_interval">
								<div className="option-label">Time Interval</div>
								<div className="option-content">
									<div className="date-time">
										<div className="rdt from_date_time" id="from_time">
											<div className="rdt from_date_time">
												<FromDateTime callback={(from_time) => {this.setFromTime(from_time);}} />
											</div>
										</div>
									</div>
									<div className="hyphen">-</div>
									<div className="date-time">
										<div className="rdt upto_date_time" id="upto_time">
											<div className="rdt upto_date_time">
												<UptoDateTime callback={(upto_time) => {this.setUptoTime(upto_time);}} />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div className="option-row" id="download_data_format">
								<div className="option-label">Download Data Format</div>
								<div className="option-content">
									<div className="option">
										<input type="radio" name="format-type" value='1' id="format_csv" checked={this.state.download_format_selected == 'csv'} onChange={() => this.setState({download_format_selected: 'csv'})} />
										<label htmlFor="format_csv">CSV</label>
									</div>
									<div className="option">
										<input type="radio" name="format-type" value='2' id="format_html" checked={this.state.download_format_selected == 'html'} onChange={() => this.setState({download_format_selected: 'html'})} />
										<label htmlFor="format_html">HTML</label>
									</div>
									<div className="option">
										<input type="radio" name="format-type" value='3' id="format_pdf" checked={this.state.download_format_selected == 'pdf'} onChange={() => this.setState({download_format_selected: 'pdf'})} />
										<label htmlFor="format_pdf">PDF</label>
									</div>
									<div className="option">
										<input type="radio" name="format-type" value='4' id="format_xlsx" checked={this.state.download_format_selected == 'xlsx'} onChange={() => this.setState({download_format_selected: 'xlsx'})}/>
										<label htmlFor="format_xlsx">Excel</label>
									</div>
									<div className="option">
										<input type="radio" name="format-type" value='5' id="format_docx" checked={this.state.download_format_selected == 'docx'} onChange={() => this.setState({download_format_selected: 'docx'})} />
										<label htmlFor="format_docx">Word</label>
									</div>
								</div>
							</div>
						</div>
						<div className="report-form-actions">
							<button type="button" className="btn btn-primary" id="view_button" onClick={() => this.getSMSReport()}>View</button>
							<button type="button" className="btn btn-success" id="download_button" onClick={() => this.downloadReport()}>Download</button>
						</div>
						<div className="err-msg" />
						<div className="disable-form hide" />
					</div>;
				}else{
					return <div className="loading">
						<svg className="loading-spinner" width="30px" height="30px" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
					</div>;
				}
			})()}
		</div>;
	}
}

SMSReportsForm.contextTypes = {
	router: React.PropTypes.object
};