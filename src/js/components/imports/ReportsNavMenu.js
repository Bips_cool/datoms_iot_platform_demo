import React from 'react';
import { Link } from 'react-router-dom';

export default class ReportsNavMenu extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};
	}

	render() {
		return <div className="submenu-nav">
			<div className="page-sub-menu">
				<div className="flex">
					<div className="submenu-width">
						<div className={'padding-with-icon' + ((this.props.activeLink === 'industry') ? ' active' : '')}>
							<svg fill={(this.props.activeLink === 'industry') ? '#B6C9FF' : '#fff'} xmlns="http://www.w3.org/2000/svg" height="13" width="13" viewBox="0 0 512 512">
								<path d="M447.933 103.629c-.034-3.076-1.224-6.09-3.485-8.352L352.683 3.511c-.004-.004-.007-.005-.011-.008C350.505 1.338 347.511 0 344.206 0H89.278C75.361 0 64.04 11.32 64.04 25.237v461.525c0 13.916 11.32 25.237 25.237 25.237h333.444c13.916 0 25.237-11.32 25.237-25.237V103.753c.002-.044-.021-.081-.025-.124zm-91.739-62.698l50.834 50.834h-49.572c-.695 0-1.262-.567-1.262-1.262V40.931zm67.789 445.832c0 .695-.566 1.261-1.261 1.261H89.278c-.695 0-1.261-.566-1.261-1.261V25.237c0-.695.566-1.261 1.261-1.261h242.94v66.527c0 13.916 11.322 25.239 25.239 25.239h66.527v371.021z"/>
								<path d="M362.088 164.014H149.912c-6.62 0-11.988 5.367-11.988 11.988 0 6.62 5.368 11.988 11.988 11.988h212.175c6.62 0 11.988-5.368 11.988-11.988.001-6.621-5.368-11.988-11.987-11.988zM362.088 236.353H149.912c-6.62 0-11.988 5.368-11.988 11.988 0 6.62 5.368 11.988 11.988 11.988h212.175c6.62 0 11.988-5.368 11.988-11.988.001-6.62-5.368-11.988-11.987-11.988zM362.088 308.691H149.912c-6.62 0-11.988 5.368-11.988 11.988 0 6.621 5.368 11.988 11.988 11.988h212.175c6.62 0 11.988-5.367 11.988-11.988.001-6.619-5.368-11.988-11.987-11.988zM256 381.031H149.912c-6.62 0-11.988 5.368-11.988 11.988 0 6.621 5.368 11.988 11.988 11.988H256c6.62 0 11.988-5.367 11.988-11.988 0-6.621-5.368-11.988-11.988-11.988z"/>
							</svg>
							<Link to={'/reports/archive-report'} className={(this.props.activeLink === 'industry') ? ' active' : ''} >Archive Data</Link>
						</div>
					</div>
					<div className="submenu-width">
						<div className={'padding-with-icon' + ((this.props.activeLink === 'sms') ? ' active' : '')}>
							<svg fill={(this.props.activeLink === 'sms') ? '#B6C9FF' : '#fff'} xmlns="http://www.w3.org/2000/svg" height="13" width="13" viewBox="0 0 490 490">
								<path d="M383.947 490L230.709 364.252H50.713c-27.924 0-50.631-23.111-50.631-51.528V51.528C.082 23.111 22.789 0 50.713 0h388.574c27.924 0 50.631 23.111 50.631 51.528v261.197c0 28.417-22.707 51.528-50.631 51.528h-55.34V490zM50.713 30.615c-11.032 0-20.016 9.388-20.016 20.913v261.197c0 11.525 8.984 20.913 20.016 20.913h190.938l111.681 91.635v-91.635h85.954c11.032 0 20.016-9.388 20.016-20.913V51.528c0-11.525-8.984-20.913-20.016-20.913H50.713z"/>
								<path d="M75.812 89.512h338.376v30.615H75.812zM75.812 164.569h277.147v30.615H75.812zM75.812 239.611h215.917v30.615H75.812z"/>
							</svg>
							<Link to={'/reports/violation-summary-report'} className={(this.props.activeLink === 'sms') ? ' active' : ''} >Violation Summary</Link>
						</div>
					</div>
				</div>
			</div>
		</div>;
	}
}

ReportsNavMenu.contextTypes = {
	router: React.PropTypes.object
};