import React from 'react';
// import ReactDOM from 'react-dom';
// import { Link } from 'react-router-dom';
// import { Scrollbars } from 'react-custom-scrollbars';
// import moment from 'moment-timezone';
/** 
 * NavLink class
 */
export default class Circle extends React.Component {
	constructor(props){
		super(props);
		this.state = {
		};
	}

	componentDidMount(){
		// console.log('all props',this.props);
		this.drawCircle();
	}


	drawCircle(){
		let canvas = document.getElementById(this.props.data_set);
		canvas.style.border = '2px solid '+this.props.border_color;
		canvas.width = canvas.height = this.props.size;
		
		if(this.props.parcent != 0){
			let ctx = canvas.getContext('2d');
	
			ctx.translate(this.props.size / 2, this.props.size / 2); // change center
	
			let radius = (this.props.size - this.props.lineWidth) / 2;
			let percent = (100-parseInt(this.props.parcent))/100;
			ctx.rotate((-1 / 2 + 0 / 180) * Math.PI); // rotate -90 deg
	
			ctx.beginPath();
			if(percent > 0){
				ctx.arc(0, 0, radius, 0, percent * 360 * 0.0174533, true);			
			}else{
				ctx.arc(0, 0, radius, 0, 2 * Math.PI, false);
			}
			ctx.strokeStyle = this.props.fill_color;
			ctx.lineCap = 'butt'; // butt, round or square
			ctx.lineWidth = this.props.lineWidth;
			ctx.stroke();
			
		}		
	}



	render(){
		return <div className="canvas-container" style={{position: 'relative'}}>
			<canvas className="canvas-circle" id={this.props.data_set}>
			</canvas>
			{(()=>{
				if(parseInt(this.props.parcent) === 100){
					return <span className="canvas-span" style={{color : this.props.border_color, left : "calc(50% - 24px)"}}>{parseInt(this.props.parcent)}% </span>;					
				}else if(parseInt(this.props.parcent) >= 10){
					return <span className="canvas-span" style={{color : this.props.border_color, left : "calc(50% - 15px)"}}>{parseInt(this.props.parcent)}% </span>;
				}else{
					return <span className="canvas-span" style={{color : this.props.border_color, left : "calc(50% - 10px)"}}>{parseInt(this.props.parcent)}% </span>;					
				}

			})()}
		</div>;
	}
}
