import React from 'react';
import ReactDOM from 'react-dom';

export default class Select extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			options: props.options ? props.options : {},
			status: props.status ? props.status : {},
			selected: props.selected ? props.selected : null,
			dismissible: props.dismissible ? props.dismissible : false,
			options_shown: props.options_shown ? props.options_shown : false,
			placeholder: props.placeholder ? props.placeholder : 'Select',
			class_name: props.className ? props.className : ''
		};
	}

	toggleSelection(e, val) {
		e.stopPropagation();
		val = isNaN(val) ? val : (val ? parseInt(val) : '');
		this.setState({
			selected: val,
			options_shown: false
		}, () => {
			// Callback Function call
			if (this.props.callback) {
				this.props.callback(val);
			}
		});
	}

	clearSelection(e) {
		e.stopPropagation();
		this.setState({selected: null});
	}

	toggleSelectBox() {
		this.setState({
			options_shown: !this.state.options_shown
		});
		// console.log('toggling custom selectbox');
	}

	closeDropdowns(event) {
		const selectDropdown = ReactDOM.findDOMNode(this.refs.selectDropdown);
		if (selectDropdown && !selectDropdown.contains(event.target)) {
			// console.log('closing custom selectbox');
			this.setState({
				options_shown: false
			});
		}
	}

	componentDidMount() {
		window.react_container.addEventListener('click', (event) => this.closeDropdowns(event));
	}

	componentWillUnmount() {
		window.react_container.removeEventListener('click', (event) => this.closeDropdowns(event));
	}

	render() {
		return <div ref="selectDropdown" className={'pr-react-select single' + (this.state.options_shown ? ' open' : '') + (this.state.class_name ? (' ' + this.state.class_name) : '')} onClick={() => this.setState({options_shown: !this.state.options_shown})}>
			{(() => {
				if (this.state.options && Object.keys(this.state.options).length && this.state.selected) {
					return <div className="pr-selected-items hellip">
						{this.state.options[this.state.selected]}
						{(() => {
							if (this.state.dismissible) {
								return <span className="pr-clear-selection" onClick={(e) => this.clearSelection(e)}/>;
							}
						})()}
					</div>;
				} else {
					return <div className="pr-placeholder">{this.state.placeholder}</div>;
				}
			})()}
			{(() => {
				let options = [];
				if (this.state.options && Object.keys(this.state.options).length) {
					Object.keys(this.state.options).map((key, index) => {
						options.push(<div className={'pr-option flex hellip' + (Object.keys(this.state.status).length ? ' status-icon ' + this.state.status[key] : '') + (this.state.selected == key ? ' checked' : '')} key={index} onClick={(e) => this.toggleSelection(e, key)} title={this.state.options[key]}>{this.state.options[key]}</div>);
					});
				} else {
					options = <div className="pr-no-option">No options available!</div>;
				}
				return <div className="pr-options">{options}</div>;
			})()}
		</div>;
	}
}
