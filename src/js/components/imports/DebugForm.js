import React from 'react';
import moment from 'moment-timezone';
import DateTime from 'react-datetime';

var FromDateTime = React.createClass({
	displayName: 'FromDateTime',
	getInitialState: function() {
		return {fromDate: moment().subtract(1, 'd').add(1, 'm')};
	},
	handleChange: function(date) {
		this.setState({fromDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="from_date_time" dateFormat="DD-MM-YYYY" id="fromTime" timeFormat="HH:mm" defaultValue={that.state.fromDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

var UptoDateTime = React.createClass({
	displayName: 'UptoDateTime',
	getInitialState: function() {
		return {uptoDate: moment()};
	},
	handleChange: function(date) {
		this.setState({uptoDate: date});
	},
	render: function() {
		var that = this,
			valid = function(current){
				return current.isBefore(moment());
			};
		return <DateTime className="upto_date_time" dateFormat="DD-MM-YYYY" id="upToTime" timeFormat="HH:mm" defaultValue={that.state.uptoDate} isValidDate={valid} onChange={that.handleChange} />;
	}
});

export default class DebugForm extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
		};
		console.log('Device ID:',this.state.device_id);
	}

	changeUrl(dev_id) {
		this.setState({device_id: dev_id});
	}

	shouldComponentUpdate(nextProps) {
		console.log(nextProps, this.props);
		if ((nextProps.device_id != this.props.device_id) || (nextProps.city_id != this.props.city_id) || (this.props.time_interval && ((nextProps.time_interval[0] != this.props.time_interval[0]) || (nextProps.time_interval[1] != this.props.time_interval[1])))) {
			this.setState({
				device_id: nextProps.device_id,
				city_id: nextProps.city_id
			});
		}
		return true;
	}

	callToParent() {
		if (this.props.active_view == 'table') {
			this.props.get_device_error_log(1);
		} else if (this.props.active_view == 'graph') {
			this.props.get_device_online_status();
		}
	}

	render() {
		/*return <div className="debug-form">
			<div className="debug-options">
				<div className="date-time-container">
					<div className="date-time-item">
						<FromDateTime />
					</div>
					<div className="date-time-item">
						<UptoDateTime />
					</div>
					<div className="action-buttons">
						<button type="button" className="btn btn-primary" onClick={() => this.callToParent()}>View</button>				
					</div>
				
				</div>
			</div>			
		</div>;*/
		return <div className="form-container">
			<div className="row debug-form">
				<div className="col-sm-12">
					<div className="col-sm-3 col-sm-offset-3 margin-top-sm">
						<FromDateTime />				
					</div>
					<div className="col-sm-3 margin-top-sm form-padding">
						<UptoDateTime />				
					</div>
					<div className="col-sm-1 margin-top-sm form-padding">
						<button type="button" className="btn btn-primary btn-crisp btn-responsive" onClick={() => this.callToParent()}>View</button>								
					</div>					
				</div>
			</div>
		</div>;
	}
}

DebugForm.contextTypes = {
	router: React.PropTypes.object
};