import React from 'react';
import ReactDOM from 'react-dom';

export default class MultiSelect extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			options: props.options ? props.options : {},
			options_shown: props.options_shown ? props.options_shown : false,
			item_type: props.item_type ? props.item_type : 'Items'
		};
	}

	toggleSelectAll(e) {
		e.stopPropagation();
		let selected = this.props.selected;
		if (Object.keys(this.state.options).length > this.props.selected.length) {
			selected = Object.keys(this.state.options).map((val) => val);
		} else {
			selected = [];
		}
		console.log('selected values', selected);
		// Callback Function call
		if (this.props.callback) {
			this.props.callback(selected);
		}
	}

	toggleSelection(e, val) {
		e.stopPropagation();
		let selected = this.props.selected;
		if (selected.indexOf(val) === -1) {
			selected.push(val);
		} else {
			selected.splice(selected.indexOf(val), 1);
		}
		// Callback Function call
		if (this.props.callback) {
			this.props.callback(selected);
		}
	}

	clearSelection(e) {
		e.stopPropagation();
		// Callback Function call
		if (this.props.callback) {
			this.props.callback([]);
		}
	}

	toggleSelectBox() {
		this.setState({
			options_shown: !this.state.options_shown
		});
		// console.log('toggling custom selectbox');
	}

	closeDropdowns(event) {
		const multiSelectDropdown = ReactDOM.findDOMNode(this.refs.multiSelectDropdown);
		if (multiSelectDropdown && !multiSelectDropdown.contains(event.target)) {
			// console.log('closing custom selectbox');
			this.setState({
				options_shown: false
			});
		}
	}

	componentDidMount() {
		window.react_container.addEventListener('click', (event) => this.closeDropdowns(event));
	}

	componentWillUnmount() {
		window.react_container.removeEventListener('click', (event) => this.closeDropdowns(event));
	}

	componentDidUpdate() {
		/*if (this.props.selected == [] && this.props.selected.length) {
			this.setState({
				selected: this.props.selected ? this.props.selected : []
			});
		}*/
	}

	render() {
		return <div ref="multiSelectDropdown" className={'pr-react-select' + (this.state.options_shown ? ' open' : '')} onClick={() => this.toggleSelectBox()}>
			{(() => {
				if (this.state.options && Object.keys(this.state.options).length && this.props.selected && this.props.selected.length) {
					let options_selected = [];
					if (this.props.selected && this.props.selected.length === 1) {
						options_selected = this.props.selected.map((key, index) => {
							return <span className="pr-selected" key={index}>{this.state.options[key]}</span>;
						});
					} else if (this.props.selected) {
						options_selected = <span className="pr-selected">{this.props.selected.length + ' ' + this.state.item_type} Selected</span>;
					}
					return <div className="pr-selected-items hellip">
						{options_selected}
						<span className="pr-clear-selection" onClick={(e) => this.clearSelection(e)}/>
					</div>;
				} else {
					return <div className="pr-placeholder">{this.props.placeholder}</div>;
				}
			})()}
			{(() => {
				let options = [];
				if (this.state.options && Object.keys(this.state.options).length) {
					let selected = this.props.selected ? this.props.selected.length : 0;
					options.push(<div className="pr-option pr-select-all" onClick={(e) => this.toggleSelectAll(e, 'all')}>{Object.keys(this.state.options).length > selected ? 'Select All' : 'Unselect All'}</div>);
					Object.keys(this.state.options).map((key, index) => {
						if (this.props.selected && this.props.selected.indexOf(key) === -1) {
							options.push(<div className="pr-option flex hellip" key={index} onClick={(e) => this.toggleSelection(e, key)} title={this.state.options[key]}>{this.state.options[key]}</div>);
						} else if (this.props.selected) {
							options.push(<div className="pr-option flex hellip checked" key={index} onClick={(e) => this.toggleSelection(e, key)} title={this.state.options[key]}>{this.state.options[key]}</div>);
						}
					});
				} else {
					options = <div className="pr-no-option">No options available!</div>;
				}
				return <div className="pr-options">{options}</div>;
			})()}
		</div>;
	}
}
