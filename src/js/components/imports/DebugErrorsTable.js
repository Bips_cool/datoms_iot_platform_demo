import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import DetectedError from './DetectedError';
import ErrorLog from './ErrorLog';

export default class DebugErrorsTable extends React.Component {
	constructor(props){
		super(props);
		this.state = {
			error_filter: []
		};
		console.log('props:', props);
	}

	setErrorFilter(error){
		// console.log(error);
		let error_filter = this.state.error_filter;		
		if(error !== 'all'){
			if(error_filter.indexOf(error) === -1){
				error_filter.push(error);
			}else{
				error_filter.splice(error_filter.indexOf(error),1);
			}			
		}else {
			if(error_filter.length === Object.keys(this.props.error_count_percent).length){
				error_filter = [];
			}else{
				error_filter = Object.keys(this.props.error_count_percent);
			}
		}
		

		this.setState({
			error_filter:error_filter
		})
	}

	render(){
		return <div className="row height-bottom-row">
			<div className="col-sm-4 full-height">
				<div className="panel panel-default height-panel">
					<div className="panel-heading table-header text-center">
						<span className="debug-table-header">Detected Errors</span>
					</div>
					<div className="panel-body fixed-height">
						<Scrollbars autoHide>
							{(()=>{
								if(this.props.error_count_percent && this.props.error_count_percent != null && Object.keys(this.props.error_count_percent).length > 0){
									return <DetectedError error_filter={this.state.error_filter} error_count_percent={this.props.error_count_percent} setErrorFilter={(error) => this.setErrorFilter(error)} />;
								}
							})()}
						</Scrollbars>
					</div>
				</div>
			</div>
			<div className="col-sm-8 full-height">
				<div className="panel panel-default height-panel">
					<div className="panel-heading table-header text-center">
						<span className="debug-table-header">Error Log</span>
					</div>
					<div className="panel-body fixed-height">
						<Scrollbars autoHide>
							{(()=>{
								if(this.props.error_data && this.props.error_data != null && Object.keys(this.props.error_data).length > 0){
									return <ErrorLog error_filter={this.state.error_filter} error_data={this.props.error_data} />;
								}
							})()}
						</Scrollbars>
					</div>
				</div>
			</div>
		</div>;
	}
}