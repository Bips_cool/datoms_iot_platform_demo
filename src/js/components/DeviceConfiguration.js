import React from 'react';
import { Scrollbars } from 'react-custom-scrollbars';
import NavLink from './NavLink';
import ReactDOM from 'react-dom';
import moment from 'moment-timezone';
import Datetime from 'react-datetime';
import Select from 'react-select';
import ReactTooltip from 'react-tooltip';
import ClipboardButton from 'react-clipboard.js';
/**
 * This is the main class of Device Configuration page.
 */
export default class DeviceConfiguration extends React.Component {
	/**
	 * This is the Constructor for Device Configuration Page use to set the default task while page load.
	 * @param  {Object} props This will import the attributes passed from its Parent Class.
	 */
	constructor(props) {
		super(props);
		this.state = {
			alert_details:{},
			daily_alert_users: [],
			pre_warning_alert_users: [],
			offline_alert_users: [],
			violation_alert_users: []
		};
		this.applicable_parameter = [];
	}

	componentDidUpdate() {
		ReactTooltip.rebuild();
	}

	componentWillMount() {
		this.setState({loc_id: this.props.match.params.station_id});
		let that = this;
		fetch('##PR_STRING_REPLACE_API_BASE_PATH##get_data_for_device_configuration.php', {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			credentials: 'include',
			body: JSON.stringify({
				loc_id: that.props.match.params.station_id
			})
		}).then(function(Response) {
			return Response.json()
		}).then(function(json) {
			console.log(json);
			if(json.status === 'success') {
				that.setState(json);
				that.setState({
					loc_spcb_id_disabled: json.dataset.loc_spcb_id ? true : false,
					org_category_disabled: parseInt(json.dataset.org_category) ? true : false,
					org_cpcb_code_disabled: json.dataset.org_cpcb_code ? true : false,
					loc_send_data_to_spcb_disabled: json.dataset.loc_send_data_to_spcb ? true : false,
					loc_spcb_data_published_disabled: json.dataset.loc_spcb_data_published ? true : false,
					loc_push_data_to_ospcb_disabled: json.dataset.loc_push_data_to_ospcb ? true : false,
					loc_cpcb_data_published_disabled: json.dataset.loc_cpcb_data_published ? true : false,
					loc_alert_send_cpcb_alert_disabled: json.dataset.loc_alert_send_cpcb_alert ? true : false,
					loc_push_data_to_cpcb_disabled: json.dataset.loc_push_data_to_cpcb ? true : false,
					loc_sim_operator: [
						{
							value: 'Airtel',
							label: 'Airtel'
						},
						{
							value: 'BSNL',
							label: 'BSNL'
						},
						{
							value: 'Custom',
							label: 'Custom'
						}
					],
					loc_analyser_status: [
						{
							value: 'U',
							label: 'Usable'
						},
						{
							value: 'M',
							label: 'Maintenance'
						},
						{
							value: 'F',
							label: 'Faulty'
						}
					],
					loc_param_channel: {
						1: '1',
						2: '2',
						3: '3',
						4: '4',
						5: '5',
						6: '6',
						7: '7',
						8: '8',
						9: '9',
						10: '10',
						11: '11',
						12: '12'
					},
					dev_protocol: 'HTTP',
					alert_details:{
						param_units: json.dataset.param_units,
						param_thresholds: json.dataset.param_thresholds,
						parameters: json.dataset.alert_parameters
					},
					users_list: json.dataset.users,
					daily_alert_users: json.dataset.daily_alert_users,
					pre_warning_alert_users: json.dataset.pre_warning_alert_users,
					offline_alert_users: json.dataset.offline_alert_users,
					violation_alert_users: json.dataset.violation_alert_users
				});
				if (that.state.alert_details.param_thresholds == null) {
					let alert_details = that.state.alert_details;
					let thresholds_limits = [];
					alert_details.parameters.map((param, id) => {
						thresholds_limits.push(null);
					});
					alert_details.param_thresholds = thresholds_limits;
					that.setState({alert_details: alert_details});
				}
			} else {
				showPopup('danger', json.message);
			}
		}).then(() => {
			that.setState({
				data_submitting: false
			});
		}).catch(function(ex) {
			console.log('parsing failed', ex);
			showPopup('danger', 'Unable to load data!');
		});
		that.onSuccess = that.onSuccess.bind(that);
		that.getText = that.getText.bind(that);
		that.alert_details = that.state.alert_details;
	}

	updateValues(key, value, index, array) {
		let dataset = this.state.dataset;
		console.log(key + '->' + index, value);
		Object.keys(this.state.dataset).map((data_key) => {
			if ((key == data_key) && (index === undefined)) {
				dataset[key] = value;
				console.log('[key]', value);
			} else if ((key == data_key) && (index !== undefined) && (array === undefined)) {
				if (dataset[key].constructor !== Array) {
					dataset[key] = [];
				}
				dataset[key][parseInt(index)] = value;
				console.log('[key][index]', value);
			} else if ((key == data_key) && (index !== undefined) && (array !== undefined)) {
				let val = null;
				if (value.constructor === Array) {
					val = value.map((v) => {
						return v.value;
					}).filter(Boolean);
				} else {
					val = value;
				}
				if (!dataset[key] || dataset[key].constructor !== Array) {
					dataset[key] = [];
				}
				if (!dataset[key][parseInt(index)] || dataset[key][parseInt(index)].constructor !== Array && dataset[key][parseInt(index)].constructor !== Object) {
					dataset[key][parseInt(index)] = {};
				}
				dataset[key][parseInt(index)][array] = val;
				console.log('[key][index][array]', val);
			}
		});
		this.setState({dataset: dataset});
		console.log('State Updated', this.state.dataset);
	}

	onSuccess(key) {
		console.info('successfully coppied');
	}

	removeTooltip(key) {
		ReactDOM.findDOMNode(this.refs[key]).setAttribute("data-tip", 'Click to copy to clipboard.');
		// ReactDOM.findDOMNode(this.refs.copy).setAttribute("data-tip-disable", true);
		ReactTooltip.rebuild();
	}

	getText(key) {
		console.log('Copy Text', this.state.dataset[key]);
		console.info('successfully coppied', this.refs[key]);
		let that = this,
			tooltip = ReactDOM.findDOMNode(that.refs[key]);
		console.info('successfully coppied', tooltip);
		tooltip.setAttribute("data-tip", 'Copied!');
		// tooltip.setAttribute("data-tip-disable", false);
		ReactTooltip.hide(tooltip);
		ReactTooltip.rebuild();
		ReactTooltip.show(tooltip);
		return this.state.dataset[key];
	}

	addParameterRow() {
		let dataset = this.state.dataset;
		Object.keys(this.state.dataset).map((data_key) => {
			if (
				(data_key == 'loc_param_list') ||
				(data_key == 'loc_param_received_unit_list') ||
				(data_key == 'loc_param_view_unit_list')
			) {
				if (!dataset[data_key]) {
					dataset[data_key] = [];
				}
				dataset[data_key].push(0);
			} else if (data_key == 'loc_param_ranges') {
				if (!dataset[data_key]) {
					dataset[data_key] = [];
				}
				dataset[data_key].push([0,0]);
			} else if (data_key == 'loc_param_thresholds') {
				if (!dataset[data_key]) {
					dataset[data_key] = [];
				}
				dataset[data_key].push([80,100]);
			} else if (data_key == 'loc_param_channel') {
				if (!dataset[data_key]) {
					dataset[data_key] = [];
				}
				dataset[data_key].push(0);
			} else if (data_key == 'loc_param_offset') {
				if (!dataset[data_key]) {
					dataset[data_key] = [];
				}
				dataset[data_key].push(0);
			} else if (data_key == 'loc_param_multiplier') {
				if (!dataset[data_key]) {
					dataset[data_key] = [];
				}
				dataset[data_key].push(0);
			}
		});
		this.setState({dataset: dataset});
		console.log('Parameter Added', this.state.dataset);
	}

	addUserRow(user_id) {
		let dataset = this.state.dataset;
		if (!dataset.loc_alert_users) {
			dataset.loc_alert_users = [];
		}
		dataset.loc_alert_users.push({
			id: parseInt(user_id),
			sms: true,
			email: true
			
		});
		this.setState({dataset: dataset});
		console.log('User Added', this.state.dataset.loc_alert_users);
	}

	updateUserRow(index, user_id, sms, email) {
		let dataset = this.state.dataset;
		dataset.loc_alert_users[index] = {
			id: parseInt(user_id),
			sms: sms,
			email: email
		};
		this.setState({dataset: dataset});
		console.log('User Updated', this.state.dataset.loc_alert_users);
	}

	removeUserRow(index) {
		let dataset = this.state.dataset;
		Object.keys(this.state.dataset).map((data_key) => {
			if (data_key == 'loc_alert_users') {
				dataset[data_key].splice(index, 1);
			}
		});
		this.setState({dataset: dataset});
		console.log('User Removed', this.state.dataset);
	}

	removeParameterRow(index) {
		let dataset = this.state.dataset;
		Object.keys(this.state.dataset).map((data_key) => {
			if (
				(data_key == 'loc_param_list') ||
				(data_key == 'loc_param_received_unit_list') ||
				(data_key == 'loc_param_view_unit_list')
			) {
				dataset[data_key].splice(index, 1);
			} else if (data_key == 'loc_param_ranges') {
				dataset[data_key].splice(index, 1);
			} else if (data_key == 'loc_param_channel') {
				dataset[data_key].splice(index, 1);
			} else if (data_key == 'loc_param_offset') {
				dataset[data_key].splice(index, 1);
			} else if (data_key == 'loc_param_multiplier') {
				dataset[data_key].splice(index, 1);
			}
		});
		this.setState({dataset: dataset});
		console.log('Parameter Removed', this.state.dataset);
	}

	addAnalyserRow() {
		let dataset = this.state.dataset;
		Object.keys(this.state.dataset).map((data_key) => {
			if (data_key == 'loc_param_details') {
				if (!dataset[data_key]) {
					dataset[data_key] = [];
				}
				dataset[data_key].push({
					vendor: '',
					make: '',
					model: '',
					certification: '',
					serial_no: '',
					flag: '',
					parameters_measured: []
				});
			}
		});
		this.setState({dataset: dataset});
		console.log('Analyser Added', this.state.dataset);
	}

	removeAnalyserRow(index) {
		let dataset = this.state.dataset;
		Object.keys(this.state.dataset).map((data_key) => {
			if (data_key == 'loc_param_details') {
				dataset[data_key].splice(index, 1);
			}
		});
		this.setState({dataset: dataset});
		console.log('Analyser Removed', this.state.dataset);
	}

	// Checkbox for parameters
	changeParamLimits(key, parameter_key, index, check) {
		let alert_details = this.state.alert_details;
		// console.log('checked', check);
		// console.log('index', index);
		if (check) {
			document.getElementById('param_limit_ll_warning_' + parameter_key) ? document.getElementById('param_limit_ll_warning_' + parameter_key).removeAttribute("disabled") : '';
			document.getElementById('param_limit_ul_warning_' + parameter_key) ? document.getElementById('param_limit_ul_warning_' + parameter_key).removeAttribute("disabled") : '';
			document.getElementById('param_limit_ll_danger_' + parameter_key) ? document.getElementById('param_limit_ll_danger_' + parameter_key).removeAttribute("disabled") : '';
			document.getElementById('param_limit_ul_danger_' + parameter_key) ? document.getElementById('param_limit_ul_danger_' + parameter_key).removeAttribute("disabled") : '';
			let thresholds_limits = this.state.alert_details.param_thresholds;
			if (parameter_key == "ph") {
				thresholds_limits[index]= {
					'll_warning': null,
					'ul_warning': null,
					'll_danger': null,
					'ul_danger': null
				};
			} else {
				thresholds_limits[index]= {
					'ul_warning': null,
					'ul_danger': null
				};
			}
			
			this.alert_details.param_thresholds = thresholds_limits;
			alert_details.param_thresholds = this.alert_details.param_thresholds;
			console.log('alert_details_check', this.alert_details.param_thresholds);
		} else {
			document.getElementById('param_limit_ll_warning_' + parameter_key) ? document.getElementById('param_limit_ll_warning_' + parameter_key).setAttribute("disabled", true) : '';
			document.getElementById('param_limit_ul_warning_' + parameter_key) ? document.getElementById('param_limit_ul_warning_' + parameter_key).setAttribute("disabled", true) : '';
			document.getElementById('param_limit_ll_danger_' + parameter_key) ? document.getElementById('param_limit_ll_danger_' + parameter_key).setAttribute("disabled", true) : '';
			document.getElementById('param_limit_ul_danger_' + parameter_key) ? document.getElementById('param_limit_ul_danger_' + parameter_key).setAttribute("disabled", true) : '';
			let thresholds_limits = this.state.alert_details.param_thresholds;
			
			thresholds_limits[index]= null;

			this.alert_details.param_thresholds = thresholds_limits;
			alert_details.param_thresholds = this.alert_details.param_thresholds;
			console.log('alert_details_uncheck', this.alert_details.param_thresholds);
		}
		this.setState({alert_details: alert_details});
		console.log('previous', this.applicable_parameter);
	}

	// setting parameter values
	setChangedLimits(index,value,key){
		console.log('alert_details', this.state.alert_details);
		let alert_details = this.state.alert_details,
			limits = this.state.alert_details.param_thresholds;
		if(!limits || !limits.length){
			limits = [];
		}
		console.log((limits == null),limits);
		if(!limits[index]){
			limits[index]={};
		}
		if(!isNaN(value)){
			if( value.charAt(value.length - 1) === '.' && value.match(/\./g).length < 2){
				limits[index][key] = value;						
			} else {
				limits[index][key] = parseFloat(value);						
			}
		} else {
			limits[index][key] = '';
		}
		alert_details.param_thresholds = limits;
		this.setState({
			alert_details:alert_details
		});
		console.log('alert_details', this.state.alert_details);
	}

	// setting all the alerts
	setDailyAlert(user_id){
		let alert_users = this.state.daily_alert_users;
		
		if(alert_users === null){
			alert_users = [];
		}
		
		if(alert_users.indexOf(parseInt(user_id)) === -1 ){
			alert_users.push(parseInt(user_id));
		}else if(alert_users.indexOf(parseInt(user_id)) !== -1){
			alert_users.splice(alert_users.indexOf(parseInt(user_id)),1);
		}
		// console.log('daily_alert', user_id);
		this.setState({
			daily_alert_users: alert_users,
			value_changed: true
		});

	}

	setPreWarningAlert(user_id){
		let alert_users = this.state.pre_warning_alert_users;
		if(alert_users === null){
			alert_users = [];
		}
		if(alert_users.indexOf(parseInt(user_id)) === -1 ){
			alert_users.push(parseInt(user_id));
		}else if(alert_users.indexOf(parseInt(user_id)) !== -1){
			alert_users.splice(alert_users.indexOf(parseInt(user_id)),1);
		}
		// console.log('pre_alert', user_id);
		this.setState({
			pre_warning_alert_users: alert_users,
			value_changed: true
		});
	}

	setOfflineAlert(user_id){
		let alert_users = this.state.offline_alert_users;
		if(alert_users === null){
			alert_users = [];
		}
		if(alert_users.indexOf(parseInt(user_id)) === -1 ){
			alert_users.push(parseInt(user_id));
		}else if(alert_users.indexOf(parseInt(user_id)) !== -1){
			alert_users.splice(alert_users.indexOf(parseInt(user_id)),1);
		}
		// console.log('offline_alert', user_id);
		this.setState({
			offline_alert_users: alert_users,
			value_changed: true
		});
		console.log(this.state.alert_details.parameters);
	}

	setViolationAlert(user_id){
		let alert_users = this.state.violation_alert_users;
		if(alert_users === null){
			alert_users = [];
		}
		if(alert_users.indexOf(parseInt(user_id)) === -1 ){
			alert_users.push(parseInt(user_id));
		}else if(alert_users.indexOf(parseInt(user_id)) !== -1){
			alert_users.splice(alert_users.indexOf(parseInt(user_id)),1);
		}
		// console.log('violation_alert', user_id);
		this.setState({
			violation_alert_users: alert_users,
			value_changed: true
		});
	}

	// Check all alerts
	checkAllSettings(key) {
		console.log('Select all for key:', key);
		if (this.state.users_list) {
			let states = this.state;
			let users_ids = Object.keys(this.state.users_list).map((user) => parseInt(user));
			// console.log('user-ids', users_ids);
			if (states[key].length !== users_ids.length) {
				states[key] = users_ids;
				this.setState(states);
			} else if (states[key].length === users_ids.length) {
				states[key] = [];
				this.setState(states);
			}
		}
	}

	saveConfiguration() {
		console.log('Save Config', this.state.dataset);
		let that = this;
		if (that.state.dataset.loc_param_list && that.state.dataset.loc_param_list.length) {
			that.setState({data_submitting: true});
			console.log('fetch', that.state.alert_details.param_thresholds);
			fetch('##PR_STRING_REPLACE_API_BASE_PATH##save_device_configuration.php', {
				method: 'POST',
				headers: {'Content-Type': 'application/json'},
				credentials: 'include',
				body: JSON.stringify({
					loc_id: that.state.loc_id,
					dataset: that.state.dataset,
					daily_alert_users: that.state.daily_alert_users,
					pre_warning_alert_users: that.state.pre_warning_alert_users,
					offline_alert_users: that.state.offline_alert_users,
					violation_alert_users: that.state.violation_alert_users,
					param_thresholds: that.state.alert_details.param_thresholds
				})
			}).then(function(Response) {
				return Response.json()
			}).then(function(json) {
				console.log(json);
				if(json.status === 'success') {
					showPopup('success','Successfully Saved.');
					that.setState({
						data_submitting: false,
						loc_spcb_id_disabled: that.state.dataset.loc_spcb_id ? true : false,
						org_category_disabled: parseInt(that.state.dataset.org_category) ? true : false,
						org_cpcb_code_disabled: that.state.dataset.org_cpcb_code ? true : false,
						loc_send_data_to_spcb_disabled: that.state.dataset.loc_send_data_to_spcb ? true : false,
						loc_spcb_data_published_disabled: that.state.dataset.loc_spcb_data_published ? true : false,
						loc_push_data_to_ospcb_disabled: that.state.dataset.loc_push_data_to_ospcb ? true : false,
						loc_cpcb_data_published_disabled: that.state.dataset.loc_cpcb_data_published ? true : false,
						loc_alert_send_cpcb_alert_disabled: that.state.dataset.loc_alert_send_cpcb_alert ? true : false,
						loc_push_data_to_cpcb_disabled: that.state.dataset.loc_push_data_to_cpcb ? true : false,
					});
				} else {
					showPopup('danger', json.message);
				}
			}).then(() => {
				that.setState({
					data_submitting: false
				});
			}).catch(function(ex) {
				console.log('parsing failed', ex);
				showPopup('danger', 'Unable to load data!');
			});
		} else {
			showPopup('danger', 'Please Add Atleast One Parameter!');
		}
	}
	
	componentDidMount() {
		document.title = 'Device Configuration -  DATOMS® Pollution Monitoring';

		window.addEventListener('resize', () => {
			this.setState({ view_mode: (window.innerWidth >= 1024) ? 'desktop' : 'mobile' });
		});
	}

	/**
     * This Renders Entire Device Configuration with navigation bar.
     * @return {object}   Returns value in Object format.
     */
	render() {
		return <div className="full-page-container" id="vendor_device_configuration">
			<Scrollbars autoHide>
				<NavLink activeLink={'configuration'} />
				<h3 className="page-title">Save Configuration</h3>
				{(() => {
					if (this.state.dataset) {
						return <div className="container-fluid">
							<div className="new-configuration-form">
								<div className="title-text less-margin">Industry Details</div>
								<div className="row">
									<div className="form-group col-lg-50">
										<b className="disabled-value" dangerouslySetInnerHTML={{__html: 'Industry Name - ' + this.state.dataset.org_name}}/>
									</div>
								</div>
								<div className="row">
									<div className="form-group col-lg-20">
										<label>Industry Category</label>
										{(() => {
											if (this.state.org_category_disabled) {
												return <b className="disabled-value">{this.state.org_categories[this.state.dataset.org_category]}</b>;
											} else {
												let org_cat_options = Object.keys(this.state.org_categories).map((key) => {
													return {value: parseInt(key), label: this.state.org_categories[key]};
												});
												return <Select options={org_cat_options} onChange={(e) => this.updateValues('org_category', parseInt(e.value))} value={parseInt(this.state.dataset.org_category)} clearable={false} />
											}
										})()}
									</div>
									<div className="form-group col-lg-15">
										<label>Code Alloted By CPCB</label>
										{(() => {
											if (this.state.org_cpcb_code_disabled) {
												return <b className="disabled-value">{this.state.dataset.org_cpcb_code}</b>;
											} else {
												return <input type="text" className="form-control" placeholder="Code Alloted By CPCB" value={this.state.dataset.org_cpcb_code} onChange={(e) => this.updateValues('org_cpcb_code', e.target.value)} />;
											}
										})()}
									</div>
									<div className="form-group col-lg-20">
										<label>SPCB User Id</label>
										<input type="text" className="form-control" placeholder="SPCB User Id" value={this.state.dataset.org_spcb_userid} onChange={(e) => this.updateValues('org_spcb_userid', e.target.value)} />
									</div>
									<div className="form-group col-lg-15">
										<label>SPCB Password</label>
										<input type="text" className="form-control" placeholder="SPCB Password" value={this.state.dataset.org_spcb_password} onChange={(e) => this.updateValues('org_spcb_password', e.target.value)} />
									</div>
									<div className="form-group col-lg-15 daily-alerts in-ganga">
										<input type="checkbox" id="org_in_ganga" className="margin-right" checked={this.state.dataset.org_in_ganga} onChange={(e) => this.updateValues('org_in_ganga', e.target.checked ? true : false)} />
										<label htmlFor="org_in_ganga">In Ganga Basin</label>
									</div>
								</div>
								<div className="row">
									<div className="form-group col-lg-20">
										<label>Industry Contact Person Name</label>
										<input type="text" className="form-control" placeholder="Contact Person Name" value={this.state.dataset.org_contacts ? this.state.dataset.org_contacts[0].name : ''} onChange={(e) => this.updateValues('org_contacts', e.target.value, 0, 'name')}/>
									</div>
									<div className="form-group col-lg-20">
										<label>Contact Person Designation</label>
										<input type="text" className="form-control" placeholder="Contact Person Designation" value={this.state.dataset.org_contacts ? this.state.dataset.org_contacts[0].designation : ''} onChange={(e) => this.updateValues('org_contacts', e.target.value, 0, 'designation')}/>
									</div>
									<div className="form-group col-lg-20">
										<label>Contact Person Email Id</label>
										<input type="text" className="form-control" placeholder="Contact Person Email Id" value={this.state.dataset.org_contacts ? this.state.dataset.org_contacts[0].email : ''} onChange={(e) => this.updateValues('org_contacts', e.target.value, 0, 'email')} />
									</div>
									<div className="form-group col-lg-20">
										<label>Contact Person Phone No</label>
										<input type="text" className="form-control" placeholder="Contact Person Phone No" value={this.state.dataset.org_contacts ? this.state.dataset.org_contacts[0].phone : ''} onChange={(e) => this.updateValues('org_contacts', e.target.value, 0, 'phone')} />
									</div>
								</div>
								<div className="row">
									<div className="title-text less-margin">Station Details</div>
									<div className="less-margin">
										<span className="disabled-value">Station ID -</span>
										<span className="consolas">{this.state.loc_id}</span>
									</div>
									<div className="form-group col-lg-20">
										<label>Choose Protocol To View Device ID</label>
									</div>
								</div>
								<div className="row">
									<div className="form-group col-lg-20 onoffswitch">
										<input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id={'myonoffswitch'} checked={(this.state.dev_protocol == 'HTTP') ? false : true} onChange={() => this.setState({dev_protocol: (this.state.dev_protocol == 'HTTP') ? 'TCP' : 'HTTP'})} />
										<label className="onoffswitch-label" htmlFor={'myonoffswitch'}>
											<span className="onoffswitch-inner onoffswitch-content"></span>
											<span className="onoffswitch-switch onoffswitch-top"></span>
										</label>
									</div>
									{(() => {
										if (this.state.dev_protocol == 'HTTP') {
											return <div>
												<div className="form-group center col-lg-15 flex">
													<span>
														<b className="disabled-value">ID -</b><span className="consolas">{this.state.dataset.loc_dev_id}</span>
													</span>
													<span className="input-group-button-copy" onMouseLeave={() => this.removeTooltip("loc_dev_id")}>
														<ClipboardButton option-text={(e) => this.getText('loc_dev_id')} onClick={(e) => this.onSuccess('loc_dev_id')} ref="loc_dev_id" data-tip="Click to copy to clipboard." wrapper="span" className="btn-tranparent-bg">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20px" height="20px"><path fill="none" d="M0 0h24v24H0z"/><path d="M16 1H4C3 1 2 2 2 3v14h2V3h12V1zm3 4H8C7 5 6 6 6 7v14c0 1 1 2 2 2h11c1 0 2-1 2-2V7c0-1-1-2-2-2zm0 16H8V7h11v14z"/></svg>
														</ClipboardButton>
													</span>
												</div>
												<div className="form-group center col-lg-25 flex">
													<span>
														<b className="disabled-value">Key -</b><span className="consolas">{this.state.dataset.loc_raw_dev_key}</span>
													</span>
													<span className="input-group-button-copy" onMouseLeave={() => this.removeTooltip("loc_raw_dev_key")}>
														<ClipboardButton option-text={(e) => this.getText('loc_raw_dev_key')} onClick={(e) => this.onSuccess('loc_raw_dev_key')} ref="loc_raw_dev_key" data-tip="Click to copy to clipboard." wrapper="span" className="btn-tranparent-bg">
															<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20px" height="20px"><path fill="none" d="M0 0h24v24H0z"/><path d="M16 1H4C3 1 2 2 2 3v14h2V3h12V1zm3 4H8C7 5 6 6 6 7v14c0 1 1 2 2 2h11c1 0 2-1 2-2V7c0-1-1-2-2-2zm0 16H8V7h11v14z"/></svg>
														</ClipboardButton>
													</span>
												</div>
											</div>;
										} else {
											return <div className="form-group center col-lg-60 flex">
												<b className="disabled-value">Auth Token -</b><span className="consolas">{this.state.dataset.idev_auth_token}</span>
												<span className="input-group-button-copy" onMouseLeave={() => this.removeTooltip("idev_auth_token")}>
													<ClipboardButton option-text={(e) => this.getText('idev_auth_token')} onClick={(e) => this.onSuccess('idev_auth_token')} ref="idev_auth_token" data-tip="Click to copy to clipboard." wrapper="span" className="btn-tranparent-bg">
														<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24" width="20px" height="20px"><path fill="none" d="M0 0h24v24H0z"/><path d="M16 1H4C3 1 2 2 2 3v14h2V3h12V1zm3 4H8C7 5 6 6 6 7v14c0 1 1 2 2 2h11c1 0 2-1 2-2V7c0-1-1-2-2-2zm0 16H8V7h11v14z"/></svg>
													</ClipboardButton>
												</span>
											</div>;
										}
									})()}
								</div>
								<div className="row">
									<div className="form-group col-lg-20">
										<label>Data Accusition Medium</label>
										{(() => {
											let data_medium_options = Object.keys(this.state.data_acquisition_mediums).map((key) => {
												return {value: key, label: this.state.data_acquisition_mediums[key]};
											});
											return <Select options={data_medium_options} onChange={(e) => this.updateValues('loc_data_acquisition_medium', e.value)} value={this.state.dataset.loc_data_acquisition_medium} clearable={false} />
										})()}
									</div>
									<div className="form-group col-lg-20 access-station">
										<label>Allow User To Access Station</label>
										<div className="flex power-line-height">
											<span className="power-svg">
												<svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" viewBox="-10 5 75 40">
													<path fill={(this.state.dataset.loc_is_active == '1') ? '#91DC5A' : '#D80027' } d="M49.652 24.826C49.652 11.137 38.515 0 24.826 0S0 11.137 0 24.826c0 13.688 11.137 24.826 24.826 24.826 13.69 0 24.826-11.136 24.826-24.826zm-22.388-.038c0 1.302-1.055 2.355-2.355 2.355-1.301 0-2.356-1.055-2.356-2.355V12.535c0-1.301 1.055-2.356 2.356-2.356s2.355 1.054 2.355 2.356v12.253zm13.593.079c0 8.839-7.191 16.029-16.031 16.029S8.795 33.706 8.795 24.867c0-4.285 1.669-8.313 4.701-11.34.46-.46 1.062-.689 1.665-.689s1.207.23 1.667.691c.92.921.919 2.412-.002 3.332-2.139 2.138-3.318 4.981-3.318 8.006 0 6.24 5.077 11.315 11.318 11.315s11.317-5.075 11.317-11.315c0-3.023-1.176-5.865-3.313-8.003-.92-.921-.919-2.412.001-3.333.921-.921 2.412-.919 3.333.001 3.025 3.028 4.693 7.054 4.693 11.335z"/>
												</svg>
											</span>
											<div className="onoffswitch">
												<input type="checkbox" name="onoffswitch" className="onoffswitch-checkbox" id={'allow_user_to_station'} checked={this.state.dataset.loc_is_active} onChange={() => this.updateValues('loc_is_active', (this.state.dataset.loc_is_active == true) ? false : true)} />
												<label className="onoffswitch-label" htmlFor={'allow_user_to_station'}>
													<span className="onoffswitch-inner"></span>
													<span className="onoffswitch-switch"></span>
												</label>
											</div>
										</div>
									</div>
								</div>
								<div className="row">
									<div className="form-group col-lg-20">
										<label>Station Type</label>
										{(() => {
											let loc_type_options = Object.keys(this.state.loc_types).map((key) => {
												return {value: key, label: this.state.loc_types[key]};
											});
											return <Select options={loc_type_options} onChange={(e) => this.updateValues('loc_type', e.value)} value={this.state.dataset.loc_type} clearable={false} />
										})()}
									</div>
									<div className="form-group col-lg-20">
										<label>Station Name</label>
										<input type="text" className="form-control" placeholder="Station Name" value={this.state.dataset.loc_name} onChange={(e) => this.updateValues('loc_name', e.target.value)} />
									</div>
									<div className="form-group col-lg-20">
										<label>Latitude</label>
										<input type="text" className="form-control" placeholder="Latitude" value={this.state.dataset.loc_lat} onChange={(e) => this.updateValues('loc_lat', e.target.value)} />
									</div>
									<div className="form-group col-lg-20">
										<label>Longitude</label>
										<input type="text" className="form-control" placeholder="Longitude" value={this.state.dataset.loc_long} onChange={(e) => this.updateValues('loc_long', e.target.value)} />
									</div>
								</div>
								<div className="clear" />
								<div className="title-text">Parameters</div>
								<div className="parameter-container">
									<div className="row">
										<div className="form-group col-lg-2 col-md-3 col-sm-6">
											<label>Parameter Name</label>
										</div>
										<div className="form-group col-lg-2 col-md-3 col-sm-6">
											<label>Parameter Receiving Unit</label>
										</div>
										<div className="form-group col-lg-2 col-md-3 col-sm-6">
											<label>Parameter Viewing Unit</label>
										</div>
										<div className="form-group col-lg-2 col-md-3 col-sm-6">
											<label>Range</label>
										</div>
										<div className="form-group col-lg-1 col-md-2 col-sm-3 padding-range">
											<label>Channel</label>
										</div>
										<div className="form-group col-lg-1 col-md-2 col-sm-3">
											<label>Offset</label>
										</div>
										<div className="form-group col-lg-1 col-md-2 col-sm-3">
											<label>Multiplier</label>
										</div>
									</div>
									{(() => {
										if (this.state.dataset.loc_param_list) {
											let param_rows = this.state.dataset.loc_param_list.map((param, index) => {
												return <div className="row parameter-item">
													<div className="form-group col-lg-2 col-md-2 col-sm-6">
														{(() => {
															let param_names_options = Object.keys(this.state.parameters).map((key) => {
																return {value: parseInt(key), label: this.state.parameters[key]};
															});
															console.log(index, this.state.dataset.loc_param_list[index]);
															return <Select options={param_names_options} onChange={(e) => this.updateValues('loc_param_list', parseInt(e.value), index)} value={this.state.dataset.loc_param_list[index]} placeholder="Parameter Name" clearable={false} />
														})()}
													</div>
													<div className="form-group col-lg-2 col-md-2 col-sm-6">
														{(() => {
															let param_units_options = Object.keys(this.state.parameter_units).map((key) => {
																return {value: parseInt(key), label: this.state.parameter_units[key]};
															});
															return <Select options={param_units_options} onChange={(e) => this.updateValues('loc_param_received_unit_list', parseInt(e.value), index)} value={this.state.dataset.loc_param_received_unit_list[index]} placeholder="Parameter Receiving Unit" clearable={false} />
														})()}
													</div>
													<div className="form-group col-lg-2 col-md-2 col-sm-6">
														{(() => {
															let param_units_options = Object.keys(this.state.parameter_units).map((key) => {
																return {value: parseInt(key), label: this.state.parameter_units[key]};
															});
															return <Select options={param_units_options} onChange={(e) => this.updateValues('loc_param_view_unit_list', parseInt(e.value), index)} value={this.state.dataset.loc_param_view_unit_list[index]} placeholder="Parameter Viewing Unit" clearable={false} />
														})()}
													</div>
													<div className="form-group col-lg-1 col-md-2 col-sm-3">
														<input type="text" className="form-control" value={this.state.dataset.loc_param_ranges[index][0]} onChange={(e) => this.updateValues('loc_param_ranges', e.target.value, index, 0)} />
													</div>
													<div className="form-group col-lg-1 col-md-2 col-sm-3 flex hyphen">
														<input type="text" className="form-control" value={this.state.dataset.loc_param_ranges[index][1]} onChange={(e) => this.updateValues('loc_param_ranges', e.target.value, index, 1)} />
													</div>
													<div className="form-group col-lg-1 col-md-2 col-sm-3">
														{(() => {
															if (this.state.loc_param_channel) {
																let loc_param_channel_options = Object.keys(this.state.loc_param_channel).map((key) => {
																	return {value: parseInt(key), label: this.state.loc_param_channel[key]};
																});
																return <Select options={loc_param_channel_options} onChange={(e) => this.updateValues('loc_param_channel', parseInt(e.value), index)} value={this.state.dataset.loc_param_channel[index]} placeholder="Channel" clearable={false} />;
															}
														})()}
													</div>
													<div className="form-group col-lg-1 col-md-2 col-sm-3">
														<input type="text" className="form-control" value={this.state.dataset.loc_param_offset[index]} onChange={(e) => this.updateValues('loc_param_offset', e.target.value, index)} />
													</div>
													<div className="form-group col-lg-1 col-md-2 col-sm-3">
														<input type="text" className="form-control" value={this.state.dataset.loc_param_multiplier[index]} onChange={(e) => this.updateValues('loc_param_multiplier', e.target.value, index)} />
													</div>
													<div className="form-group col-lg-1 last remove param">
														<span className="remove-param" onClick={() => this.removeParameterRow(index)}>✖</span>
													</div>
												</div>;
											});
											return param_rows;
										}
									})()}
									<button className="btn btn-left btn-sm green-border-btn" onClick={() => this.addParameterRow()}>＋ Add Parameter</button>
								</div>
								<div className="title-text">Analyzer</div>
								<div className="analyser-container">
									{(() => {
										if (this.state.dataset.loc_param_details) {
											let analyser_rows = this.state.dataset.loc_param_details.map((details, index) => {
												return <div className="analyser-item">
													<div className="row">
														<div className="form-group col-lg-2 col-md-4 col-sm-6">
															<label>Vendor</label>
															<input type="text" className="form-control" value={details.vendor} onChange={(e) => this.updateValues('loc_param_details', e.target.value, index, 'vendor')} />
														</div>
														<div className="form-group col-lg-2 col-md-4 col-sm-6">
															<label>Make</label>
															<input type="text" className="form-control" value={details.make} onChange={(e) => this.updateValues('loc_param_details', e.target.value, index, 'make')} />
														</div>
														<div className="form-group col-lg-2 col-md-4 col-sm-6">
															<label>Model</label>
															<input type="text" className="form-control" value={details.model} onChange={(e) => this.updateValues('loc_param_details', e.target.value, index, 'model')} />
														</div>
														<div className="form-group col-lg-2 col-md-4 col-sm-6">
															<label>Certification</label>
															<input type="text" className="form-control" value={details.certification} onChange={(e) => this.updateValues('loc_param_details', e.target.value, index, 'certification')} />
														</div>
														<div className="form-group col-lg-2 col-md-4 col-sm-6">
															<label>Serial No.</label>
															<input type="text" className="form-control" value={details.serial_no} onChange={(e) => this.updateValues('loc_param_details', e.target.value, index, 'serial_no')} />
														</div>
														<div className="form-group col-lg-2 col-md-4 col-sm-6 analyser-status">
															<label>Analyser Status</label>
															{(() => {
																let loc_analyser_status = [];
																if (this.state.loc_analyser_status) {
																	this.state.loc_analyser_status.map((value, key) => {
																		loc_analyser_status.push(value);
																	});
																}
																return <Select options={loc_analyser_status} onChange={(e) => this.updateValues('loc_param_details', e.value, index, 'flag')} value={details.flag} clearable={false} />
															})()}
														</div>
													</div>
													<div className="row">
														<div className="form-group col-lg-11 col-md-12 col-sm-12">
															<label>Parameters</label>
															{(() => {
																let param_options = Object.keys(this.state.parameters).map((key) => {
																	return {value: parseInt(key), label: this.state.parameters[key]};
																});
																return <Select options={param_options} onChange={(e) => this.updateValues('loc_param_details', e, index, 'parameters_measured')} value={details.parameters_measured} multi={true} />
															})()}
														</div>
														<div className="form-group col-lg-1 last remove analyser">
															<span className="remove-param" onClick={() => this.removeAnalyserRow(index)}>✖</span>
														</div>
													</div>
												</div>;
											});
											return analyser_rows;
										}
									})()}
									<button className="btn btn-left btn-sm green-border-btn" onClick={() => this.addAnalyserRow()}>＋ Add Analyser</button>
								</div>
								<div className="row">
									<div className="form-group col-lg-15">
										<label>Device Attached To Process</label>
										<input type="text" className="form-control" value={this.state.dataset.loc_process_to_which_device_attached} placeholder="Device Attached Process" onChange={(e) => this.updateValues('loc_process_to_which_device_attached', e.target.value)} />
									</div>
								</div>
								<div className="row">
									<div className="title-text margin-less">SPCB Details</div>
									<div className="form-group col-lg-15">
										<label>SPCB Id</label>
										{(() => {
											if (this.state.loc_spcb_id_disabled) {
												return <b className="disabled-value">{this.state.dataset.loc_spcb_id}</b>;
											} else {
												return <input type="text" className="form-control" value={this.state.dataset.loc_spcb_id} placeholder="SPCB Id" onChange={(e) => this.updateValues('loc_spcb_id', e.target.value)}/>;
											}
										})()}
									</div>
									<div className={'form-group col-lg-15' + (this.state.loc_send_data_to_spcb_disabled ? ' daily-alerts' : '')}>
										{(() =>{
											if (this.state.loc_send_data_to_spcb_disabled) {
												return <b className="disabled-value padding-left">Data Sent To SPCB - YES</b>;
											} else {
												return <div className="daily-alerts">
													<input type="checkbox" id="loc_send_data_to_spcb" className="margin-right" checked={this.state.dataset.loc_send_data_to_spcb} onChange={(e) => this.updateValues('loc_send_data_to_spcb', e.target.checked ? true : false)} />
													<label htmlFor="loc_send_data_to_spcb">Data sent to SPCB</label>
												</div>;
											}
										})()}
									</div>
									<div className={'form-group col-lg-15' + (this.state.loc_spcb_data_published_disabled ? ' daily-alerts' : '')}>
										{(() => {
											if (this.state.loc_spcb_data_published_disabled) {
												return <b className="disabled-value padding-left">SPCB Data Published - YES</b>;
											} else {
												return <div className="daily-alerts">
													<input type="checkbox" id="loc_spcb_data_published" className="margin-right" checked={this.state.dataset.loc_spcb_data_published} onChange={(e) => this.updateValues('loc_spcb_data_published', e.target.checked ? true : false)} />
													<label htmlFor="loc_spcb_data_published">SPCB Data Published</label>
												</div>;
											}
										})()}
										
									</div>
									<div className={'form-group col-lg-20' + (this.state.loc_push_data_to_ospcb_disabled ? ' daily-alerts' : '')}>
										{(() => {
											if (this.state.loc_push_data_to_ospcb_disabled) {
												return <b className="disabled-value padding-left">SPCB Data Pushed - YES</b>;
											} else {
												return <div className="daily-alerts">
													<input type="checkbox" id="loc_push_data_to_ospcb" className="margin-right" checked={this.state.dataset.loc_push_data_to_ospcb} onChange={(e) => this.updateValues('loc_push_data_to_ospcb', e.target.checked ? true : false)} />
													<label htmlFor="loc_push_data_to_ospcb">Push Data to SPCB</label>
												</div>;
											}
										})()}
									</div>
								</div>
								<div className="row">
									<div className="form-group col-lg-15">
										<label>SPCB Station ID</label>
										<input type="text" className="form-control" value={(this.state.dataset.loc_ospcb_station_id)} placeholder="Station ID" onChange={(e) => this.updateValues('loc_ospcb_station_id', e.target.value)}/>
									</div>
									<div className="form-group col-lg-15">
										<label>SPCB Organization ID</label>
										<input type="text" className="form-control" value={(this.state.dataset.loc_ospcb_plant_id)} placeholder="Organization ID" onChange={(e) => this.updateValues('loc_ospcb_plant_id', e.target.value)}/>
									</div>
									<div className="form-group col-lg-40">
										<label>SPCB Data Push Token No</label>
										<input type="text" className="form-control" value={(this.state.dataset.loc_ospcb_data_push_token)} placeholder="Data Push Token No" onChange={(e) => this.updateValues('loc_ospcb_data_push_token', e.target.value)}/>
									</div>
								</div>
								<div className="row">
									<div className="title-text margin-less">CPCB Details</div>
									<div className={'form-group col-lg-20' + (this.state.loc_cpcb_data_published_disabled ? ' daily-alerts less-padding-top' : '')}>
										{(() => {
											if (this.state.loc_cpcb_data_published_disabled) {
												return <b className="disabled-value padding-left">CPCB Data Published - YES</b>;
											} else {
												return <div className="daily-alerts less-padding-top">
													<input type="checkbox" id="loc_cpcb_data_published" className="margin-right" checked={this.state.dataset.loc_cpcb_data_published} onChange={(e) => this.updateValues('loc_cpcb_data_published', e.target.checked ? true : false)} />
													<label htmlFor="loc_cpcb_data_published">CPCB Data Published</label>
												</div>;
											}
										})()}
									</div>
									<div className={'form-group col-lg-15' + (this.state.loc_alert_send_cpcb_alert_disabled ? ' daily-alerts less-padding-top' : '')}>
										{(() => {
											if (this.state.loc_alert_send_cpcb_alert_disabled) {
												return <b className="disabled-value padding-left">Send CPCB Alerts - YES</b>;
											} else {
												return <div className="daily-alerts less-padding-top">
													<input type="checkbox" id="loc_alert_send_cpcb_alert" className="margin-right" checked={this.state.dataset.loc_alert_send_cpcb_alert} onChange={(e) => this.updateValues('loc_alert_send_cpcb_alert', e.target.checked ? true : false)} />
													<label htmlFor="loc_alert_send_cpcb_alert">Send CPCB Alerts</label>
												</div>;
											}
										})()}
									</div>
									<div className={'form-group col-lg-20' + (this.state.loc_push_data_to_cpcb_disabled ? ' daily-alerts less-padding-top' : '')}>
										{(() => {
											if (this.state.loc_push_data_to_cpcb_disabled) {
												return <b className="disabled-value padding-left">CPCB Data Pushed - YES</b>;
											} else {
												return <div className="daily-alerts less-padding-top">
													<input type="checkbox" id="loc_push_data_to_cpcb" className="margin-right" checked={this.state.dataset.loc_push_data_to_cpcb} onChange={(e) => this.updateValues('loc_push_data_to_cpcb', e.target.checked ? true : false)} />
													<label htmlFor="loc_push_data_to_cpcb">Push Data to CPCB</label>
												</div>;
											}
										})()}
									</div>
								</div>
								<div className="row">
									<div className="form-group col-lg-20">
										<label>SIM Operator</label>
										{(() => {
											let loc_sim_operator = [];
											if (this.state.loc_sim_operator) {
												this.state.loc_sim_operator.map((value, key) => {
													loc_sim_operator.push(value);
												});
											}
											return <Select options={loc_sim_operator} onChange={(e) => this.updateValues('loc_sim_operator', e.value)} value={this.state.dataset.loc_sim_operator} clearable={false} />
										})()}
									</div>
									<div className="form-group col-lg-30">
										<label>SIM No</label>
										<input type="text" className="form-control" value={(this.state.dataset.loc_sim_serial_num)} placeholder="SIM no" onChange={(e) => this.updateValues('loc_sim_serial_num', e.target.value)}/>
									</div>
									<div className="form-group col-lg-20">
										<label>Mobile No</label>
										<input type="text" className="form-control" value={this.state.dataset.loc_sim_mob_num} placeholder="Mobile No" onChange={(e) => this.updateValues('loc_sim_mob_num', e.target.value)}/>
									</div>
								</div>
								<div className="row">
									<div className="form-group col-lg-20">
										<label>QR Code</label>
										<input type="text" className="form-control" placeholder="QR Code" value={this.state.dataset.idev_qr_code} onChange={(e) => this.updateValues('idev_qr_code', e.target.value)} />
									</div>
									<div className="form-group col-lg-20">
										<label>Firmware Version</label>
										<input type="text" className="form-control" placeholder="Firmware Version" value={this.state.dataset.idev_firmware_version} onChange={(e) => this.updateValues('idev_firmware_version', e.target.value)} />
									</div>
									<div className="form-group col-lg-30">
										<label>Installed By</label>
										<input type="text" className="form-control" placeholder="Installed By" value={this.state.dataset.loc_instl_done_by} onChange={(e) => this.updateValues('loc_instl_done_by', e.target.value)} />
									</div>
									<div className="form-group col-lg-20">
										<label>Installed On</label>
										<Datetime dateFormat="DD-MM-YYYY" timeFormat={false} value={(this.state.dataset.loc_instl_done_at == 0) ? '' : moment.unix(this.state.dataset.loc_instl_done_at).tz('Asia/Kolkata').format('DD-MM-YYYY')} onChange={(e) => this.updateValues('loc_instl_done_at', parseInt(e.tz('Asia/Kolkata').format('X')))} />
									</div>
								</div>
								<div className="title-text">Alerts</div>
								<div className="param-threshold-settings">
									<div className="alerts-title">Set Threshold limits for Parameters</div>
										{(() => {
											if (this.state.alert_details && this.state.alert_details.parameters) {
												return <div className="param-threshold-details">
													<div className="row param-threshold-table-head flex">
														{(()=>{
															let isPH = false;
															if(this.state.alert_details.parameters.indexOf('PH') != -1){
																isPH = true;
															}
															if(isPH){
																return <div className="flex threshold-width">
																	<div className="threshold-center width-control-5">Parameter Name</div>
																	<div className="threshold-center width-control-5">Lower Warning Limit</div>
																	<div className="threshold-center width-control-5">Upper Warning Limit</div>
																	<div className="threshold-center width-control-5">Lower Danger Limit</div>
																	<div className="threshold-center width-control-5">Upper Danger Limit</div>
																</div>;
															} else {
																return <div className="flex threshold-width">
																	<div className="threshold-center width-control-3">Parameter Name</div>
																	<div className="threshold-center width-control-3">Warning Limit</div>
																	<div className="threshold-center width-control-3">Danger Limit</div>
																</div>;
															} 
														})()}
													</div>
													<div className="row param-threshold-table-body">
														{(()=>{
															if(this.state.alert_details.parameters){
																let isPH = false;
																if(this.state.alert_details.parameters.indexOf('PH') != -1){
																	isPH = true;
																}
																let parameter_details = this.state.alert_details.parameters.map((param,index)=>{
																	if(isPH){
																		return <div className="flex threshold-width threshold-row">
																			<input type="checkbox" name="param-limit-check" id={'param_limit_check_' + (this.state.alert_details.parameters[index])} checked={this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] != null ? true : false} className="param-limit-check" onChange={(e) => this.changeParamLimits('param_limit_check', this.state.alert_details.parameters[index], index, (e.target.checked ? true : false))} />
																			<div className="flex sub-row">
																				<div className="threshold-center width-control-5" dangerouslySetInnerHTML={{__html:param}}/>
																				<div className="threshold-center width-control-5">
																					{(()=>{
																						if(param === 'PH') {
																							if(this.state.alert_details.param_thresholds != null && this.state.alert_details.param_thresholds[index] && !isNaN(this.state.alert_details.param_thresholds[index].ll_warning) && typeof(this.state.alert_details.param_thresholds[index].ll_warning) != 'undefined') {
																								return <input type="text" className="edit-value" id={'param_limit_ll_warning_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={(this.state.alert_details.param_thresholds[index].ll_warning != null) ? this.state.alert_details.param_thresholds[index].ll_warning : ''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ll_warning');}} />;
																							} else {
																								return <input type="text" className="edit-value" id={'param_limit_ll_warning_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ll_warning');}} />;
																							}
																						}else{
																							return 'NA';
																						}
																					})()}
																				</div>
																				<div className="threshold-center width-control-5">
																					{(()=>{
																						if(this.state.alert_details.param_thresholds != null && this.state.alert_details.param_thresholds[index] && !isNaN(this.state.alert_details.param_thresholds[index].ul_warning) && typeof(this.state.alert_details.param_thresholds[index].ul_warning) != 'undefined') {
																							return <input type="text" className="edit-value" id={'param_limit_ul_warning_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={(this.state.alert_details.param_thresholds[index].ul_warning != null) ? this.state.alert_details.param_thresholds[index].ul_warning : ''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ul_warning');}} />;
																						} else {
																							return <input type="text" className="edit-value" id={'param_limit_ul_warning_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ul_warning');}} />;
																						}
																					})()}
																				</div>	
																				<div className="threshold-center width-control-5">
																					{(()=>{
																						if(param === 'PH') {
																							if((this.state.alert_details.param_thresholds != null && this.state.alert_details.param_thresholds[index] && !isNaN(this.state.alert_details.param_thresholds[index].ll_warning) && this.state.alert_details.param_thresholds[index].ll_danger != null)) {
																								return <input type="text" className="edit-value" id={'param_limit_ll_warning_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={(this.state.alert_details.param_thresholds[index].ll_warning != null) ? this.state.alert_details.param_thresholds[index].ll_warning : ''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ll_warning');}} />;
																							} else {
																								return <input type="text" className="edit-value" id={'param_limit_ll_warning_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ll_warning');}} />;
																							}
																						}else{
																							return 'NA';
																						}
																					})()}
																				</div>
																				<div className="threshold-center width-control-5">
																					{(()=>{
																						if(this.state.alert_details.param_thresholds != null && this.state.alert_details.param_thresholds[index] && typeof(this.state.alert_details.param_thresholds[index].ul_danger) !== 'undefined') {
																							return <input type="text" className="edit-value" id={'param_limit_ul_danger_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={(this.state.alert_details.param_thresholds[index].ul_danger != null) ? this.state.alert_details.param_thresholds[index].ul_danger : ''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ul_danger');}} />;
																						} else {
																							return <input type="text" className="edit-value" id={'param_limit_ul_danger_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ul_danger');}} />;
																						}
																					})()}
																				</div>
																			</div>
																		</div>;
																	} else {
																		return <div className="flex threshold-width threshold-row">
																			<input type="checkbox" name="param-limit-check" id={'param_limit_check_' + (this.state.alert_details.parameters[index])} checked={this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] != null ? true : false} className="param-limit-check" onChange={(e) => this.changeParamLimits('param_limit_check', this.state.alert_details.parameters[index], index, (e.target.checked ? true : false))} />
																			<div className="flex sub-row">
																				<div className="threshold-center width-control-3" dangerouslySetInnerHTML={{__html:param}}/>
																				<div className="threshold-center width-control-3">
																					{(()=>{
																						if(this.state.alert_details.param_thresholds != null && this.state.alert_details.param_thresholds[index] && !isNaN(this.state.alert_details.param_thresholds[index].ul_warning) && typeof(this.state.alert_details.param_thresholds[index].ul_warning) != 'undefined') {
																							return <input type="text" className="edit-value" id={'param_limit_ul_warning_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={(this.state.alert_details.param_thresholds[index].ul_warning != null) ? this.state.alert_details.param_thresholds[index].ul_warning : ''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ul_warning');}} />;
																						} else {
																							return <input type="text" className="edit-value" id={'param_limit_ul_warning_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == null) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ul_warning');}} />;
																						}
																					})()}
																				</div>
																				<div className="threshold-center width-control-3">
																					{(()=>{
																						if(this.state.alert_details.param_thresholds != null && this.state.alert_details.param_thresholds[index] && typeof(this.state.alert_details.param_thresholds[index].ul_danger) !== 'undefined') {
																							return <div className="flex">
																								<input type="text" className="edit-value width-control-input-span" id={'param_limit_ul_danger_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == 0) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={(this.state.alert_details.param_thresholds[index].ul_danger != null) ? this.state.alert_details.param_thresholds[index].ul_danger : ''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ul_danger');}} />
																								<span className="width-control-input-span" dangerouslySetInnerHTML={{__html:this.state.alert_details.param_units[index]}}/>
																							</div>;
																						} else {
																							return <div className="flex">
																								<input type="text" className="edit-value width-control-input-span" id={'param_limit_ul_danger_' + (this.state.alert_details.parameters[index])} disabled= {(this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] && Object.keys(this.state.alert_details.param_thresholds[index]).length == 0) || this.state.alert_details.param_thresholds && this.state.alert_details.param_thresholds[index] == null ? true : false} value={''} onChange={(e)=>{this.setChangedLimits(index,e.target.value,'ul_danger');}} />
																								<span className="width-control-input-span" dangerouslySetInnerHTML={{__html:this.state.alert_details.param_units[index]}}/>
																							</div>;
																						}
																					})()}
																				</div>
																			</div>
																		</div>;
																	}
																}).filter(Boolean);
																return parameter_details;
															}else {
																return <div className="flex threshold-width">
																	<div className="threshold-center width-control"> No </div>
																	<div className="threshold-center width-control"> Data </div>
																	<div className="threshold-center width-control"> Found </div>
																</div>;
															}
														})()}
													</div>
												</div>;
											}
										})()}
									</div>
								<div className="alert-settings">
								<div className="alerts-title">Add Alerts to users</div>
									<div className="station-alert-details">
										<div className="alert-table">
											{(() => {
												if (this.state.users_list && Object.keys(this.state.users_list).length) {
													return <div className="alert-table-body-height">
														<div className="alerts-content-table">
															<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12 alert-details-table">
																<div className="row table-head-alerts flex">
																	<div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 flex line-height-alerts">User Name</div>
																	<div className="col-lg-6 col-md-6 col-sm-6 col-xs-6 alerts-center border-alerts">
																		<div className="flex">
																			<div className="alerts-industries">Daily alerts</div>
																			<div className="alerts-industries">Pre-warning alerts</div>
																			<div className="alerts-industries">Offline alerts</div>
																		</div>
																		<small className="alerts-type">(Industries)</small>
																	</div>
																	<div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 alerts-center">
																		<div className="flex">
																			<div className="alerts-cpcb">Violation</div>
																			{/*<div className="alerts-cpcb">Offline Alert</div>*/}
																		</div>
																		<small className="alerts-type">(CPCB)</small>
																	</div>
																</div>
																<div className="row table-body-alerts select-all-row-alerts">
																	<div className="col-lg-3 col-md-3 col-sm-3 col-xs-3">
																		<div className="select-all-alerts">Select All</div>
																	</div>
																	<div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																		<div className="flex">
																			<div className="alerts-industries">
																				<input type="checkbox" onChange={() => this.checkAllSettings('daily_alert_users')} checked= {(this.state.daily_alert_users && this.state.daily_alert_users.length === (this.state.users_list ? Object.keys(this.state.users_list).length : '')) ? true : false}/>
																			</div>
																			<div className="alerts-industries">
																				<input type="checkbox" onChange={() => this.checkAllSettings('pre_warning_alert_users')} checked= {(this.state.pre_warning_alert_users && this.state.pre_warning_alert_users.length === (this.state.users_list ? Object.keys(this.state.users_list).length : '')) ? true : false} />
																			</div>
																			<div className="alerts-industries">
																				<input type="checkbox" onChange={() => this.checkAllSettings('offline_alert_users')} checked= {(this.state.offline_alert_users && this.state.offline_alert_users.length === (this.state.users_list ? Object.keys(this.state.users_list).length : '')) ? true : false} />
																			</div>
																		</div>
																	</div>
																	<div className="col-lg-3 col-md-3 col-sm-3 col-xs-3 alerts-center">
																		<div className="flex">
																			<div className="alerts-cpcb">
																				<input type="checkbox" onChange={() => this.checkAllSettings('violation_alert_users')} checked= {(this.state.violation_alert_users && this.state.violation_alert_users.length === (this.state.users_list ? Object.keys(this.state.users_list).length : '')) ? true : false} />
																			</div>
																			{/*<div className="alerts-cpcb">Offline Alert</div>*/}
																		</div>
																	</div>
																</div>
																{(()=>{
																	if(this.state.users_list){
																		let alert_details = Object.keys(this.state.users_list).map((key)=>{
																			if(this.state.users_list[key]){
																				return <div className="row table-body-alerts">
																				<div className="col-lg-3 col-md-3 col-sm-3 col-xs-3">
																					<div className="name">{this.state.users_list[key]}</div>
																				</div>
																				<div className="col-lg-6 col-md-6 col-sm-6 col-xs-6">
																					<div className="flex">
																						{(()=>{
																							let is_checked = false;
																							if(this.state.daily_alert_users !== null && this.state.daily_alert_users.indexOf(parseInt(key)) > -1) {
																								is_checked = true;
																							}			
																							return <div className="alerts-industries">
																								<input onChange={() => this.setDailyAlert(key)} type="checkbox" className="" checked={is_checked}/>
																							</div>;		
																						})()}
																						{(()=>{
																							let is_true = false;		
																							if(this.state.pre_warning_alert_users !== null && this.state.pre_warning_alert_users.indexOf(parseInt(key)) > -1) {
																								is_true = true;
																							}
																							return <div className="alerts-industries">
																								<input onChange={() => this.setPreWarningAlert(key)} type="checkbox" className="" checked={is_true}/>
																							</div>;
																						})()}
																						{(()=>{
																							let is_true = false;		
																							if(this.state.offline_alert_users !== null && this.state.offline_alert_users.indexOf(parseInt(key)) > -1) {
																								is_true = true;
																							}	
																							return <div className="alerts-industries">
																								<input onChange={() => this.setOfflineAlert(key)} type="checkbox" className="" checked={is_true} />
																							</div>;
																						})()}
																						{/* <div className="alerts-industries">
																							<input type="checkbox" className="" />
																						</div> */}
																						{/* <div className="alerts-industries">
																							<input type="checkbox" className="" />
																						</div> */}
																						{/* <div className="alerts-industries">
																							<input type="checkbox" className="" />
																						</div> */}
																					</div>
																				</div>
																				<div className="col-lg-3 col-md-3 col-sm-3 col-xs-3">
																					<div className="flex">
																						{(()=>{
																							let is_true = false;		
																							if(this.state.violation_alert_users !== null && this.state.violation_alert_users.indexOf(parseInt(key)) > -1) {
																								is_true = true;
																							}	
																							return<div className="alerts-cpcb"><input type="checkbox" className="" onChange={() => this.setViolationAlert(key)} checked={is_true}/></div>;
																						})()}
																						{/* <div className="alerts-cpcb"><input type="checkbox" className="" /></div> */}
																						{/*<div className="alerts-cpcb"><input type="checkbox" className="" /></div>*/}
																					</div>
																				</div>
																			</div>;
																			}
																		});
																		return alert_details;
																	}else{
																		return <div className="row table-body-alerts">
																			<div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
																				<div className="name text-center">
																					Sorry, No data found
																				</div>
																			</div>
																		</div>;
																	}
																})()}
															</div>
														</div>
													</div>;
												} else {
													return <div className="alerts-no-data-text">
														<center>No Users Found!</center>
													</div>;
												}
											})()}
										</div>
									</div>
								</div>
								<div className="row">
									{(() => {
										if (this.state.data_submitting) {
											return <div className="inline-loading">
												<svg className="loading-spinner" width="20" height="20" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
												<span>Saving Configuration...</span>
												<div className="form-cover" />
											</div>;
										} else {
											return <button type="button" className="btn green-fill-btn btn-right centered-btn" onClick={() => this.saveConfiguration()}>Save</button>;
										}
									})()}
								</div>
							</div>
							<ReactTooltip effect="solid" />
						</div>;
					} else {
						return <div className="loading">
							<svg className="loading-spinner" width="30" height="30" viewBox="0 0 66 66" xmlns="http://www.w3.org/2000/svg"><circle className="path" fill="none" strokeWidth={6} strokeLinecap="round" cx={33} cy={33} r={30} /></svg>
						</div>;
					}
				})()}
			</Scrollbars>
		</div>;
	}
}

DeviceConfiguration.contextTypes = {
	router: React.PropTypes.object
};