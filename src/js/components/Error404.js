import React from 'react';
import NavLink from './NavLink';
import { Scrollbars } from 'react-custom-scrollbars';

export default class Error404 extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		document.title = 'Error404 - Datoms Vendor Portal';
	}

	render() {
		return(
			<div className="full-page-container">
				<Scrollbars autoHide>
					<NavLink />
					<div className= "full-page-container body-height">
						<div className="no-data-text">
							<center className="page-not-found">
								<h1>404 Not Found</h1>
							</center>
						</div>
					</div>
				</Scrollbars>
			</div>
		);
	}
}
